from setuptools import setup

setup(name='melissa',
      version='1.0.0',
      python_requires='>=3.8',
      package_data={'melissa': ['utility/bash_scripts/*.sh']},
)
