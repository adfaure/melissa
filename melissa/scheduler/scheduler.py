#!/usr/bin/python3

# Copyright (c) 2020-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__all__ = ["DirectScheduler", "IndirectScheduler", "Options", "Scheduler"]

import re
from typing import cast, Dict, Generic, List, Optional, Tuple, TypeVar, Union
from melissa.utility.process import ArgumentList, CompletedProcess, Environment, Process

from .options import Options

JobT = TypeVar("JobT")


class Scheduler(Generic[JobT]):
    _valid_name_pattern = re.compile("[A-Za-z0-9_-]+", re.ASCII)

    @classmethod
    def name(cls) -> str:
        """
        Returns a scheduler name for use by programs.
        The name must contain only ASCII characters and no whitespace.
        """
        name = cls._name_impl()
        if cls._valid_name_pattern.fullmatch(name) is None:
            raise RuntimeError("invalid scheduler name: '%s'" % name)
        return name

    @classmethod
    def is_direct(cls) -> bool:
        raise NotImplementedError("Scheduler.is_direct not implemented")

    @classmethod
    def is_available(cls) -> Tuple[bool, Union[str, Tuple[str, str]]]:
        """
        Returns a triple containing True, the scheduler path, and scheduler
        version if available; a pair containing False, and a reason otherwise.
        """
        return cls._is_available_impl()

    def sanity_check(self, options: Options) -> List[str]:
        return self._sanity_check_impl(options)

    def submit_job(
        self,
        commands: List[ArgumentList],
        env: Dict[str, Optional[str]],
        options: Options,
        name: str,
        unique_id: int,
    ) -> Tuple[ArgumentList, Environment]:
        if commands == []:
            raise ValueError("expected at least one command, got none")

        # allow None as dictionary value so that callers can pass, for example,
        #   env = { "PYTHONPATH": os.getenv("PYTHONPATH") }
        # without having to check if the environment variable was set
        env_clean = dict([(k, cast(str, env[k])) for k in env.keys() if env[k] is not None])

        return self._submit_job_impl(commands, env_clean, options, name, unique_id)

    @classmethod
    def _name_impl(cls) -> str:
        raise NotImplementedError("BUG missing implementation")

    @classmethod
    def _is_available_impl(cls) -> Tuple[bool, Union[str, Tuple[str, str]]]:
        raise NotImplementedError("BUG missing implementation")

    def _sanity_check_impl(self, options: Options) -> List[str]:
        return []

    def _submit_job_impl(
        self,
        commands: List[ArgumentList],
        env: Environment,
        options: Options,
        name: str,
        unique_id: int,
    ) -> Tuple[ArgumentList, Environment]:
        raise NotImplementedError("BUG missing implementation")


class DirectScheduler(Generic[JobT], Scheduler[JobT]):
    @classmethod
    def is_direct(cls) -> bool:
        return True

    def make_job(self, proc: "Process[str]", unique_id: int) -> JobT:
        return self._make_job_impl(proc, unique_id)

    @classmethod
    def update_jobs(cls, jobs: List[JobT]) -> None:
        """
        This method updates the status of the given jobs.

        The method is a classmethod because it is not supposed any state. It
        might be executed concurrently with other scheduler methods.
        """
        if jobs == []:
            raise ValueError("the list of jobs must not be empty")
        return cls._update_jobs_impl(jobs)

    @classmethod
    def cancel_jobs(cls, jobs: List[JobT]) -> None:
        """
        This method cancels the given jobs.

        The method is a classmethod because it is not supposed any state. It
        might be executed concurrently with other scheduler methods.
        """
        if jobs == []:
            raise ValueError("the list of jobs must not be empty")
        return cls._cancel_jobs_impl(jobs)

    def _make_job_impl(self, proc: "Process[str]", unique_id: int) -> JobT:
        raise NotImplementedError("BUG missing implementation")

    @classmethod
    def _update_jobs_impl(cls, jobs: List[JobT]) -> None:
        raise NotImplementedError("BUG missing implementation")

    @classmethod
    def _cancel_jobs_impl(cls, jobs: List[JobT]) -> None:
        raise NotImplementedError("BUG missing implementation")


class IndirectScheduler(Generic[JobT], Scheduler[JobT]):
    @classmethod
    def is_direct(cls) -> bool:
        return False

    def make_job(self, proc: CompletedProcess, unique_id: int) -> JobT:
        return self._make_job_impl(proc, unique_id)

    def update_jobs(self, jobs: List[JobT]) -> Tuple[ArgumentList, Environment]:
        return self._update_jobs_impl(jobs)

    def cancel_jobs(self, jobs: List[JobT]) -> Tuple[ArgumentList, Environment]:
        return self._cancel_jobs_impl(jobs)

    def parse_update_jobs(self, jobs: List[JobT], proc: CompletedProcess) -> None:
        return self._parse_update_jobs_impl(jobs, proc)

    def parse_cancel_jobs(self, jobs: List[JobT], proc: CompletedProcess) -> None:
        return self._parse_cancel_jobs_impl(jobs, proc)

    def _make_job_impl(self, proc: CompletedProcess, unique_id: int) -> JobT:
        raise NotImplementedError("BUG missing implementation")

    def _update_jobs_impl(self, jobs: List[JobT]) -> Tuple[ArgumentList, Environment]:
        raise NotImplementedError("BUG missing implementation")

    def _cancel_jobs_impl(self, jobs: List[JobT]) -> Tuple[ArgumentList, Environment]:
        raise NotImplementedError("BUG missing implementation")

    def _parse_update_jobs_impl(self, jobs: List[JobT], proc: CompletedProcess) -> None:
        raise NotImplementedError("BUG missing implementation")

    def _parse_cancel_jobs_impl(self, jobs: List[JobT], proc: CompletedProcess) -> None:
        raise NotImplementedError("BUG missing implementation")
