#!/usr/bin/python3

# Copyright (c) 2020-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import logging
import os
import re
import shutil
import subprocess
from types import ModuleType
from typing import cast, List, Tuple, Union

from melissa.utility import time
from melissa.utility.process import ArgumentList, Environment, Process

from .job import Id, Job, State
from .scheduler import DirectScheduler, Options

logging = cast(ModuleType, logging.getLogger(__name__))


class SrunJob(Job):
    def __init__(self, uid: Id, process: "subprocess.Popen[str]") -> None:
        super().__init__()
        self._uid = uid
        self._process = process
        self._state = State.RUNNING

    def id(self) -> Id:
        return self._process.pid

    def unique_id(self) -> Union[str, int]:
        return self._uid

    def state(self) -> State:
        return self._state

    def __repr__(self) -> str:
        r = "SrunJob(id={:d},state={:s})".format(self.id(), str(self._state))
        return r


class SlurmGlobalScheduler(DirectScheduler[SrunJob]):
    # always compile regular expressions to enforce ASCII matching
    # allow matching things like version 1.2.3-rc4
    _srun_version_regexp = r"slurm (\d+)[.](\d+)[.](\d+\S*)"
    _srun_version_pattern = re.compile(_srun_version_regexp, re.ASCII)
    _sbatch_job_id_regexp = r"(\d+)"
    _sbatch_job_id_pattern = re.compile(_sbatch_job_id_regexp, re.ASCII)
    _sacct_line_regexp = r"(\d+)([+]0[.]batch)?[|](\w+)"
    _sacct_line_pattern = re.compile(_sacct_line_regexp, re.ASCII)
    _use_het_prefix: bool = False

    @classmethod
    def _name_impl(cls) -> str:
        return "slurm"

    @classmethod
    def _is_available_impl(cls) -> Tuple[bool, Union[str, Tuple[str, str]]]:
        srun_path = shutil.which("srun")
        if srun_path is None:
            return False, "srun executable not found"

        srun = subprocess.run(
            [srun_path, "--version"],
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if srun.returncode != 0:
            return False, "failed to execute %s: %s" % (srun_path, srun.stderr)

        # do not use pattern.fullmatch() because of the newline at the end of
        # the output
        match = cls._srun_version_pattern.match(srun.stdout)
        if match is None:
            e = "srun output '{:s}' does not match expected format"
            raise ValueError(e.format(srun.stdout))

        version_major = int(match.group(1))
        # the minor version might be of the form `05` but the Python int()
        # function handles this correctly
        version_minor = int(match.group(2))
        version_patch = int(match.group(3))

        if version_major < 19 or (version_major == 19 and version_minor < 5):
            logging.warn(
                "Melissa has not been tested with Slurm versions older than 19.05.5", RuntimeWarning
            )

        if version_major < 17 or (version_major == 17 and version_minor < 11):
            return (
                False,
                (
                    "Expected at least Slurm 17.11, got"
                    f"{version_major}.{version_minor}.{version_patch}"
                    "which does not support heterogeneous jobs"
                ),
            )

        cls._use_het_prefix = version_major >= 20

        version_str = srun.stdout[match.span(1)[0] : match.span(3)[1]]
        return True, (srun_path, version_str)

    def __init__(self) -> None:
        is_available, info = self.is_available()
        if not is_available:
            raise RuntimeError("Slurm unavailable: %s" % (info,))

        assert self._use_het_prefix is not None

    def _sanity_check_impl(self, options: Options) -> List[str]:
        args = options.raw_arguments
        errors = []

        for a in args:
            if a[0] != "-":
                errors.append("non-option argument '{:s}' detected".format(a))
            elif a in ["-n", "--ntasks", "--test-only"]:
                errors.append("remove '{:s}' argument".format(a))

        command = ["srun", "--test-only", "--ntasks=1"] + args + ["--", "true"]
        srun = subprocess.run(
            command,
            stdin=subprocess.DEVNULL,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if srun.returncode != 0:
            e = "srun error on trial execution: {:s}".format(srun.stderr)
            errors.append(e)

        return errors

    def _submit_job_impl(
        self,
        commands: List[ArgumentList],
        env: Environment,
        options: Options,
        name: str,
        unique_id: int,
    ) -> Tuple[ArgumentList, Environment]:
        # Approach to environment variables:
        # By default all environment variables are propagated
        srun_env = os.environ.copy()
        srun_env.update(env)

        output_filename = "./stdout/job.{:d}.{:s}.out".format(unique_id, name)
        error_filename = "./stdout/job.{:d}.{:s}.err".format(unique_id, name)

        # build propagated options of the srun command line
        propagated_options = [
            "--output={:s}".format(output_filename),
            "--error={:s}".format(error_filename),
        ] + options.raw_arguments

        sched_cmd = options.sched_cmd
        sched_cmd_opt = options.sched_cmd_opt

        assert len(commands) == 1, "non-unit groups are not supported in this configuration"
        srun_arguments: List[List[str]] = []
        if not sched_cmd:
            srun_arguments = [commands[0]]
        else:
            srun_arguments = [["--"], commands[0]]

        def args2str(args: List[str]) -> str:
            return "" + "".join(args)
        logging.debug(f"srun_arguments: {srun_arguments}")
        srun_arguments_str = [args2str(args) for args in srun_arguments]
        logging.debug(f"srun_arguments_str: {srun_arguments_str}")

        # write srun/job execution call
        if not sched_cmd:
            srun_call = srun_arguments_str
        else:
            srun_call = [sched_cmd] + sched_cmd_opt + propagated_options + srun_arguments_str

        return srun_call, srun_env

    def _make_job_impl(self, proc: "Process[str]", unique_id: int) -> SrunJob:
        return SrunJob(unique_id, proc)

    @classmethod
    def _update_jobs_impl(cls, jobs: List[SrunJob]) -> None:
        for j in jobs:
            returncode = j._process.poll()
            if returncode is None:
                state = State.RUNNING
            elif returncode == 0:
                state = State.TERMINATED
            else:
                state = State.FAILED
            j._state = state

    @classmethod
    def _cancel_jobs_impl(cls, jobs: List[SrunJob]) -> None:
        # when the user presses ctrl+c, the shell will send all processes in
        # the same process group SIGINT. some programs respond intelligently to
        # signals by freeing ressources and exiting. these programs may also
        # exit _immediately_ if they receive a second signal within a short
        # time frame (e.g., srun or mpirun which won't terminate its child
        # processes in this case). for this reason, we wait before terminating
        # jobs here.
        max_wait_time = time.Time(seconds=5)

        # wait at most max_wait_time overall
        def compute_timeout(t_start: time.Time) -> time.Time:
            t_waited = time.monotonic() - t_start
            if t_waited < max_wait_time:
                return max_wait_time - t_waited
            return time.Time(seconds=0)

        # terminate processes
        t_0 = time.monotonic()
        for j in jobs:
            try:
                timeout = compute_timeout(t_0)
                j._process.wait(timeout.total_seconds())
            except subprocess.TimeoutExpired:
                logging.debug("Slurm srun scheduler terminating process %d", j.id())
                j._process.terminate()

        # kill processes if necessary
        t_1 = time.monotonic()
        for j in jobs:
            try:
                timeout = compute_timeout(t_1)
                j._process.wait(timeout.total_seconds())
            except subprocess.TimeoutExpired:
                logging.debug("Slurm srun scheduler killing process %d", j.id())
                j._process.kill()

            j._state = State.FAILED
