#!/usr/bin/python3

# Copyright (c) 2020-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import json
import logging
import os
import re
import shutil
import subprocess
from typing import List, Tuple, Union, Optional, Dict
from pathlib import Path

from melissa.utility.process import ArgumentList, CompletedProcess, Environment

from .job import Job, State
from .scheduler import IndirectScheduler, Options

logger = logging.getLogger(__name__)


class OarJob(Job):
    def __init__(self, job_uid: Union[str, int], job_id: int, queue: str) -> None:
        super(OarJob, self).__init__()
        self._uid = job_uid
        self._id = job_id
        self._state = State.WAITING
        self.queue = queue

    def id(self) -> int:
        return self._id

    def unique_id(self) -> Union[str, int]:
        return self._uid

    def state(self) -> State:
        return self._state

    def __repr__(self) -> str:
        return f"OarJob(uid={self._uid},id={self._id},state={self._state})"

    def set_state(self, new_state: State) -> None:
        self._state = new_state


class OarContainer:
    def __init__(
        self,
        container_options: List[str],
        container_client_size: int,
        besteffort_alloc_freq: int
    ) -> None:
        # general attributes
        self.container_job: Optional[OarJob] = None
        self.options: List[str] = container_options
        self.container_client_size: int = container_client_size
        self.besteffort_alloc_freq: int = besteffort_alloc_freq


class OarHybridScheduler(IndirectScheduler[OarJob]):
    # always compile regular expressions to enforce ASCII matching
    # allow matching things like version 1.2.3-rc4
    # there is a space in front of the colon in version 2.5.8
    _oarsub_version_regexp = r"OAR version\s*: (\d+)[.](\d+)[.](\d+\S*)"
    _oarsub_version_pattern = re.compile(_oarsub_version_regexp, re.ASCII)

    _oarsub_job_id_regexp = r"OAR_JOB_ID=(\d+)"
    _oarsub_job_id_pattern = re.compile(_oarsub_job_id_regexp, re.ASCII)

    @classmethod
    def _name_impl(cls) -> str:
        return "oar"

    @classmethod
    def _is_available_impl(cls) -> Tuple[bool, Union[str, Tuple[str, str]]]:
        oarsub_path = shutil.which("oarsub")
        if oarsub_path is None:
            return False, "oarsub executable not found"

        oarsub = subprocess.run(
            [oarsub_path, "--version"],
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if oarsub.returncode != 0:
            return False, "failed to execute %s: %s" % (oarsub_path, oarsub.stderr)

        # do not use pattern.fullmatch() because of the newline at
        # the end of the oarsub output and because the release name
        # will be printed after the version number.
        match = cls._oarsub_version_pattern.match(oarsub.stdout)
        if match is None:
            e = "oarsub output '{:s}' does not match expected format"
            raise RuntimeError(e.format(oarsub.stdout))

        version_str = oarsub.stdout[match.span(1)[0] : match.span(3)[1]]
        return True, (oarsub_path, version_str)

    def __init__(
        self,
        mpi_provider: str,
        container_options: List[str],
        container_client_size: int,
        besteffort_alloc_freq: int
    ) -> None:
        is_available, info = self.is_available()
        if not is_available:
            raise RuntimeError("OAR unavailable: {:s}".format(info))

        if mpi_provider not in ["openmpi"]:
            raise ValueError("unknown MPI implementation '%s'" % mpi_provider)

        super().__init__()
        self.mpi_provider = mpi_provider

        # initialize attributes related to the HybridAllocation
        self.container = OarContainer(
            container_options,
            container_client_size,
            besteffort_alloc_freq
        )

        # list of client uids submitted in the container
        self.container_uid: List[int] = []
        # list of submitted and terminated clients
        self.submitted_client_uid: List[int] = []
        self.terminated_client_uid: List[int] = []
        # server uid and client counter
        self.server_uid: int = 0
        self.client_ctr: int = 0

    def _submit_job_impl(
        self,
        commands: List[ArgumentList],
        env: Environment,
        options: Options,
        name: str,
        uid: int,
    ) -> Tuple[ArgumentList, Environment]:
        if self.container.container_job is None:
            self._submit_job_impl_container()
        if name == "melissa-server":
            self.server_uid = uid
            return self._submit_job_impl_server(commands, env, options, name, uid)
        else:
            self.client_ctr += 1
            return self._submit_job_impl_client(commands, env, options, name, uid)

    def _submit_job_impl_container(self):
        def opt2cmd(raw_arguments: List[str]) -> Tuple[str, List[str]]:
            """
            this function scans all client/server options to derive the resource and
            submission commands.
            """
            contr_resource_cmd = ""
            contr_submission_opt = []
            for o in raw_arguments:
                if o[:2] in ["-t", "-p", "-q"]:
                    contr_submission_opt += [o[:2], o[3:]]
                else:
                    contr_resource_cmd += o
            return contr_resource_cmd, contr_submission_opt

        # get container resource and options
        contr_resource_cmd, contr_submission_opt = opt2cmd(self.container.options)
        contr_resource_cmd = ["-l"] + [contr_resource_cmd]

        # build the submission command
        script_filename = "dummy_script.sh"
        # oarsub cannot handle single quotes!
        contr_submission_opt = [
            "-t",
            "container",
            "-t",
            "cosystem",
        ] + contr_submission_opt

        # create dummy sleeping script to keep the container alive
        stdout_filename = "./stdout/oar.container.out"
        stderr_filename = "./stdout/oar.container.err"
        with open(script_filename, "w") as f:
            print("#!/bin/sh", file=f)
            print("#OAR -O %s" % stdout_filename, file=f)
            print("#OAR -E %s" % stderr_filename, file=f)
            print("echo \"DATE                      =$(date)\"", file=f)
            print("echo \"Hostname                  =$(hostname -s)\"", file=f)
            print("echo \"Working directory         =$(pwd)\"", file=f)
            print("echo \"\"", file=f)
            print("sleep $(($OAR_JOB_WALLTIME_SECONDS-60))", file=f)
        os.chmod(script_filename, 0o744)

        maybe_job_name = ["--name", "melissa-container"]

        oar_command = (
            [
                "oarsub",
                "--scanscript",
                "--directory={:s}".format(os.getcwd()),
            ]
            + contr_submission_opt
            + contr_resource_cmd
            + maybe_job_name
            + ["--", os.path.abspath(script_filename)]
        )

        # since container jobs are specific to this scheduler it must
        # be launched manually from here
        logger.debug(f"launching {oar_command}")
        with open(stdout_filename, "w") as stdout:
            with open(stderr_filename, "w") as stderr:
                proc = subprocess.Popen(
                    args=oar_command,
                    env=os.environ,
                    stdin=subprocess.DEVNULL,
                    stdout=stdout,
                    stderr=stderr,
                    universal_newlines=True,
                )
        logger.debug("submitting job uid container")
        proc.wait()

        # the corresponding CompletedProcess and Job are made here too
        with open(stdout_filename, "r") as f:
            stdout = f.read()
        with open(stderr_filename, "r") as f:
            stderr = f.read()
        oarsub = CompletedProcess(exit_status=proc.returncode, stdout=stdout, stderr=stderr)
        self.container.container_job = self._make_job_impl(oarsub, "container")
        logger.debug(
            f"job submission succeeded (UID container ID {self.container.container_job.id()})"
        )

    def _submit_job_impl_server(
        self,
        commands: List[ArgumentList],
        env: Environment,
        options: Options,
        name: str,
        uid: int,
    ) -> Tuple[ArgumentList, Environment]:
        sched_cmd = options.sched_cmd
        sched_cmd_opt = options.sched_cmd_opt

        if self.mpi_provider == "openmpi":
            machinefile_arg = ["-machinefile", '"$OAR_NODE_FILE"']
        else:
            fmt = "OAR scheduler implementation for MPI provider '{:s}' missing"
            raise NotImplementedError(fmt.format(self.mpi_provider))

        # assemble mpirun arguments
        mpirun_args = []

        # gather environment variables
        oar_env: Dict = {}
        oar_env.update(env)

        for key in ["LD_LIBRARY_PATH", "PATH"]:
            if key not in oar_env and key in os.environ:
                oar_env[key] = os.environ[key]

        if oar_env:
            env_args = ["env"] + ["%s=%s" % (key, oar_env[key]) for key in oar_env]
        else:
            env_args = []

        for i, cmd in enumerate(commands):
            args = sched_cmd_opt + ["--"] + env_args + cmd
            args_str = " ".join(args)
            mpirun_args.append(args_str)

        mpirun_command = (
            "exec " + sched_cmd
            + " "
            + " ".join(machinefile_arg)
            + " \\\n  "
            + " : \\\n  ".join(mpirun_args)
        )

        def opt2cmd(raw_arguments: List[str]) -> Tuple[str, List[str]]:
            """
            this function scans all client/server options to derive the resource and
            submission commands.
            """
            oar_resource_cmd = ""
            oar_submission_opt = []
            for o in raw_arguments:
                if o[:2] in ["-t", "-p", "-q"]:
                    oar_submission_opt += [o[:2], o[3:]]
                else:
                    oar_resource_cmd += o
            return oar_resource_cmd, oar_submission_opt

        server_resource_cmd, server_submission_opt = opt2cmd(options.raw_arguments)

        Path('./oarsub').mkdir(parents=True, exist_ok=True)
        script_filename = "./oarsub/oarsub.%d.sh" % uid
        # oarsub cannot handle single quotes!
        with open(script_filename, "w") as f:
            print("#!/bin/sh", file=f)
            print("#OAR -O ./stdout/oar.%d.out" % uid, file=f)
            print("#OAR -E ./stdout/oar.%d.err" % uid, file=f)
            print(mpirun_command, file=f)
        os.chmod(script_filename, 0o744)

        maybe_job_name = ["--name", name] if name else []

        assert self.container.container_job

        oar_command = (
            [
                "oarsub",
                "--scanscript",
                "--directory={:s}".format(os.getcwd()),
            ]
            + ["-t", "inner=" + str(self.container.container_job.id())]
            + server_submission_opt
            + ["-l", server_resource_cmd]
            + maybe_job_name
            + ["--", os.path.abspath(script_filename)]
        )

        return oar_command, os.environ

    def _submit_job_impl_client(
        self,
        commands: List[ArgumentList],
        env: Environment,
        options: Options,
        name: str,
        uid: int,
    ) -> Tuple[ArgumentList, Environment]:
        sched_cmd = options.sched_cmd
        sched_cmd_opt = options.sched_cmd_opt

        if self.mpi_provider == "openmpi":
            machinefile_arg = ["-machinefile", '"$OAR_NODE_FILE"']
        else:
            fmt = "OAR scheduler implementation for MPI provider '{:s}' missing"
            raise NotImplementedError(fmt.format(self.mpi_provider))

        # assemble mpirun arguments
        mpirun_args = []

        # gather environment variables
        oar_env: Dict[str, str] = {}
        oar_env.update(env)

        for key in ["LD_LIBRARY_PATH", "PATH"]:
            if key not in oar_env and key in os.environ:
                oar_env[key] = os.environ[key]

        if oar_env:
            env_args = ["env"] + ["%s=%s" % (key, oar_env[key]) for key in oar_env]
        else:
            env_args = []

        for cmd in commands:
            args = sched_cmd_opt + ["--"] + env_args + cmd
            args_str = " ".join(args)
            mpirun_args.append(args_str)

        mpirun_command = (
            "exec " + sched_cmd
            + " "
            + " ".join(machinefile_arg)
            + " \\\n  "
            + " : \\\n  ".join(mpirun_args)
        )

        def opt2cmd(raw_arguments: List[str]) -> Tuple[str, List[str]]:
            """
            this function scans all client/server options to derive the resource and
            submission commands.
            """
            oar_resource_cmd = ""
            oar_submission_opt = []
            for o in raw_arguments:
                if o[:2] in ["-t", "-p", "-q"]:
                    oar_submission_opt += [o[:2], o[3:]]
                else:
                    oar_resource_cmd += o
            return oar_resource_cmd, oar_submission_opt

        client_resource_cmd, client_submission_opt = opt2cmd(options.raw_arguments)

        Path('./oarsub').mkdir(parents=True, exist_ok=True)
        script_filename = "./oarsub/oarsub.%d.sh" % uid
        # oarsub cannot handle single quotes!
        with open(script_filename, "w") as f:
            print("#!/bin/sh", file=f)
            print("#OAR -O ./stdout/oar.%d.out" % uid, file=f)
            print("#OAR -E ./stdout/oar.%d.err" % uid, file=f)
            print(mpirun_command, file=f)
        os.chmod(script_filename, 0o744)

        maybe_job_name = ["--name", name] if name else []

        # queue selection conditions depend on the launching phase (1st or 2nd) which is inferred
        # from the size of the terminated client list (=0 => 1st phase >0 => 2nd phase)
        # -> in the 1st phase: is the submission counter a multiple of the frequency parameter ?
        # -> in the 2nd phase: are there less clients in the container than there is room in it ?
        fst_cond = (
            len(self.terminated_client_uid) == 0
            and self.client_ctr % self.container.besteffort_alloc_freq == 0
        )
        n_client_in_cont = len(
            [uid for uid in self.container_uid if uid not in self.terminated_client_uid]
        )
        snd_cond = (
            len(self.terminated_client_uid) > 0
            and n_client_in_cont >= self.container.container_client_size
        )
        if fst_cond or snd_cond:
            logger.debug(f"first condition: {fst_cond}, second condition: {snd_cond}")
            oar_command = (
                [
                    "oarsub",
                    "--scanscript",
                    "--directory={:s}".format(os.getcwd()),
                ]
                + ["-t", "besteffort"]
                + client_submission_opt
                + ["-l", client_resource_cmd]
                + maybe_job_name
                + ["--", os.path.abspath(script_filename)]
            )
        else:
            assert self.container.container_job
            self.container_uid.append(uid)
            oar_command = (
                [
                    "oarsub",
                    "--scanscript",
                    "--directory={:s}".format(os.getcwd()),
                ]
                + ["-t", "inner=" + str(self.container.container_job.id())]
                + client_submission_opt
                + ["-l", client_resource_cmd]
                + maybe_job_name
                + ["--", os.path.abspath(script_filename)]
            )

        return oar_command, os.environ

    def _make_job_impl(self, oarsub: CompletedProcess, uid: Union[int, str]) -> OarJob:
        if oarsub.stderr != "":
            logger.warning(f"oarsub (uid={uid}): {oarsub.stderr}")
        if oarsub.exit_status != 0:
            raise RuntimeError("oarsub failed (exit status {oarsub.exit_status})")

        # do not match because oarsub may print lines starting with
        # [ADMISSION RULE]
        match = self._oarsub_job_id_pattern.search(oarsub.stdout)
        if match is None:
            raise ValueError("no job id found in oarsub output: '{:s}'".format(oarsub.stdout))

        if type(uid) is str or uid == self.server_uid:
            return OarJob(uid, int(match.group(1)), "default")
        else:
            self.submitted_client_uid.append(int(uid))
            return OarJob(
                uid,
                int(match.group(1)),
                "default" if uid in self.container_uid else "best-effort",
            )

    def _update_jobs_impl(self, jobs: List[OarJob]) -> Tuple[ArgumentList, Environment]:
        assert self.container.container_job
        job_args = ["--job={:d}".format(j.id()) for j in jobs + [self.container.container_job]]
        # `oarstat --state` does not distinguish between failed and successful
        # jobs
        return ["oarstat", "--full", "--json"] + job_args, os.environ

    def _parse_update_jobs_impl(self, jobs: List[OarJob], oarstat: CompletedProcess) -> None:
        states_map = {
            "Error": State.ERROR,
            "Finishing": State.TERMINATED,
            "Hold": State.WAITING,
            "Launching": State.WAITING,
            "Running": State.RUNNING,
            "Terminated": State.TERMINATED,
            "toAckReservation": State.WAITING,
            "toLaunch": State.WAITING,
            "Waiting": State.WAITING,
        }

        info = json.loads(oarstat.stdout)

        assert self.container.container_job
        for j in jobs + [self.container.container_job]:
            key = str(j.id())
            if key not in info:
                continue

            oar_state = info[key]["state"]
            new_state = states_map.get(oar_state)
            if new_state is None:
                fmt = "unknown OAR job state '{:s}'"
                raise RuntimeError(fmt.format(oar_state))

            if new_state == State.TERMINATED:
                assert "exit_code" in info[key]
                # when a BESTEFFORT_KILL event is trigerred by OAR
                # the state becomes finishing with a null (i.e. None) exit_code
                if info[key]["exit_code"] is not None and int(info[key]["exit_code"]) == 0:
                    j.set_state(State.TERMINATED)
                    self.terminated_client_uid.append(int(j.unique_id()))
                else:
                    if info[key]["exit_code"] is None:
                        logger.debug(f"best-effort job {j.id()} was killed")
                    j.set_state(State.FAILED)
            elif new_state == State.ERROR and j.queue == "best-effort":
                # State.Failed will result in a job resubmission request from the server
                logger.debug(f"best-effort job {j.id()} was killed")
                j.set_state(State.FAILED)
            else:
                j.set_state(new_state)

    def _cancel_jobs_impl(self, jobs: List[OarJob]) -> Tuple[ArgumentList, Environment]:
        jobs_str = ["{:d}".format(job.id()) for job in jobs]
        if self.server_uid in [job.unique_id() for job in jobs]:
            assert self.container.container_job
            logger.debug("Cancelling container")
            jobs_str.append(str(self.container.container_job.id()))
            self.container.container_job = None
        return ["oardel"] + jobs_str, os.environ

    def _parse_cancel_jobs_impl(self, jobs: List[OarJob], proc: CompletedProcess) -> None:
        job_already_killed_code = 6
        if proc.exit_status not in [0, job_already_killed_code]:
            raise RuntimeError("oardel error: exit status {:d}".format(proc.exit_status))
