#!/usr/bin/python3

# Copyright (c) 2020-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import rapidjson
import logging
import os
import re
import shutil
import subprocess
from typing import List, Tuple, Union, Dict
from pathlib import Path

from melissa.utility.process import ArgumentList, CompletedProcess, Environment

from .job import Job, State
from .scheduler import IndirectScheduler, Options

logger = logging.getLogger(__name__)


class OarJob(Job):
    def __init__(self, job_id: int) -> None:
        super(OarJob, self).__init__()
        self._id = job_id
        self._state = State.WAITING

    def id(self) -> int:
        return self._id

    def unique_id(self) -> Union[str, int]:
        return self._id

    def state(self) -> State:
        return self._state

    def __repr__(self) -> str:
        return "OarJob(id={:d},state={:s})".format(self._id, str(self._state))

    def set_state(self, new_state: State) -> None:
        self._state = new_state


class OarScheduler(IndirectScheduler[OarJob]):
    # always compile regular expressions to enforce ASCII matching
    # allow matching things like version 1.2.3-rc4
    # there is a space in front of the colon in version 2.5.8
    _oarsub_version_regexp = r"OAR version\s*: (\d+)[.](\d+)[.](\d+\S*)"
    _oarsub_version_pattern = re.compile(_oarsub_version_regexp, re.ASCII)

    _oarsub_job_id_regexp = r"OAR_JOB_ID=(\d+)"
    _oarsub_job_id_pattern = re.compile(_oarsub_job_id_regexp, re.ASCII)

    @classmethod
    def _name_impl(cls) -> str:
        return "oar"

    @classmethod
    def _is_available_impl(cls) -> Tuple[bool, Union[str, Tuple[str, str]]]:
        oarsub_path = shutil.which("oarsub")
        if oarsub_path is None:
            return False, "oarsub executable not found"

        oarsub = subprocess.run(
            [oarsub_path, "--version"],
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if oarsub.returncode != 0:
            return False, "failed to execute %s: %s" % (oarsub_path, oarsub.stderr)

        # do not use pattern.fullmatch() because of the newline at
        # the end of the oarsub output and because the release name
        # will be printed after the version number.
        match = cls._oarsub_version_pattern.match(oarsub.stdout)
        if match is None:
            e = "oarsub output '{:s}' does not match expected format"
            raise RuntimeError(e.format(oarsub.stdout))

        version_str = oarsub.stdout[match.span(1)[0] : match.span(3)[1]]
        return True, (oarsub_path, version_str)

    def __init__(self, mpi_provider: str) -> None:
        is_available, info = self.is_available()
        if not is_available:
            raise RuntimeError("OAR unavailable: {:s}".format(info))

        super().__init__()
        self.mpi_provider = mpi_provider

    def _submit_job_impl(
        self,
        commands: List[ArgumentList],
        env: Environment,
        options: Options,
        name: str,
        uid: int,
    ) -> Tuple[ArgumentList, Environment]:
        sched_cmd = options.sched_cmd
        sched_cmd_opt = options.sched_cmd_opt

        if self.mpi_provider == "openmpi":
            machinefile_arg = ["-machinefile", '"$OAR_NODE_FILE"']
        else:
            fmt = "OAR scheduler implementation for MPI provider '{:s}' missing"
            raise NotImplementedError(fmt.format(self.mpi_provider))

        # assemble mpirun arguments
        mpirun_args = []

        # gather environment variables
        oar_env: Dict = {}
        oar_env.update(env)

        for key in ["LD_LIBRARY_PATH", "PATH"]:
            if key not in oar_env and key in os.environ:
                oar_env[key] = os.environ[key]

        if oar_env:
            env_args = ["env"] + ["%s=%s" % (key, oar_env[key]) for key in oar_env]
        else:
            env_args = []

        for i, cmd in enumerate(commands):
            args = sched_cmd_opt + ["--"] + env_args + cmd
            args_str = " ".join(args)
            mpirun_args.append(args_str)

        mpirun_command = (
            "exec " + sched_cmd
            + " "
            + " ".join(machinefile_arg)
            + " \\\n  "
            + " : \\\n  ".join(mpirun_args)
        )

        def opt2cmd(raw_arguments: List[str]) -> Tuple[str, List[str]]:
            """
            this function scans all client/server options to derive the resource and
            submission commands.
            """
            oar_resource_cmd = "#OAR --resource "
            oar_submission_opt = []
            for o in raw_arguments:
                if o[:2] in ["-t", "-p", "-q"]:
                    oar_submission_opt += [o[:2], o[3:]]
                else:
                    oar_resource_cmd += o
            return oar_resource_cmd, oar_submission_opt

        Path('./oarsub').mkdir(parents=True, exist_ok=True)
        script_filename = "./oarsub/oarsub.%d.sh" % uid
        # oarsub cannot handle single quotes!
        oar_resource_cmd, oar_submission_opt = opt2cmd(options.raw_arguments)
        with open(script_filename, "w") as f:
            print("#!/bin/sh", file=f)
            print("#OAR -O ./stdout/oar.%d.out" % uid, file=f)
            print("#OAR -E ./stdout/oar.%d.err" % uid, file=f)
            print(oar_resource_cmd, file=f)
            print(mpirun_command, file=f)
        os.chmod(script_filename, 0o744)

        maybe_job_name = ["--name", name] if name else []

        oar_command = (
            [
                "oarsub",
                "--scanscript",
                "--directory={:s}".format(os.getcwd()),
            ]
            + oar_submission_opt
            + maybe_job_name
            + ["--", os.path.abspath(script_filename)]
        )

        return oar_command, os.environ

    def _make_job_impl(self, oarsub: CompletedProcess, uid: int) -> OarJob:
        if oarsub.stderr != "":
            logger.warning("oarsub (uid=%d): %s", uid, oarsub.stderr)
        if oarsub.exit_status != 0:
            raise RuntimeError("oarsub failed (exit status {:d})".format(oarsub.exit_status))

        # do not match because oarsub may print lines starting with
        # [ADMISSION RULE]
        match = self._oarsub_job_id_pattern.search(oarsub.stdout)
        if match is None:
            raise ValueError("no job id found in oarsub output: '{:s}'".format(oarsub.stdout))

        return OarJob(int(match.group(1)))

    def _update_jobs_impl(self, jobs: List[OarJob]) -> Tuple[ArgumentList, Environment]:
        job_args = ["--job={:d}".format(j.id()) for j in jobs]
        # `oarstat --state` does not distinguish between failed and successful
        # jobs
        return ["oarstat", "--full", "--json"] + job_args, os.environ

    def _parse_update_jobs_impl(self, jobs: List[OarJob], oarstat: CompletedProcess) -> None:
        states_map = {
            "Error": State.ERROR,
            "Finishing": State.TERMINATED,
            "Hold": State.WAITING,
            "Launching": State.WAITING,
            "Running": State.RUNNING,
            "Terminated": State.TERMINATED,
            "toAckReservation": State.WAITING,
            "toLaunch": State.WAITING,
            "Waiting": State.WAITING,
        }

        info = rapidjson.loads(oarstat.stdout)

        for j in jobs:
            key = str(j.id())
            if key not in info:
                continue

            oar_state = info[key]["state"]
            new_state = states_map.get(oar_state)
            if new_state is None:
                fmt = "unknown OAR job state '{:s}'"
                raise RuntimeError(fmt.format(oar_state))

            if new_state == State.TERMINATED:
                assert "exit_code" in info[key]
                if int(info[key]["exit_code"]) == 0:
                    j.set_state(State.TERMINATED)
                else:
                    j.set_state(State.FAILED)
            else:
                j.set_state(new_state)

    def _cancel_jobs_impl(self, jobs: List[OarJob]) -> Tuple[ArgumentList, Environment]:
        jobs_str = ["{:d}".format(job.id()) for job in jobs]
        return ["oardel"] + jobs_str, os.environ

    def _parse_cancel_jobs_impl(self, jobs: List[OarJob], proc: CompletedProcess) -> None:
        job_already_killed_code = 6
        if proc.exit_status not in [0, job_already_killed_code]:
            raise RuntimeError("oardel error: exit status {:d}".format(proc.exit_status))
