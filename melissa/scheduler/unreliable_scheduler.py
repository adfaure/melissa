#!/usr/bin/python3

# Copyright (c) 2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from random import Random
from typing import List, Optional, Tuple, Union

from .scheduler import IndirectScheduler, Options
from .slurm import SlurmJob, SlurmScheduler
from melissa.utility.time import Time
from melissa.utility.process import ArgumentList, CompletedProcess, Environment


class UnreliableScheduler(IndirectScheduler[SlurmJob]):
    """
    This class executes the commands of an indirect scheduler with a fixed
    delay for testing purposes (i.e., for testing asynchronicity).
    """

    min_delay = Time(seconds=1)
    max_delay = Time(seconds=5)
    fail_probability = 0.1

    @classmethod
    def _name_impl(cls) -> str:
        return "unreliable-scheduler"

    @classmethod
    def _is_available_impl(cls) -> Tuple[bool, Union[str, Tuple[str, str]]]:
        return SlurmScheduler.is_available()

    def __init__(self, seed: Optional[int] = None) -> None:
        self._rng = Random()
        self._rng.seed(seed)
        self._impl = SlurmScheduler()

    def _sanity_check_impl(self, options: Options) -> List[str]:
        return self._impl.sanity_check(options)

    def _modify_args(self, args: ArgumentList) -> ArgumentList:
        assert self.fail_probability >= 0
        assert self.fail_probability <= 1

        min_delay_s = self.min_delay.total_seconds()
        max_delay_s = self.max_delay.total_seconds()
        sleep_before = self._rng.uniform(min_delay_s / 2, max_delay_s / 2)
        sleep_after = self._rng.uniform(min_delay_s / 2, max_delay_s / 2)

        if self._rng.random() <= self.fail_probability:
            ret = self._rng.randint(2, 127)
            args = ["exit", str(ret)]

        def sleep(t_sec: float) -> ArgumentList:
            return ["sleep", str(t_sec)]

        args_mod = sleep(sleep_before) + ["&&"] + args + ["&&"] + sleep(sleep_after)

        return ["sh", "-c", " ".join(args_mod)]

    def _submit_job_impl(
        self, commands: List[ArgumentList], env: Environment, options: Options, name: str, uid: int
    ) -> Tuple[ArgumentList, Environment]:
        args, env = self._impl._submit_job_impl(commands, env, options, name, uid)
        return self._modify_args(args), env

    def _make_job_impl(self, proc: CompletedProcess, uid: int) -> SlurmJob:
        return self._impl.make_job(proc, uid)

    def _update_jobs_impl(self, jobs: List[SlurmJob]) -> Tuple[ArgumentList, Environment]:
        args, env = self._impl.update_jobs(jobs)
        return self._modify_args(args), env

    def _parse_update_jobs_impl(self, jobs: List[SlurmJob], proc: CompletedProcess) -> None:
        self._impl.parse_update_jobs(jobs, proc)

    def _cancel_jobs_impl(self, jobs: List[SlurmJob]) -> Tuple[ArgumentList, Environment]:
        args, env = self._impl.cancel_jobs(jobs)
        return self._modify_args(args), env

    def _parse_cancel_jobs_impl(self, jobs: List[SlurmJob], proc: CompletedProcess) -> None:
        self._impl.parse_cancel_jobs(jobs, proc)
