#!/usr/bin/python3

# Copyright (c) 2020-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import logging
import os
import re
import shutil
import subprocess
from typing import Dict, List, Optional, Tuple, Union
from pathlib import Path

from melissa.utility.process import ArgumentList, CompletedProcess, Environment

from .job import Id, Job, State
from .scheduler import IndirectScheduler, Options

logger = logging.getLogger(__name__)


class SlurmJob(Job):
    def __init__(self, job_id: Id) -> None:
        self._id = job_id
        self._state = State.WAITING

    def id(self) -> Id:
        return self._id

    def unique_id(self) -> Union[str, int]:
        return self._id

    def state(self) -> State:
        return self._state

    def __repr__(self) -> str:
        r = "SlurmJob(id={:d},state={:s})".format(self.id(), str(self.state()))
        return r


class SlurmScheduler(IndirectScheduler[SlurmJob]):
    # always compile regular expressions to enforce ASCII matching
    # allow matching things like version 1.2.3-rc4
    _srun_version_regexp = r"slurm (\d+)[.](\d+)[.](\d+\S*)"
    _srun_version_pattern = re.compile(_srun_version_regexp, re.ASCII)
    _sbatch_job_id_regexp = r"(\d+)"
    _sbatch_job_id_pattern = re.compile(_sbatch_job_id_regexp, re.ASCII)
    _sacct_line_regexp = r"(\d+)([+]0[.]batch)?[|](\w+)"
    _sacct_line_pattern = re.compile(_sacct_line_regexp, re.ASCII)

    _use_het_prefix = None  # type: Optional[bool]

    @classmethod
    def _name_impl(cls) -> str:
        return "slurm"

    @classmethod
    def _is_available_impl(cls) -> Tuple[bool, Union[str, Tuple[str, str]]]:
        srun_path = shutil.which("srun")
        if srun_path is None:
            return False, "srun executable not found"

        srun = subprocess.run(
            [srun_path, "--version"],
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if srun.returncode != 0:
            return False, "failed to execute %s: %s" % (srun_path, srun.stderr)

        # do not use pattern.fullmatch() because of the newline at the end of
        # the output
        match = cls._srun_version_pattern.match(srun.stdout)
        if match is None:
            e = "srun output '{:s}' does not match expected format"
            raise ValueError(e.format(srun.stdout))

        version_major = int(match.group(1))
        # the minor version might be of the form `05` but the Python int()
        # function handles this correctly
        version_minor = int(match.group(2))
        version_patch = match.group(3)

        if version_major < 19 or (version_major == 19 and version_minor < 5):
            logger.warn(
                "Melissa has not been tested with Slurm versions older than 19.05.5", RuntimeWarning
            )

        if version_major < 17 or (version_major == 17 and version_minor < 11):
            return (
                False,
                "expected at least Slurm 17.11,"
                f"got {version_major}.{version_minor}.{version_patch}"
                "which does not support heterogeneous jobs",
            )

        cls._use_het_prefix = version_major >= 20

        version_str = srun.stdout[match.span(1)[0] : match.span(3)[1]]
        return True, (srun_path, version_str)

    def __init__(self) -> None:
        is_available, info = self.is_available()
        if not is_available:
            raise RuntimeError("Slurm unavailable: %s" % (info,))

        assert self._use_het_prefix is not None

    def _sanity_check_impl(self, options: Options) -> List[str]:
        args = options.raw_arguments
        errors = []

        for a in args:
            if a[0] != "-":
                errors.append("non-option argument '{:s}' detected".format(a))
            elif a in ["-n", "--ntasks", "--test-only"]:
                errors.append("remove '{:s}' argument".format(a))

        command = ["srun", "--test-only", "--ntasks=1"] + args + ["--", "true"]
        srun = subprocess.run(
            command,
            stdin=subprocess.DEVNULL,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if srun.returncode != 0:
            e = "srun error on trial execution: {:s}".format(srun.stderr)
            errors.append(e)

        return errors

    def _submit_job_impl(
        self, commands: List[ArgumentList], env: Environment, options: Options, name: str, uid: int
    ) -> Tuple[ArgumentList, Environment]:
        sbatch_env = os.environ.copy()
        sbatch_env.update(env)

        output_filename = "./stdout/job.{:d}.{:s}.out".format(uid, name)
        error_filename = "./stdout/job.{:d}.{:s}.err".format(uid, name)

        # sbatch Script and srun Arguments Assembly
        #
        # Command arguments are assembled initially as lists of lists of
        # strings (`List[List[str]]`) to avoid ambiguities, to allow sanity
        # checks, and to allow sanitizing the text later. The inner lists are
        # * lines in sbatch scripts
        # * hetgroup arguments on the srun command line.
        #
        # The actual strings are created in two steps:
        # * The inner lists are turned into strings with `#SBATCH`, `:`
        #   prefixes, back-slashes, and so on *without* newlines at the end.
        # * The list of strings is turned into a string and the missing
        #   newlines are added.
        #
        # The convention of leaving away newlines means that an empty string
        # will be replaced a newline.

        # assemble sbatch options
        #
        # the options below need to be specified only once; these are so-called
        # "propagated options" in the Slurm documentation
        # user-provided options are assumed to be non-propagated
        propagated_options = [
            "--output={:s}".format(output_filename),
            "--error={:s}".format(error_filename),
        ]

        hetjob_keyword = "hetjob" if self._use_het_prefix else "packjob"
        hetjob_options = []  # type: List[str]
        for i, _ in enumerate(commands):
            hetjob_options.extend(options.raw_arguments)
            if i + 1 < len(commands):
                hetjob_options.append(hetjob_keyword)

        sbatch_options = propagated_options + hetjob_options

        # serialize sbatch options
        def options2str(options: str) -> str:
            return "#SBATCH " + options

        sbatch_options_str = [options2str(o) for o in sbatch_options]

        # assemble srun arguments
        hetgroup_keyword = "het-group" if self._use_het_prefix else "pack-group"

        sched_cmd = options.sched_cmd
        sched_cmd_opt = options.sched_cmd_opt

        srun_arguments: List[List[str]] = []
        if not sched_cmd:
            assert len(commands) == 1, "non-unit groups are not supported in this configuration"
            srun_arguments = [commands[0]]
        else:
            if len(commands) == 1:
                srun_arguments = [sched_cmd_opt + ["--"] + commands[0]]
            else:
                for i, cmd in enumerate(commands):
                    args = [" ".join(sched_cmd_opt), f"--{hetgroup_keyword}={i}", "--"] + cmd
                    srun_arguments.append(args)

        # serialize srun arguments
        def args2str(hetgroup: int, args: List[str]) -> str:
            assert hetgroup >= 0
            assert hetgroup < len(commands)

            prefix = ": " if hetgroup > 0 else ""
            suffix = " \\" if hetgroup + 1 < len(commands) else ""
            return "  " + prefix + " ".join(args) + suffix

        srun_arguments_str = [args2str(i, args) for i, args in enumerate(srun_arguments)]

        # write srun calls to file
        Path('./sbatch').mkdir(parents=True, exist_ok=True)
        sbatch_script_filename = "./sbatch/sbatch.{:d}.sh".format(uid)
        sbatch_script = (
            ["#!/bin/sh"]
            + ["# sbatch script for job {:s}".format(name)]
            + sbatch_options_str
            + [""]
            + (["exec \\"] if not sched_cmd else [f"exec {options.sched_cmd} \\"])
            + srun_arguments_str
        )

        # POSIX requires files to end with a newline; missing newlines at the
        # end of a file may break scripts that append text.
        # this string won't contain a newline at the end; it must be added
        # manually or by using a function that adds it, e.g., `print`
        sbatch_script_str_noeol = "\n".join(sbatch_script)

        with open(sbatch_script_filename, "w") as f:
            print(sbatch_script_str_noeol, file=f)

        sbatch_call = (
            ["sbatch"]
            + ["--parsable"]
            + ["--job-name={:s}".format(name)]
            + [sbatch_script_filename]
        )

        return sbatch_call, sbatch_env

    def _make_job_impl(self, sbatch: CompletedProcess, uid: int) -> SlurmJob:
        if sbatch.stderr != "":
            logger.warning(f"sbatch (uid={uid}): {sbatch.stderr}")

        if sbatch.exit_status != 0:
            raise RuntimeError("sbatch failed (exit status {:d})".format(sbatch.exit_status))

        match = self._sbatch_job_id_pattern.fullmatch(sbatch.stdout.strip())
        if match is None:
            e = "no job ID found in sbatch output: '{:s}'"
            raise ValueError(e.format(sbatch.stdout))

        return SlurmJob(int(match.group(1)))

    def _update_jobs_impl(self, jobs: List[SlurmJob]) -> Tuple[ArgumentList, Environment]:
        job_list = ["--job={:d}".format(j.id()) for j in jobs]
        sacct_command = ["sacct", "--parsable2", "--format=JobID,State"] + job_list
        return sacct_command, os.environ

    def _parse_update_jobs_impl(self, jobs: List[SlurmJob], sacct: CompletedProcess) -> None:
        job_map = dict([(j.id(), j) for j in jobs])
        for line in sacct.stdout.strip().split("\n"):
            self._update_jobs_impl_pure(job_map, line)

    @classmethod
    def _update_jobs_impl_pure(cls, job_map: Dict[int, SlurmJob], sacct_line: str) -> None:
        slurm_states_waiting = ["PENDING", "REQUEUED", "SUSPENDED", "RESIZING"]
        slurm_states_failure = [
            "BOOT_FAIL",
            "CANCELLED",
            "DEADLINE",
            "FAILED",
            "NODE_FAIL",
            "OUT_OF_MEMORY",
            "PREEMPTED",
            "TIMEOUT",
        ]

        match = cls._sacct_line_pattern.fullmatch(sacct_line)
        if match is None:
            return

        job_id = int(match.group(1))
        slurm_state = match.group(3)

        assert job_id in job_map

        j = job_map[job_id]

        if slurm_state == "REVOKED":
            e = "cannot handle Slurm siblings like job {:d}"
            raise NotImplementedError(e.format(job_id))

        if slurm_state in slurm_states_waiting:
            assert j.state() in [State.WAITING, State.RUNNING]
            j._state = State.WAITING
        elif slurm_state in ["RUNNING"]:
            assert j.state() in [State.WAITING, State.RUNNING]
            j._state = State.RUNNING
        elif slurm_state in ["COMPLETED"]:
            assert j.state() in [State.WAITING, State.RUNNING, State.TERMINATED]
            j._state = State.TERMINATED
        elif slurm_state in slurm_states_failure:
            assert j.state() in [State.WAITING, State.RUNNING, State.FAILED]
            j._state = State.FAILED
        else:
            e = "unknown Slurm job state '{:s}'"
            raise RuntimeError(e.format(slurm_state))

    def _cancel_jobs_impl(self, jobs: List[SlurmJob]) -> Tuple[ArgumentList, Environment]:
        job_list = [str(j.id()) for j in jobs]
        scancel_command = ["scancel", "--batch", "--quiet"] + job_list
        return scancel_command, os.environ

    def _parse_cancel_jobs_impl(self, jobs: List[SlurmJob], proc: CompletedProcess) -> None:
        # scancel exits with status 1 if at least one job had already been
        # terminated
        if proc.exit_status not in [0, 1]:
            raise RuntimeError("scancel error: exit status {:d}".format(proc.exit_status))
