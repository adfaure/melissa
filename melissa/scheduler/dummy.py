#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from typing import List, Tuple, Union

from melissa.utility.process import ArgumentList, Environment, Process

from .job import Job, State
from .scheduler import DirectScheduler, Options


class DummyJob(Job):
    def __init__(self, jid: Union[str, int]) -> None:
        super().__init__()
        self._id = jid
        self._state = State.WAITING

    def id(self) -> Union[str, int]:
        return self._id

    def unique_id(self) -> Union[str, int]:
        return self._id

    def state(self) -> State:
        return self._state

    def __repr__(self) -> str:
        r = f"DummyJob(id={self.id()},state={str(self.state())})"
        return r

    def set_state(self, new_state: State) -> None:
        self._state = new_state


class DummyScheduler(DirectScheduler[DummyJob]):
    """
    This class implements a scheduler that never runs the submitted jobs.
    """

    @classmethod
    def _name_impl(cls) -> str:
        return "dummy"

    @classmethod
    def _is_available_impl(cls) -> Tuple[bool, Union[str, Tuple[str, str]]]:
        return True, ("\0", "1.0")

    def _submit_job_impl(
        self,
        commands: List[ArgumentList],
        env: Environment,
        options: Options,
        name: str,
        unique_id: int,
    ) -> Tuple[ArgumentList, Environment]:
        return ["/bin/true"], {}

    def _make_job_impl(self, proc: "Process[str]", unique_id: int) -> DummyJob:
        proc.wait()
        return DummyJob(unique_id)

    @classmethod
    def _update_jobs_impl(cls, jobs: List[DummyJob]) -> None:
        for j in jobs:
            if j.state() == State.WAITING:
                j.set_state(State.RUNNING)

    @classmethod
    def _cancel_jobs_impl(cls, jobs: List[DummyJob]) -> None:
        for j in jobs:
            j.set_state(State.FAILED)
