#!/usr/bin/python3

# Copyright (c) 2020-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import enum
import logging
import os
import re
import shutil
import subprocess
from typing import List, Tuple, Union, Dict
from pathlib import Path

from melissa.utility import time
from melissa.utility.process import ArgumentList, Environment, Process

from .job import Id, Job, State
from .scheduler import DirectScheduler, Options
from .slurm_parser import break2str

logger = logging.getLogger(__name__)


class JobType(enum.Enum):

    Server = 0
    Client = 1


class HybridJob(Job):
    def __init__(self, uid: Id, process: "subprocess.Popen[str]", job_type: JobType) -> None:
        super().__init__()
        self._type = job_type
        self._state = State.WAITING
        self._uid = uid
        self._process = process

    def id(self) -> Id:
        return self._process.pid

    def unique_id(self) -> Union[str, int]:
        return self._uid

    def state(self) -> State:
        return self._state

    def __repr__(self) -> str:
        r = f"<{self.__class__.__name__} (id={self.id:d},state={self.state:s})>"
        return r


class NodeWorkload:
    """Keep track of submitted jobs on different nodes."""

    def __init__(self):
        self._node_list: List[str] = []
        self._allocation_size: int = 0
        self._ntasks_per_node: int = 0
        self._node_workload: Dict[str, List[HybridJob]] = {}

    @property
    def node_list(self) -> List[str]:
        return self._node_list

    @node_list.setter
    def node_list(self, nodes: List[str]):
        self._node_list = nodes
        self._node_workload = {node: [] for node in nodes}

    @property
    def allocation_size(self) -> int:
        return self._allocation_size

    @allocation_size.setter
    def allocation_size(self, size: int):
        self._allocation_size = size

    @property
    def ntasks_per_node(self) -> int:
        return self._ntasks_per_node

    @ntasks_per_node.setter
    def ntasks_per_node(self, ntasks: int):
        self._ntasks_per_node = ntasks

    def append(self, node: str, job: HybridJob):
        self._node_workload[node].append(job)

    def select_node(self, iteration) -> str:
        return min(
            self._node_workload, key=lambda node: len(self._node_workload[node])
        )

    def __len__(self) -> int:
        n_submitted_jobs = 0
        for submitted_jobs in self._node_workload.values():
            n_submitted_jobs += len(submitted_jobs)
        return n_submitted_jobs

    def update_jobs(self):
        for node in self._node_workload:
            SlurmSemiGlobalScheduler._update_jobs_impl(self._node_workload[node])
            self._node_workload[node] = [
                job for job in self._node_workload[node]
                if job._state in [State.WAITING, State.RUNNING]
            ]


class SlurmSemiGlobalScheduler(DirectScheduler[HybridJob]):
    # always compile regular expressions to enforce ASCII matching
    # allow matching things like version 1.2.3-rc4
    _srun_version_regexp = r"slurm (\d+)[.](\d+)[.](\d+\S*)"
    _srun_version_pattern = re.compile(_srun_version_regexp, re.ASCII)
    _sbatch_job_id_regexp = r"(\d+)"
    _sbatch_job_id_pattern = re.compile(_sbatch_job_id_regexp, re.ASCII)
    _sacct_line_regexp = r"(\d+)([+]0[.]batch)?[|](\w+)"
    _sacct_line_pattern = re.compile(_sacct_line_regexp, re.ASCII)
    _use_het_prefix: bool = False

    @classmethod
    def is_direct(cls) -> bool:
        return True

    @classmethod
    def _name_impl(cls) -> str:
        return "slurm"

    @classmethod
    def _is_available_impl(cls) -> Tuple[bool, Union[str, Tuple[str, str]]]:
        srun_path = shutil.which("srun")
        if srun_path is None:
            return False, "srun executable not found"

        srun = subprocess.run(
            [srun_path, "--version"],
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if srun.returncode != 0:
            return False, "failed to execute %s: %s" % (srun_path, srun.stderr)

        # do not use pattern.fullmatch() because of the newline at the end of
        # the output
        match = cls._srun_version_pattern.match(srun.stdout)
        if match is None:
            e = "srun output '{:s}' does not match expected format"
            raise ValueError(e.format(srun.stdout))

        version_major = int(match.group(1))
        # the minor version might be of the form `05` but the Python int()
        # function handles this correctly
        version_minor = int(match.group(2))
        version_patch = match.group(3)

        if version_major < 19 or (version_major == 19 and version_minor < 5):
            logger.warn(
                "Melissa has not been tested with Slurm versions older than 19.05.5", RuntimeWarning
            )

        if version_major < 17 or (version_major == 17 and version_minor < 11):
            return (
                False,
                (
                    "expected at least Slurm 17.11,"
                    f"got {version_major}.{version_minor}.{version_patch}"
                    "which does not support heterogeneous jobs"
                ),
            )

        cls._use_het_prefix = version_major >= 20

        version_str = srun.stdout[match.span(1)[0] : match.span(3)[1]]
        return True, (srun_path, version_str)

    def __init__(self) -> None:
        # Attributes for the srun allocation on multiple nodes
        self.srun_ctr: int = 0
        self._node_workload = NodeWorkload()
        # Standard initialization function
        is_available, info = self.is_available()
        if not is_available:
            raise RuntimeError("Slurm unavailable: %s" % (info,))

        assert self._use_het_prefix is not None

    def _sanity_check_impl(self, options: Options) -> List[str]:
        args = options.raw_arguments
        errors = []

        for a in args:
            if a[0] != "-":
                errors.append("non-option argument '{:s}' detected".format(a))
            elif a in ["-n", "--ntasks", "--test-only"]:
                errors.append("remove '{:s}' argument".format(a))

        command = ["srun", "--test-only", "--ntasks=1"] + args + ["--", "true"]
        srun = subprocess.run(
            command,
            stdin=subprocess.DEVNULL,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if srun.returncode != 0:
            e = "srun error on trial execution: {:s}".format(srun.stderr)
            errors.append(e)

        return errors

    def _submit_job_impl(
        self,
        commands: List[ArgumentList],
        env: Environment,
        options: Options,
        name: str,
        unique_id: int,
    ) -> Tuple[ArgumentList, Environment]:
        if name == "melissa-server":
            return self._submit_job_impl_server(commands, env, options, name, unique_id)
        else:
            return self._submit_job_impl_client(commands, env, options, name, unique_id)

    def _submit_job_impl_server(
        self,
        commands: List[ArgumentList],
        env: Environment,
        options: Options,
        name: str,
        unique_id: int,
    ) -> Tuple[ArgumentList, Environment]:
        # sbatch submission
        sbatch_env = os.environ.copy()
        sbatch_env.update(env)

        output_filename = "./stdout/job.{:d}.{:s}.out".format(unique_id, name)
        error_filename = "./stdout/job.{:d}.{:s}.err".format(unique_id, name)

        propagated_options = [
            "--output={:s}".format(output_filename),
            "--error={:s}".format(error_filename),
        ]

        uid = unique_id
        self._server_uid = uid

        sbatch_options = propagated_options + options.raw_arguments

        # serialize sbatch options
        def options2str(options: str) -> str:
            return "#SBATCH " + options

        sbatch_options_str = [options2str(o) for o in sbatch_options]

        sched_cmd = options.sched_cmd
        sched_cmd_opt = options.sched_cmd_opt

        # assemble srun arguments
        srun_arguments: List[List[str]] = []
        if not sched_cmd:
            srun_arguments = [[""] + commands[0]]
        else:
            srun_arguments = [sched_cmd_opt + ["--"] + commands[0]]

        # serialize srun arguments
        def srunargs2str(hetgroup: int, args: List[str]) -> str:
            assert hetgroup >= 0
            assert hetgroup < len(commands)

            prefix = ": " if hetgroup > 0 else ""
            suffix = " \\" if hetgroup + 1 < len(commands) else ""
            return "  " + prefix + " ".join(args) + suffix

        srun_arguments_str = [srunargs2str(i, args) for i, args in enumerate(srun_arguments)]

        # write srun calls to file
        Path('./sbatch').mkdir(parents=True, exist_ok=True)
        sbatch_script_filename = "./sbatch/sbatch.{:d}.sh".format(uid)
        sbatch_script = (
            ["#!/bin/sh"]
            + ["# sbatch script for job {:s}".format(name)]
            + sbatch_options_str
            + [""]
            + (["exec \\"] if not sched_cmd else [f"exec {sched_cmd} \\"])
            + srun_arguments_str
        )

        # POSIX requires files to end with a newline; missing newlines at the
        # end of a file may break scripts that append text.
        # this string won't contain a newline at the end; it must be added
        # manually or by using a function that adds it, e.g., `print`
        sbatch_script_str_noeol = "\n".join(sbatch_script)

        with open(sbatch_script_filename, "w") as f:
            print(sbatch_script_str_noeol, file=f)

        sbatch_call = (
            ["sbatch"]
            + ["--parsable"]
            + ["--job-name={:s}".format(name)]
            + [sbatch_script_filename]
        )

        return sbatch_call, sbatch_env

    def _submit_job_impl_client(
        self,
        commands: List[ArgumentList],
        env: Environment,
        options: Options,
        name: str,
        unique_id: int,
    ) -> Tuple[ArgumentList, Environment]:
        # srun submission for clients
        srun_env = os.environ.copy()
        srun_env.update(env)
        self.srun_ctr += 1

        output_filename = "./stdout/job.{:d}.{:s}.out".format(unique_id, name)
        error_filename = "./stdout/job.{:d}.{:s}.err".format(unique_id, name)

        # command line only scanned the first time
        if self.srun_ctr == 1:
            # extract all environment variables
            self._node_workload.allocation_size = int(srun_env["SLURM_NTASKS"])
            self._node_workload.ntasks_per_node = int(srun_env["SLURM_NTASKS_PER_NODE"])
            self._node_workload.node_list = break2str(srun_env["SLURM_NODELIST"])
            assert (
                len(self._node_workload.node_list)
                == self._node_workload.allocation_size // self._node_workload.ntasks_per_node
            )
            logger.debug(
                f"allocation of {self._node_workload.allocation_size} cores "
                f"with identified nodelist {self._node_workload.node_list} "
                f"from environment variable {srun_env['SLURM_NODELIST']}"
            )

        # make sure resources are available before next series of srun
        logger.debug(f"Scheduler has currently {len(self._node_workload)} jobs running.")
        self._node_workload.update_jobs()
        node = self._node_workload.select_node(self.srun_ctr - 1)

        # build propagated options of the srun command line
        propagated_options = [
            "--output={:s}".format(output_filename),
            "--error={:s}".format(error_filename),
            "--nodelist={:s}".format(node),
        ] + options.raw_arguments

        # assemble srun arguments
        sched_cmd = options.sched_cmd
        sched_cmd_opt = options.sched_cmd_opt

        # retrieve variable value amongst scheduler_arg_client
        def get_from_options(list_of_options: List[str], variable_name: str) -> str:
            for opt in list_of_options:
                if variable_name in opt:
                    idx = opt.find("=")
                    return opt[idx + 1:]
            logger.error(
                f"The variable name {variable_name} is not amongst {list_of_options} "
                "Please make sure scheduler_arg_client is well set"
            )
            return ""

        # write srun/job execution call
        srun_call: List[str] = []
        srun_arguments: List[str] = []
        if not sched_cmd:
            assert len(commands) == 1, "non-unit groups are not supported in this configuration"
            srun_call = commands[0]
            # node and resource specification with environment variables
            new_env = {
                "NODEID": node,
                "NTASKS": get_from_options(propagated_options, "ntasks"),
                "TIME": get_from_options(propagated_options, "time")
            }
            srun_env.update(new_env)
        else:
            for i, cmd in enumerate(commands):
                args = (
                    sched_cmd_opt + propagated_options + ["--"] + cmd
                    + ([":"] if i + 1 < len(commands) else [])
                )
                srun_arguments.extend(args)
            srun_call = [sched_cmd] + srun_arguments

        return srun_call, srun_env

    def _make_job_impl(self, proc: "Process[str]", unique_id: int) -> HybridJob:
        if unique_id == self._server_uid:
            job_type = JobType.Server
        else:
            job_type = JobType.Client
        job = HybridJob(unique_id, proc, job_type)
        if job._type == JobType.Client:
            # Retrieve the node of the client
            node: str = ""
            # retrieved from process arguments (standard submission)
            for arg in proc.args:  # type: ignore
                if "nodelist" in arg:  # type: ignore
                    node = arg.split("=")[-1]  # type: ignore
                    break
            # retrieved from the process environment (no sched_cmd submission)
            if not node:
                node = proc.env["NODEID"]  # type: ignore
            assert node, f"Node list missing in {proc.args} and {proc.env}"  # type: ignore
            self._node_workload.append(node, job)
        return job

    @classmethod
    def _update_jobs_impl(cls, jobs: List[HybridJob]) -> None:
        logger.debug(f"Update - job list {[j._type for j in jobs]}")

        for j in jobs:
            if j._type == JobType.Client:
                returncode = j._process.poll()
                if returncode is None:
                    state = State.RUNNING
                elif returncode == 0:
                    state = State.TERMINATED
                else:
                    state = State.FAILED
                j._state = state
        # note that the server job status is not monitored
        # trough subprocess since it is an indirect submission.
        # instead, the server job keeps the initial WAITING status and
        # the server will be marked as running by the state machine
        # as soon as it connects to the launcher.
        # Then, his running state is only monitored through regular pinging.

    @classmethod
    def _cancel_jobs_impl(cls, jobs: List[HybridJob]) -> None:
        logger.debug(f"Cancel - job list {[j._type for j in jobs]}")

        # when the user presses ctrl+c, the shell will send all processes in
        # the same process group SIGINT. some programs respond intelligently to
        # signals by freeing resources and exiting. these programs may also
        # exit _immediately_ if they receive a second signal within a short
        # time frame (e.g., srun or mpirun which won't terminate its child
        # processes in this case). for this reason, we wait before terminating
        # jobs here.
        max_wait_time = time.Time(seconds=5)

        # wait at most max_wait_time overall
        def compute_timeout(t_start: time.Time) -> time.Time:
            t_waited = time.monotonic() - t_start
            if t_waited < max_wait_time:
                return max_wait_time - t_waited
            return time.Time(seconds=0)

        # terminate processes
        t_0 = time.monotonic()
        for j in jobs:
            if j._type == JobType.Client:
                try:
                    timeout = compute_timeout(t_0)
                    j._process.wait(timeout.total_seconds())
                except subprocess.TimeoutExpired:
                    logger.debug(f"Slurm srun scheduler terminating process {j.id()}")
                    j._process.terminate()

        # kill processes if necessary
        t_1 = time.monotonic()
        for j in jobs:
            if j._type == JobType.Client:
                try:
                    timeout = compute_timeout(t_1)
                    j._process.wait(timeout.total_seconds())
                except subprocess.TimeoutExpired:
                    logger.debug(f"Slurm srun scheduler killing process {j.id()}")
                    j._process.kill()

                j._state = State.FAILED
