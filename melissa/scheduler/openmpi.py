#!/usr/bin/python3

# Copyright (c) 2020-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import logging
import os
import re
import shutil
import subprocess
from typing import List, Tuple, Union

from melissa.utility import time
from melissa.utility.process import ArgumentList, Environment, Process

from .job import Id, Job, State
from .scheduler import DirectScheduler, Options

logger = logging.getLogger(__name__)


class OpenMpiJob(Job):
    def __init__(self, uid: Id, process: "subprocess.Popen[str]") -> None:
        super().__init__()
        self._uid = uid
        self._process = process
        self._state = State.RUNNING

    def id(self) -> Id:
        return self._process.pid

    def unique_id(self) -> Union[str, int]:
        return self._uid

    def state(self) -> State:
        return self._state

    def __repr__(self) -> str:
        r = "OpenMpiJob(id={:d},state={:s})".format(self.id(), str(self._state))
        return r


class OpenMpiScheduler(DirectScheduler[OpenMpiJob]):
    @classmethod
    def _name_impl(cls) -> str:
        return "openmpi"

    @classmethod
    def _is_available_impl(cls) -> Tuple[bool, Union[str, Tuple[str, str]]]:
        mpirun_path = shutil.which("mpirun")
        if mpirun_path is None:
            return False, "mpirun executable not found"

        mpirun = subprocess.run(
            [mpirun_path, "--version"],
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if mpirun.returncode != 0:
            return False, "failed to execute %s: %s" % (mpirun_path, mpirun.stderr)

        if mpirun.stderr != "":
            logger.warning("%s: %s", mpirun_path, mpirun.stderr)

        version_re = r"^mpirun \(Open MPI\) (\d+[.]\d+[.]\d+)"
        match = re.match(version_re, mpirun.stdout)
        if not match:
            return False, "{:s} is not an OpenMPI mpirun".format(mpirun_path)

        ompi_version = match.group(1)
        return True, (mpirun_path, ompi_version)

    def _sanity_check_impl(self, options: Options) -> List[str]:
        args = options.raw_arguments
        es = []

        for a in args:
            e = None
            if "do-not-launch" in a:
                e = "remove `{:s}` argument".format(a)
            elif a in ["-N", "-c", "-n", "--n", "-np"]:
                e = "remove `{:s}` argument".format(a)

            if e is not None:
                es.append(e)

        return es

    def _submit_job_impl(
        self,
        commands: List[ArgumentList],
        env: Environment,
        options: Options,
        name: str,
        unique_id: int,
    ) -> Tuple[ArgumentList, Environment]:
        # Approach to environment variables:
        # Follow OpenMPI mpirun man page advice, that is,
        # * set `VARIABLE=VALUE` in mpirun environment,
        # * pass `-x VARIABLE` on the mpirun command line.

        ompi_env = os.environ.copy()
        env_args = []  # type: List[str]
        for key in sorted(env.keys()):
            ompi_env[key] = env[key]
            env_args += ["-x", key]

        ompi_options = options.raw_arguments + options.sched_cmd_opt

        ompi_commands = []  # type: List[str]
        for i, cmd in enumerate(commands):
            ompi_cmd = (
                ompi_options
                + env_args
                + ["--"]
                + cmd
                + ([":"] if i + 1 < len(commands) else [])
            )

            ompi_commands = ompi_commands + ompi_cmd

        ompi_call = [options.sched_cmd] + ompi_commands
        return ompi_call, ompi_env

    def _make_job_impl(self, proc: "Process[str]", unique_id: int) -> OpenMpiJob:
        return OpenMpiJob(unique_id, proc)

    @classmethod
    def _update_jobs_impl(cls, jobs: List[OpenMpiJob]) -> None:
        for j in jobs:
            returncode = j._process.poll()
            if returncode is None:
                state = State.RUNNING
            elif returncode == 0:
                state = State.TERMINATED
            else:
                state = State.FAILED
            j._state = state

    @classmethod
    def _cancel_jobs_impl(cls, jobs: List[OpenMpiJob]) -> None:
        # when the user presses ctrl+c, the shell will send all processes in
        # the same process group SIGINT. some programs respond intelligently to
        # signals by freeing ressources and exiting. these programs may also
        # exit _immediately_ if they receive a second signal within a short
        # time frame (e.g., srun or mpirun which won't terminate its child
        # processes in this case). for this reason, we wait before terminating
        # jobs here.
        max_wait_time = time.Time(seconds=5)

        # wait at most max_wait_time overall
        def compute_timeout(t_start: time.Time) -> time.Time:
            t_waited = time.monotonic() - t_start
            if t_waited < max_wait_time:
                return max_wait_time - t_waited
            return time.Time(seconds=0)

        # terminate processes
        t_0 = time.monotonic()
        for j in jobs:
            try:
                timeout = compute_timeout(t_0)
                j._process.wait(timeout.total_seconds())
            except subprocess.TimeoutExpired:
                logger.debug("OpenMPI scheduler terminating process %d", j.id())
                j._process.terminate()

        # kill processes if necessary
        t_1 = time.monotonic()
        for j in jobs:
            try:
                timeout = compute_timeout(t_1)
                j._process.wait(timeout.total_seconds())
            except subprocess.TimeoutExpired:
                logger.debug("OpenMPI scheduler killing process %d", j.id())
                j._process.kill()

            j._state = State.FAILED
