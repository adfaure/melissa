from typing import Dict, List
import time
import logging
from melissa.server.simulation import Group

logger = logging.getLogger(__name__)


class FaultTolerance:
    def __init__(self, ft_off: bool, max_delay: float, crashes_before_redraw: int, nb_group: int):
        """
        Fault-Tolerance object has the following attributes:
        - ft_off: Fault-Tolerance switch
        - max_delay: simulation walltime
        - crashes_before_redraw: number of accepted failures before input
          resampling
        - restart_grp: dictionary indicating which groups need relaunching
          with of without resampling
        - failed_id: unique ids list of failed groups
        - nb_group: total number of groups
        """
        self.ft_off = ft_off
        self.max_delay = max_delay
        self.crashes_before_redraw = crashes_before_redraw
        self.restart_grp: Dict[int, bool] = {}
        self.failed_id: List[int] = []
        self.nb_group: int = nb_group

    def checkpointing(self) -> None:
        """
        this function needs to be designed with the following
        features:
        - save NN model [DL] or Stats [SA]
        - save buffer [DL]
        - save simulations dictionary [DL, SA]
        """

    def restart(self) -> None:
        """
        this function needs to be designed with the following
        features:
        - import NN model [DL] or Stats [SA]
        - import buffer [DL]
        - import simulations dictionary [DL, SA]
        """

    def handle_failed_group(self, group_id: int, group: Group) -> bool:
        """
        this function reacts to failed simulations
        """
        return_bool: bool = False
        group.n_failures += 1

        if group.n_failures > self.crashes_before_redraw:
            logger.warning(f"Group with id {group_id} failed too many times")
            return_bool = True
        else:
            logger.warning(f"Group with id {group_id} failed {group.n_failures} times")
            return_bool = False

        self.append_failed_id(group_id)
        return return_bool

    def check_time_out(self, groups: Dict[int, Group]) -> bool:
        """
        this function verifies if any simulation timed-out
        by doing the following:
        - create list of timed-out sim ids
        - update the number of failures
        - create a dictionary to relaunch all simulations
        - try to update the list of unique failed ids
        - return a boolean indicating if any simulation timed-out
        """
        timed_out_ids: List[int] = []

        for grp_id, grp in groups.items():
            for sim_id, sim in grp.simulations.items():
                if (
                    sim.last_message is not None
                    and not sim.finished()
                    and time.time() - sim.last_message
                    > self.max_delay
                    and grp_id not in timed_out_ids
                ):
                    timed_out_ids.append(grp_id)

        for grp_id in timed_out_ids:
            logger.warning(f"Simulation(s) in group of id {grp_id} timed-out")
            groups[grp_id].n_failures += 1
            for sim in list(groups[grp_id].simulations.values()):
                sim.last_message = None
            self.restart_grp[grp_id] = (
                groups[grp_id].n_failures > self.crashes_before_redraw
            )
            self.append_failed_id(grp_id)

        return len(self.restart_grp) > 0

    def append_failed_id(self, group_id: int):
        """
        this function tries to update the list of
        unique failed ids and raises Exception if
        needed
        """
        if group_id not in self.failed_id:
            self.failed_id.append(group_id)
        if self.ft_off:
            raise Exception(
                f"Fault-Tolerance is off, group {group_id} "
                "failure will cause the server to abort"
            )
        if len(self.failed_id) == self.nb_group:
            raise Exception(
                "All groups failed please make sure: \n"
                "- the path to the executable is correct, \n"
                "- the number of expected time steps is correct, \n"
                "- the simulation walltime was well estimated."
            )
