from typing import Union, List, Dict, Any, Callable, Optional
import logging
import time

from torch.utils.data import IterableDataset

from melissa.server.simulation import SimulationData
from melissa.server.deep_learning.buffer import BaseQueue, Empty
from melissa.server.deep_learning.tensorboard_logger import TensorboardLogger


logger = logging.getLogger(__name__)


class MelissaIterableDataset(IterableDataset):
    """
    Object whose only job is to make an iterable dataset available to torch
    or tensorflow. Only used by DeepMelissa children.
    """

    def __init__(
        self,
        buffer: BaseQueue,
        config: dict = {},
        transform: Optional[Callable] = None,
        tb_logger: Optional[TensorboardLogger] = None,
    ) -> None:
        super().__init__()
        self.buffer = buffer
        self.tb_logger = tb_logger
        self._is_receiving = True
        self.batch = 0
        self.sample_number = 0
        self.config: Dict[str, Any] = config
        self.transform = transform

    def __iter__(self):
        # Infinite iterator which will always try to pull from the
        # buffer as long as the buffer is not empty or the server
        # is still receiving data
        while self._is_receiving or not self.buffer.empty():
            try:
                start = time.time()
                items: Union[SimulationData, List[SimulationData]] = self.buffer.get()
                if self.transform:
                    data = self.transform(items, self.config)
                elif isinstance(items, list):
                    data = [item.data for item in items]
                else:
                    data = items.data
                if self.tb_logger:
                    self.sample_number += 1
                    self.tb_logger.log_scalar_dbg(
                        "get_time", time.time() - start, self.sample_number)
                    self.tb_logger.log_scalar_dbg(
                        "buffer_size", len(self.buffer), self.sample_number
                    )
                yield data
            except Empty:
                logger.warning("Buffer empty but still receiving.")
                continue

    def as_tensorflow_dataset(self, shape):
        import tensorflow as tf

        dataset = tf.data.Dataset.from_generator(
            self.__iter__, output_signature=(tf.TensorSpec(shape=shape, dtype=tf.float64))
        )
        return dataset
