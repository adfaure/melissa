import logging
import os
import threading
from abc import abstractmethod
from functools import wraps
from typing import Any, Callable, Dict, Optional, Tuple, Union
import time
import numpy as np

from melissa.server.deep_learning.tensorboard_logger import TensorboardLogger
from melissa.server.deep_learning.dataset import MelissaIterableDataset
from melissa.server.deep_learning.buffer import SimpleQueue
from melissa.server.base_server import BaseServer
from melissa.server.simulation import (PartialSimulationData, Simulation,
                                       SimulationData, SimulationDataStatus)
from melissa.utility.networking import get_rank_and_num_server_proc
from pathlib import Path


logger = logging.getLogger(__name__)


class DeepMelissaServer(BaseServer):
    """
    Director to be used for any DeepMelissa study.
    The MelissaServer is initialized with the proper options
    self.start() sets the order of operations including the
    user created training loop "train()"
    """

    def __init__(self, config: Dict[str, Any]):
        super().__init__(config)

        self.learning: int = 2
        self.check_group_size()
        self.dl_config: Dict[str, Any] = config['dl_config']
        self.study_options: Dict[str, Any] = config['study_options']
        self.debug = True if self.study_options["verbosity"] >= 3 else False
        self.tb_logger = TensorboardLogger(self.rank,
                                           disable=not self.dl_config["tensorboard"],
                                           debug=self.debug)
        # define temporary elementary buffer for typing purposes.
        self.buffer: SimpleQueue = SimpleQueue()
        self.dataset: MelissaIterableDataset = MelissaIterableDataset(
            buffer=self.buffer,
            tb_logger=self.tb_logger,
            config=self.config,
            transform=self.process_simulation_data,
        )
        self.batch_size: int = self.dl_config['batch_size']
        self.per_server_watermark: int = self.dl_config['per_server_watermark']
        self.buffer_size: int = self.dl_config["buffer_size"]
        self.pseudo_epochs: int = self.dl_config.get("pseudo_epochs", 1)
        self.sample_number: int = 0
        self.n_expected_batches: int = 1
        self.idr_rank: int | None = None
        self.setup_jz: bool = self.dl_config.get("setup_jz", False)
        self.n_batches_update: int = self.dl_config["n_batches_update"]

    def check_group_size(self):
        if self.group_size > 1 and self.num_clients % self.group_size != 0:
            logger.error("Incorrect group_size, please remove or adjust this option")
            self.catch_error = True

    def start(self):
        """
        The main execution method
        """
        self.launch_first_groups()
        if not self.setup_jz:
            self.setup_environment()
        else:
            self.setup_environment_JZ()
        self.set_model()
        self.set_expected_batches_samples_watermark()
        self.configure_data_collection()

        # put server receive on a separate thread.
        # should not be accesse by user
        rcv = threading.Thread(target=self.receive)
        rcv.start()

        self.server_online()

        self.tb_logger.close()
        if self.dl_config.get("convert_log_to_df", False):
            try:
                self.convert_log_to_df()
            except ImportError as e:
                logger.error(f"Unable to import dependencies for log conversion {e}. "
                             "Please install pandas and tensorflow.")

        self.server_finalize()

    def setup_environment(self):
        """
        Sets environment for distributed GPU training if desired.
        """
        return

    def setup_environment_JZ(self):
        """
        Unique env setup for Jean-Zay as recommended by
        http://www.idris.fr/eng/jean-zay/gpu/jean-zay-gpu-torch-multi-eng.html
        """
        return

    @abstractmethod
    def set_model(self):
        """
        Configure the server self.model to prepare for initialization
        """
        return

    @abstractmethod
    def server_online(self):
        """
        Initiating data collection and directing the custom
        methods for acting on collected data.
        """
        return

    def receive(self):
        """
        Handle data from the server.
        """
        try:
            self._is_receiving = True
            while not self.all_done():
                start = time.time()
                data = self.run()
                if data is not None and isinstance(data, SimulationData):
                    logger.debug(
                        f"Receive Message {data.simulation_id} time-step {data.time_step}"
                    )
                    self.buffer.put(data)
                    self.sample_number += 1
                    self.tb_logger.log_scalar_dbg(
                        "put_time", time.time() - start, self.sample_number)

            self._is_receiving = False
            logger.debug("Setting threshold to 0")
            self.buffer.signal_reception_over()
            logger.debug("Threshold set to 0")

        except Exception as e:
            logger.exception(f"Exception was raised in the receiving thread: \n {e}")
            self.close_connection(1)
            os._exit(1)

    def handle_simulation_data(self, msg):
        """
        Parses and validates the incoming data messages from simulations
        """
        # 1. Deserialize message
        msg_data: PartialSimulationData = PartialSimulationData.from_msg(msg, self.learning)
        group_id = msg_data.simulation_id // self.group_size
        logger.debug(
            f"Rank {self.rank} received {msg_data}  from rank {msg_data.client_rank} "
            f"(vect_size: {msg_data.data_size})")

        # 2. Apply filters
        if msg_data.field == "termination":
            logger.info(f"Rank {self.rank} received termination message "
                        f"from simulation {msg_data.simulation_id} with "
                        f"{msg_data.time_step} expected time-steps"
                        )
            # increment the total number of expected time-steps if not known
            if self.n_expected_batches == 0:
                self.num_samples += msg_data.time_step
                self.groups[group_id].simulations[msg_data.simulation_id].n_time_steps = (
                    msg_data.time_step
                )
                logger.info(f"Rank {self.rank}: simulation {msg_data.simulation_id} finished "
                            f"number of expected samples incremented to {self.num_samples}")
                self.n_finished_simulations += 1
                return None
        if (
            msg_data.time_step < 0
            or (msg_data.time_step > self.num_samples and self.n_expected_batches > 0)
        ):
            logger.warning(f"Rank {self.rank}: bad time-step {msg_data.time_step}")
            return None
        if msg_data.field not in self.fields:
            if msg_data.field != "termination":
                logger.warning(f"Rank {self.rank}: bad field {msg_data.field}")
            return None

        simulation = self.groups[group_id].simulations[msg_data.simulation_id]

        simulation_status, simulation_data = self.check_simulation_data(
            simulation, msg_data)
        if simulation_status == SimulationDataStatus.COMPLETE and simulation_data:
            logger.debug(
                f"Rank {self.rank}: assembled time-step {simulation_data.time_step} "
                f"- simulationID {simulation_data.simulation_id}")
        elif simulation_status == SimulationDataStatus.ALREADY_RECEIVED:
            logger.warning(
                f"Rank {self.rank}: duplicate simulation data {msg_data}")

        # Check if simulation has finished
        if (
            simulation_status == SimulationDataStatus.COMPLETE
            or simulation_status == SimulationDataStatus.EMPTY
        ) and simulation.finished():
            logger.info(f"Rank {self.rank}: simulation {simulation.id} finished")
            self.n_finished_simulations += 1

        return simulation_data

    def check_simulation_data(
        self, simulation: Simulation, simulation_data: PartialSimulationData
    ) -> Tuple[
        SimulationDataStatus,
        Union[Optional[SimulationData], Optional[PartialSimulationData]],
    ]:
        """
        Look for duplicated messages,
        update received_simulation_data and the simulation_data status.
        """
        if simulation_data.client_rank not in simulation.received_simulation_data:
            simulation.received_simulation_data[simulation_data.client_rank] = {}
            # the 2D-array is allocated at once if the number of expected samples is known
            if self.n_expected_batches != 0:
                simulation.received_time_steps[simulation_data.client_rank] = (
                    np.zeros((len(self.fields), self.num_samples), dtype=bool)
                )
            # if not it is initialized with a single column
            else:
                simulation.received_time_steps[simulation_data.client_rank] = (
                    np.zeros((len(self.fields), 1), dtype=bool)
                )
        # if num_samples is unknown the received_time_step matrix is built on the fly
        if (
            simulation_data.time_step
            > simulation.received_time_steps[simulation_data.client_rank].shape[1] - 1
        ):
            simulation.received_time_steps[simulation_data.client_rank] = np.concatenate(
                [simulation.received_time_steps[simulation_data.client_rank],
                 np.zeros((len(self.fields), 1), dtype=bool)], axis=1
            )

        # Data have already been received
        if simulation.has_already_received(
            simulation_data.client_rank, simulation_data.time_step, simulation_data.field
        ):
            return SimulationDataStatus.ALREADY_RECEIVED, None
        # Time step has never been seen
        if simulation_data.time_step not in simulation.received_simulation_data[
            simulation_data.client_rank
        ]:
            simulation.received_simulation_data[simulation_data.client_rank][
                simulation_data.time_step
            ] = {field: None for field in simulation.fields}
        # Update the entry
        simulation.received_simulation_data[simulation_data.client_rank][
            simulation_data.time_step
        ][simulation_data.field] = simulation_data
        simulation._mark_as_received(
            simulation_data.client_rank, simulation_data.time_step, simulation_data.field
        )
        if simulation.is_complete(simulation_data.time_step):
            # All fields have been received for the time step
            simulation.n_received_time_steps += 1
            # Check there is actual data
            is_empty = simulation_data.data_size == 0
            if is_empty:
                # Data have been set to another device, fields are empty
                del simulation.received_simulation_data[simulation_data.client_rank][
                    simulation_data.time_step
                ]
                return SimulationDataStatus.EMPTY, None
            # Concatenate data in the same order as fields.
            data = []
            for sd in simulation.received_simulation_data[simulation_data.client_rank][
                    simulation_data.time_step].values():
                if not sd:
                    logger.warning('No data dictionary found')
                else:
                    assert isinstance(sd, PartialSimulationData)
                    data.append(sd.data)

            del simulation.received_simulation_data[simulation_data.client_rank][
                simulation_data.time_step
            ]
            return SimulationDataStatus.COMPLETE, SimulationData(
                simulation_data.simulation_id,
                simulation_data.time_step,
                data,
                simulation.parameters,
            )
        else:
            # Not all fields have been received yet
            return SimulationDataStatus.PARTIAL, None

    def set_expected_batches_samples_watermark(self):
        """
        Takes user config and computes the expected samples per server proc
        and expected batches per server proc
        """
        # standard case where num_samples is given in the config file
        if self.num_samples > 0:
            # ensure watermark is sufficient
            self.check_water_mark()

            # Account for possible accumulated shift
            self.n_expected_samples = (self.num_clients // self.num_server_proc) * self.num_samples
            self.n_expected_batches = (
                self.n_expected_samples // self.batch_size * self.pseudo_epochs
            )

            if self.pseudo_epochs > 1 and self.buffer_size != self.n_expected_samples:
                logger.warning(
                    "User tried using pseudo_epochs with buffer size smaller than expected "
                    "samples. Setting buffer size to number of expected samples "
                    f"({self.n_expected_samples})."
                )
                self.buffer_size = self.n_expected_samples

            logger.info(
                f"Expecting {self.n_expected_samples} "
                f"samples across {self.n_expected_batches} batches.")
        # when num_samples is not known a priori
        else:
            logger.info("Number of expected samples a priori unknown")
            self.n_expected_batches = 0

    def check_water_mark(self):
        """
        Ensures there are sufficient samples to reach the per_server_watermark
        """
        total_samples = (self.num_samples * self.num_clients)
        samples_per_server = total_samples // self.num_server_proc
        if not self.dl_config["per_server_watermark"] <= samples_per_server:
            raise Exception('Insufficient samples to reach per_server_watermark. '
                            'please increase num_samples, or decrease per_server_watermark.')

    def other_processes_finished(self, batch_number: int) -> bool:
        """
        Ensure the server processes are emptying their buffers together
        after data reception is finished.
        """

        logger.debug(f"{self.rank} is on batch {batch_number + 1}/{self.n_expected_batches}")

        # ensure self.dataset._is_receiving is in sync across all server
        # processes.
        dataset_is_receiving = self.synchronize_is_receiving()

        # in case of pseudo_offline training, we want to avoid a
        # server timeout so we ping the launcher with time_monitor
        if not dataset_is_receiving:
            # at this point the total number of expected samples should be known
            # and used to update the value of self.n_expected_batches
            if self.n_expected_batches == 0:
                # per client number of expected time-steps
                self.num_samples //= self.num_clients
                self.set_expected_batches_samples_watermark()
            self.time_monitor.check_clock(time.monotonic(), self)
            logger.debug("One of the server processes finished receiving. "
                         f"{self.rank} is on batch {batch_number + 1}/{self.n_expected_batches}")

        # If server is done receiving from clients and the expected batch number - 1
        # has been seen
        if not dataset_is_receiving and batch_number >= self.n_expected_batches - 1:
            logger.info("Batch number sufficient, breaking train loop.")
            return True
        else:
            return False

    def convert_log_to_df(self):
        """
        Convert local TensorBoard data into Pandas DataFrame.
        Saves the pandas dataframe as a pickle file inside
        out_dir/tensorboard.
        """
        from tensorflow.python.summary.summary_iterator import summary_iterator
        import pandas as pd

        def convert_tfevent(filepath):
            return pd.DataFrame([
                parse_tfevent(e) for e in summary_iterator(filepath) if len(e.summary.value)
            ])

        def parse_tfevent(tfevent):
            return dict(
                wall_time=tfevent.wall_time,
                name=tfevent.summary.value[0].tag,
                step=tfevent.step,
                value=float(tfevent.summary.value[0].simple_value),
            )

        columns_order = ['wall_time', 'name', 'step', 'value']

        out = []
        for folder in Path("tensorboard").iterdir():
            if f"gpu_{self.rank}" in str(folder):
                for file in folder.iterdir():
                    if "events.out.tfevents" not in str(file):
                        continue
                    if f"rank_{self.rank}" not in str(file):
                        continue
                    logger.info(f"Parsing {str(file)}")
                    out.append(convert_tfevent(str(file)))

        all_df = pd.concat(out)[columns_order]
        all_df.reset_index(drop=True)
        all_df.to_pickle(f"./tensorboard/data_rank_{self.rank}.pkl")

    def server_finalize(self):
        """
        All finalization methods go here.
        """
        return

    @abstractmethod
    def configure_data_collection(self):
        """
        Instantiates the data collector and buffer.
        """
        return

    @abstractmethod
    def train(self, model: Any):
        """
        Use-case based training loop.
        """
        return

    def test(self, model: Any):
        """
        User can setup a test function if desired.
        Not required.
        """
        return model

    def synchronize_is_receiving(self) -> bool:
        """
        Coordinates the dataset _is_receiving across all
        server processes. This usually requires a library
        specific all_reduce function (e.g. dist.all_reduce
        in pytorch)
        """
        return True


def rank_zero_only(fn: Callable) -> Callable:
    """Function that can be used as a decorator to enable a function/method
    being called only on rank 0.
    Inspired by pytorch_lightning
    https://pytorch-lightning.readthedocs.io/en/latest/_modules/pytorch_lightning/utilities/rank_zero.html#rank_zero_info
    """

    rank, _ = get_rank_and_num_server_proc()

    @wraps(fn)
    def wrapped_fn(*args: Any, **kwargs: Any) -> Optional[Any]:
        if rank == 0:
            return fn(*args, **kwargs)
        return None

    return wrapped_fn
