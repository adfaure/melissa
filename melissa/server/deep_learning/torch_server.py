import logging
import os
from abc import abstractmethod
from typing import Any, Dict
from torch.nn.parallel import DistributedDataParallel as DDP
import torch.distributed as dist
import torch
import torch.utils.data

from melissa.server.deep_learning import DeepMelissaServer
from melissa.server.deep_learning.base_dl_server import rank_zero_only

logger = logging.getLogger(__name__)


class TorchServer(DeepMelissaServer):
    """
    Director to be used for any DeepMelissa study.
    The MelissaServer is initialized with the proper options
    self.start() sets the order of operations including the
    user created training loop "train()"
    """

    def __init__(self, config: Dict[str, Any]):
        super().__init__(config)
        self.model: Any = None

    def setup_environment(self):
        """
        Sets environment for distributed GPU training if desired.
        """
        os.environ["MASTER_ADDR"] = "127.0.0.1"
        os.environ["MASTER_PORT"] = "29500"
        if torch.cuda.is_available() and torch.cuda.device_count() >= self.num_server_proc:
            world_size = torch.cuda.device_count()
            self.device = f"cuda:{self.rank}"
            backend = 'nccl'
            logger.info(f"available with world_size {world_size}")
        else:
            world_size = self.num_server_proc
            self.device = f"cpu:{self.rank}"
            backend = 'gloo'
        logger.info(f"Rank {self.rank} - self device: {self.device} - world_size {world_size}")

        dist.init_process_group(
            backend, rank=self.rank, world_size=world_size)
        return

    def setup_environment_JZ(self):
        """
        Uses JZ recommendations for setting up environment
        """
        from melissa.utility import idr_torch

        if torch.cuda.is_available():
            torch.cuda.set_device(idr_torch.local_rank)
            world_size = idr_torch.size
            self.device = f"cuda:{idr_torch.local_rank}"
            self.idr_rank = idr_torch.local_rank
            backend = 'nccl'
            logger.info(f"available with world_size {world_size}")
        else:
            logger.error("Using JZ torch requires GPU reservations. No GPU found.")
            raise RuntimeError

        dist.init_process_group(
            backend, init_method="env://", rank=idr_torch.rank, world_size=idr_torch.size)
        return

    def server_online(self):
        """
        What the server should do while "online"
        """
        # main thread heads over to handle distributed training

        if not self.setup_jz and "cuda" in self.device:
            model = DDP(self.model, device_ids=[self.device])
        elif self.setup_jz:
            model = DDP(self.model, device_ids=[self.idr_rank])
        else:
            model = DDP(self.model)

        self.train(model=model)
        # self.test(model)

        return

    def server_finalize(self):
        """
        All finalization methods go here.
        """
        logger.info("Stop Server")
        self.write_final_report()
        self.close_connection()
        dist.destroy_process_group()

        return

    def synchronize_is_receiving(self) -> bool:
        """
        Coordinates the dataset _is_receiving across all
        server processes.
        """
        if "cuda" in self.device:
            self.dataset._is_receiving = torch.tensor(
                self._is_receiving, dtype=bool, device=self.device)  # type: ignore
        else:
            self.dataset._is_receiving = torch.tensor(
                int(self._is_receiving), dtype=int, device=self.device)  # type: ignore

        dist.all_reduce(self.dataset._is_receiving, op=dist.ReduceOp.PRODUCT)

        return bool(self.dataset._is_receiving)

    @abstractmethod
    def configure_data_collection(self):
        """
        Instantiates the data collector and buffer.
        """
        return

    @abstractmethod
    def train(self, model: Any):
        """
        Use-case based training loop.
        """
        return

    def test(self, model: Any):
        """
        User can setup a test function if desired.
        Not required.
        """
        return model

    class MyModel(torch.nn.Module):
        """
        The base model architecture. Use-cases can
        override if they wish to change model architecture.
        """

        def __init__(self, input_dim, n_features=16):
            super().__init__()
            self.input_dim = input_dim
            self.output_dim = input_dim
            self.n_features = n_features
            self.net = torch.nn.Sequential(
                torch.nn.Linear(self.input_dim, self.n_features),
                torch.nn.SiLU(),
                torch.nn.Linear(self.n_features, self.n_features),
                torch.nn.SiLU(),
                torch.nn.Linear(self.n_features, self.output_dim),
            )
            print(f"Model summary: {self.net}")

        def forward(self, x):
            y = self.net(x)
            return y


@rank_zero_only
def checkpoint(
    model: torch.nn.Module,
    optimizer: torch.optim.Optimizer,
    batch: int,
    loss: float,
    path: str = "model.ckpt",
):
    torch.save(
        {
            "batch": batch,
            "loss": loss,
            "model_state_dict": model.state_dict(),
            "optimizer_state_dict": optimizer.state_dict(),
        },
        path,
    )
