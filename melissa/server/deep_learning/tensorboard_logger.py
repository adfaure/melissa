
from torch.utils.tensorboard import SummaryWriter
from typing import Any


class TensorboardLogger:
    def __init__(self, rank: int, logdir: str = "tensorboard",
                 disable: bool = False, debug: bool = False):
        self.disable = disable
        self.debug = debug
        self.writer: SummaryWriter | None = None
        if not self.disable:
            self.writer = SummaryWriter(f"{logdir}/gpu_{rank}", filename_suffix=f"rank_{rank}")
            layout = {
                "Server stats": {
                    "loss": ["Multiline", ["Loss/train", "Loss/valid"]],
                    "put_get_time": ["Multiline", ["put_time", "get_time"]],
                },
            }
            self.writer.add_custom_scalars(layout)

    def log_scalar(self, tag: str, scalar_value: Any, step: int):
        if not self.disable and self.writer is not None:
            self.writer.add_scalar(tag, scalar_value, step)

    def log_scalar_dbg(self, tag: str, scalar_value: Any, step: int):
        if not self.disable and self.writer is not None and self.debug:
            self.writer.add_scalar(tag, scalar_value, step)

    def close(self):
        if not self.disable and self.writer is not None:
            self.writer.flush()
            self.writer.close()
