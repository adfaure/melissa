from typing import Optional, Tuple, Union, List, Deque
import logging
from time import time
import random
from dataclasses import dataclass
from collections import deque, Counter
import threading
from queue import Full, Empty

import numpy as np

from melissa.server.simulation import SimulationData
from melissa.typing import GetProtocol, Threshold

logger = logging.getLogger(__name__)


@dataclass
class BufferItem:

    data: SimulationData
    seen: int = 0


class PutGetMetric:
    def __init__(self, val: int):
        self.val: int = val
        self.increment_lock: threading.Lock = threading.Lock()

    def inc(self, val):
        with self.increment_lock:
            self.val = self.val + val

    def dec(self, val):
        with self.increment_lock:
            self.val = self.val - val


class NotEnoughData(Empty):
    "Not enough data in the queue"


class BaseQueue:
    """A queue inspired by the Lib/queue.py,
    but with only the functionality needed, hence no task_done feature.
    See https://github.com/python/cpython/blob/main/Lib/queue.py#L28

    """

    def __init__(self, maxsize: int = 0):
        self.maxsize = maxsize
        self.mutex = threading.RLock()
        self.not_empty = threading.Condition(self.mutex)
        self.not_full = threading.Condition(self.mutex)
        self._init_queue()
        self.put_get_metric: PutGetMetric = PutGetMetric(0)

    def _init_queue(self):
        self.queue: Deque[SimulationData] = deque()

    def _size(self) -> int:
        return len(self.queue)

    def _is_sampling_ready(self) -> bool:
        return self._size() > 0

    def __len__(self):
        with self.mutex:
            return self._size()

    def empty(self):
        with self.mutex:
            return self._size() == 0

    def _is_full(self) -> bool:
        return self._size() >= self.maxsize

    def _on_full(self, block, timeout) -> bool:
        if not block:
            raise Full
        else:
            self.not_full.wait(timeout)
        return True

    def _get(self) -> SimulationData:
        return self.queue.popleft()

    def _put(self, item: SimulationData):
        return self.queue.append(item)

    def get(self, block: bool = True, timeout: Optional[float] = None):
        with self.not_empty:
            if not block:
                if not self._is_sampling_ready():
                    raise NotEnoughData
            elif timeout is None:
                while not self._is_sampling_ready():
                    self.not_empty.wait()
            elif timeout < 0:
                raise ValueError("'timeout' must be a non-negative number")
            else:
                endtime = time() + timeout
                while not self._is_sampling_ready():
                    remaining = endtime - time()
                    if remaining <= 0.0:
                        raise NotEnoughData
                    self.not_empty.wait(remaining)
            item = self._get()
            self.not_full.notify()
            self.put_get_metric.dec(1)
            return item

    def put(self, item, block: bool = True, timeout: Optional[float] = None):
        add_item = True  # Do we actually add the item to the queue
        with self.not_full:
            if self.maxsize > 0:
                if not block:
                    if self._is_full():
                        add_item = self._on_full(block=block, timeout=timeout)
                elif timeout is None:
                    while self._is_full():
                        add_item = self._on_full(block=block, timeout=timeout)
                elif timeout < 0:
                    raise ValueError("'timeout' must be a non-negative number")
                else:
                    endtime = time() + timeout
                    while self._is_full():
                        remaining = endtime - time()
                        if remaining <= 0.0:
                            raise Full
                        add_item = self._on_full(block=block, timeout=remaining)
            if add_item:
                self._put(item)
                self.put_get_metric.inc(1)
            if self._is_sampling_ready():
                self.not_empty.notify()

    def compute_buffer_statistics(self) -> Tuple[np.ndarray, np.ndarray]:
        raise NotImplementedError


class CounterMixin:
    def __init__(self):
        self.seen_ctr = Counter()

    def put(self, item : SimulationData, block: bool = True, timeout: Optional[float] = None):
        watched_item = BufferItem(item)
        super().put(watched_item, block, timeout)  # type: ignore

    def _get_with_eviction(self, index: Union[List, int]):
        items: Union[List[BufferItem], BufferItem] = (
            super()._get_with_eviction(index)  # type: ignore
        )
        if isinstance(items, list):
            item_data = []
            item_seen = []
            for item in items:
                item.seen += 1
                item_seen.append(item.seen)
                item_data.append(item.data)
            self.seen_ctr += Counter(item_seen)
            return item_data
        self.seen_ctr += Counter([items.seen + 1])
        return items.data

    def _get_without_eviction(self, index: Union[List, int]):
        items: Union[List[BufferItem], BufferItem] = (
            super()._get_without_eviction(index)  # type: ignore
        )
        if isinstance(items, list):
            for item in items:
                item.seen += 1
            return [item.data for item in items]
        items.seen += 1
        return items.data

    def __repr__(self) -> str:
        s = super().__repr__()
        return f"{s}: {self.seen_ctr}"


class ReceptionDependant:
    def __init__(self):
        self._is_reception_over = False

    def signal_reception_over(self):
        self._is_reception_over = True


class SamplingDependant:
    def _is_sampling_ready(self) -> bool:
        return True


class ThresholdMixin(SamplingDependant, ReceptionDependant):
    """Block when not enough data are available in the queue."""

    def __init__(self, threshold: int):
        ReceptionDependant.__init__(self)
        self.threshold = threshold

    def _is_sampling_ready(self: Threshold) -> bool:
        is_ready = (
            super()._is_sampling_ready()  # type: ignore
            and (self._size() > self.threshold)
        )
        return is_ready

    def signal_reception_over(self):
        with self.mutex:
            super().signal_reception_over()
            self.threshold = 0
            self.not_empty.notify()


class ReadingWithoutEvictionMixin(ReceptionDependant):
    def _get_from_index(self: GetProtocol, index: Union[list, int]):
        if self._is_reception_over:
            return self._get_with_eviction(index)
        return self._get_without_eviction(index)


class RandomQueue(BaseQueue):
    """Queue that randomly reads items from the list container. It evicts on reading."""

    def __init__(self, maxsize: int, pseudo_epochs: int = 1):
        super().__init__(maxsize)
        self.pseudo_epochs = pseudo_epochs

    def _init_queue(self):
        self.queue: List[SimulationData] = []

    def _get_index(self) -> int:
        index = random.randrange(self._size())
        return index

    def _get_without_eviction(self, index: Union[list, int]):
        if isinstance(index, int):
            return self.queue[index]
        elif isinstance(index, list):
            return [self.queue[i] for i in index]

    def _get_with_eviction(self, index: Union[list, int]):
        if isinstance(index, int):
            item = self.queue[index]
            del self.queue[index]
            return item
        elif isinstance(index, list):
            items = [self.queue[i] for i in index]
            for i in index:
                del self.queue[i]
            return items

    def _get_from_index(self, index: int):
        if self.pseudo_epochs <= 1:
            return self._get_with_eviction(index)
        return self._get_without_eviction(index)

    def _get(self):
        index = self._get_index()
        return self._get_from_index(index)


class ReservoirQueue(ReadingWithoutEvictionMixin, RandomQueue):
    """Queue implementing the reservoir sampling algorithm."""

    def __init__(self, maxsize: int):
        ReadingWithoutEvictionMixin.__init__(self)
        RandomQueue.__init__(self, maxsize)
        self.put_ctr: int = 0

    def _evict(self, index: int):
        del self.queue[index]

    def _on_full(self, block, timeout) -> bool:
        index = random.randrange(self.put_ctr)
        if index < self.maxsize:
            self._evict(index)
            return True
        return False

    def put(self, item, block=True, timeout=None):
        with self.mutex:
            RandomQueue.put(self, item, block, timeout)
            self.put_ctr += 1


class RandomEvictOnWriteQueue(ReadingWithoutEvictionMixin, RandomQueue):
    def __init__(self, maxsize: int):
        ReadingWithoutEvictionMixin.__init__(self)
        RandomQueue.__init__(self, maxsize)

    def _init_queue(self):
        self.not_seen = []
        self.seen = []

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}:"
            f"not yet seen samples {len(self.not_seen)}, already seen {len(self.seen)}"
        )

    def _size(self) -> int:
        return len(self.not_seen) + len(self.seen)

    def _evict(self, index):
        del self.seen[index]

    def _on_full(self, block, timeout) -> bool:
        if not block:
            if len(self.seen) == 0:
                raise Full
        else:
            if len(self.seen) == 0:
                timed_in = self.not_full.wait(timeout)
                if not timed_in:
                    return True
        index = random.randrange(len(self.seen))
        self._evict(index)
        return True

    def _get_with_eviction(self, index: Union[list, int]):
        if isinstance(index, int):
            if index < len(self.not_seen):
                item = self.not_seen[index]
                del self.not_seen[index]
            else:
                index = index - len(self.not_seen)
                item = self.seen[index]
                del self.seen[index]
            return item
        elif isinstance(index, list):
            not_seen_index = sorted([i for i in index if i < len(self.not_seen)], reverse=True)
            seen_index = sorted(
                [i - len(self.not_seen) for i in index if i >= len(self.not_seen)], reverse=True
            )
            items = [self.not_seen[i] for i in not_seen_index]
            items += [self.seen[i] for i in seen_index]
            for i in not_seen_index:
                del self.not_seen[i]
            for i in seen_index:
                del self.seen[i]
            return items

    def _get_without_eviction(self, index: Union[list, int]):
        if isinstance(index, int):
            if index < len(self.not_seen):
                item = self.not_seen[index]
                del self.not_seen[index]
                self.seen.append(item)
            else:
                index = index - len(self.not_seen)
                item = self.seen[index]
            return item

        elif isinstance(index, list):
            not_seen_index = [i for i in index if i < len(self.not_seen)]
            seen_index = [i - len(self.not_seen) for i in index if i >= len(self.not_seen)]
            items = []
            for i in not_seen_index:
                item = self.not_seen[i]
                items.append(item)
                del self.not_seen[i]
                self.seen.append(item)
            items += [self.seen[i] for i in seen_index]
            return items

    def _put(self, item):
        self.not_seen.append(item)


class BatchGetMixin(SamplingDependant, ReceptionDependant):
    def __init__(self, batch_size: int):
        ReceptionDependant.__init__(self)
        self.batch_size = batch_size

    def _is_sampling_ready(self):
        is_ready = super()._is_sampling_ready()
        if self._is_reception_over:
            # No more data will arrive, we may not be able to serve batch_size data
            return is_ready
        return is_ready and (self._size() >= self.batch_size)

    def _get(self):
        if not self._is_reception_over:
            population = self.batch_size
        else:
            population = min(self.batch_size, self._size())
        indices = sorted(random.sample(range(self._size()), k=population), reverse=True)
        items = self._get_from_index(indices)
        return items


class SimpleQueue(ReceptionDependant, BaseQueue):
    def __init__(self, maxsize: int = 0):
        ReceptionDependant.__init__(self)
        BaseQueue.__init__(self, maxsize)


class ThresholdQueue(CounterMixin, ThresholdMixin, RandomQueue):
    def __init__(self, maxsize: int, threshold: int, pseudo_epochs: int = 1):
        assert threshold <= maxsize
        CounterMixin.__init__(self)
        RandomQueue.__init__(self, maxsize, pseudo_epochs)
        ThresholdMixin.__init__(self, threshold)


class ThresholdReservoirQueue(CounterMixin, ThresholdMixin, ReservoirQueue):
    def __init__(self, maxsize: int, threshold: int):
        assert threshold <= maxsize
        CounterMixin.__init__(self)
        ReservoirQueue.__init__(self, maxsize)
        ThresholdMixin.__init__(self, threshold)


class ThresoldEvictOnWriteQueue(CounterMixin, ThresholdMixin, RandomEvictOnWriteQueue):
    def __init__(self, maxsize: int, threshold: int):
        assert threshold <= maxsize
        CounterMixin.__init__(self)
        ThresholdMixin.__init__(self, threshold)
        RandomEvictOnWriteQueue.__init__(self, maxsize)


class BatchThresholdEvictOnWriteQueue(BatchGetMixin, ThresoldEvictOnWriteQueue):
    def __init__(self, maxsize: int, threshold: int, batch_size: int):
        assert threshold <= maxsize
        assert batch_size <= maxsize
        ThresoldEvictOnWriteQueue.__init__(self, maxsize, threshold)
        BatchGetMixin.__init__(self, batch_size)
