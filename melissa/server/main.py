"""
Main Melissa script
"""
import argparse
import importlib.util
import sys
import rapidjson
import logging
import os
from pathlib import Path
from typing import Any, Dict
from melissa.launcher.__main__ import validate_config, CONFIG_PARSE_MODE

from melissa.utility.logger import configure_logger, get_log_level_from_verbosity
from melissa.utility.networking import get_rank_and_num_server_proc

logger = logging.getLogger(__name__)

try:
    rank = int(os.environ["SLURM_PROCID"])
    nb_proc_server = int(os.environ["SLURM_NTASKS"])
except KeyError:
    from mpi4py import MPI
    rank = MPI.COMM_WORLD.Get_rank()
    nb_proc_server = MPI.COMM_WORLD.Get_size()


def main() -> None:
    """
    This function initiates Melissa server.

    !!!
    This should not be called directly by the user, it is
    called by the `melissa-launcher`.
    !!!

    1. Parse flags for the project_dir (or others if necessary)
    2. Resolves custom server class
    3. Instantiates the server with user config
    4. Calls server.start()

    Now the server.sh file should be calling this function:
    exec python -m melissa
        --config_name /path/to/config

    """

    parser = argparse.ArgumentParser(
        prog="melissa", description="Melissa"
    )

    # CLI flags
    parser.add_argument(
        "--project_dir",
        help="Directory to all necessary files:\n"
        "   client.sh\n"
        "   server.sh\n"
        "   config.py\n"
        "   data_generator\n"
        "   CustomServer.py"
    )

    parser.add_argument(
        "--config_name",
        help="Defaults to `config` but user can change"
        "the search name by indicating it with this flag",
        default=None
    )

    args = parser.parse_args()
    if not args.config_name:
        conf_name = 'config'
    else:
        conf_name = args.config_name

    # load the config into a python dictionary
    with open(Path(args.project_dir) / f"{conf_name}.json") as json_file:
        config_dict = rapidjson.load(json_file, parse_mode=CONFIG_PARSE_MODE)

    config_dict['user_data_dir'] = Path('user_data')

    # ensure user passed the necessary information for software configuration
    args, config_dict = validate_config(args, config_dict)

    rank, _ = get_rank_and_num_server_proc()

    # set server log level
    sconfig = config_dict['study_options']  # this will become server_config
    log_level = get_log_level_from_verbosity(sconfig.get("verbosity", 3))
    configure_logger(f"melissa_server_{rank}.log", log_level)

    # Resolve the server
    myserver = get_resolved_server(args, config_dict)
    try:
        myserver.initialize_connections()
        myserver.start()
    except Exception as msg:
        logger.exception(f"Server failed with msg {msg}.")
        myserver.close_connection(1)


def get_resolved_server(args: argparse.Namespace, config_dict: Dict[str, Any]):

    server_file_name = config_dict.get("server_filename", "server.py")
    server_path = Path(args.project_dir) / server_file_name
    server_module_name = server_path.stem
    spec_server = importlib.util.spec_from_file_location(server_module_name, server_path)
    if spec_server and spec_server.loader:
        sys.path.append(str(server_path.parent))
        server = importlib.util.module_from_spec(spec_server)
        spec_server.loader.exec_module(server)
        server_class_name = config_dict.get("server_class", "MyServer")
        MyServerClass = getattr(server, server_class_name)
        my_server = MyServerClass(config_dict)
    else:
        logger.warning('Unable to import server')

    return my_server
