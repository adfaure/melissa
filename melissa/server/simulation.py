import logging
import struct
import time
from dataclasses import dataclass
from enum import Enum
from typing import Any, Dict, List, Optional, Union

import numpy as np
import numpy.typing as npt

logger = logging.getLogger(__name__)


@dataclass
class SimulationData:
    simulation_id: int
    time_step: int
    data: Any
    parameters: List

    def __repr__(self) -> str:
        s = (
            f"<{self.__class__.__name__} "
            f"simulation={self.simulation_id} "
            f"time step={self.time_step}>"
        )
        return s


class PartialSimulationData:

    MAX_FIELD_NAME_SIZE = 128

    def __init__(
        self,
        time_step: int,
        simulation_id: int,
        client_rank: int,
        data_size: int,
        field: str,
        data: Any,
    ):
        self.time_step = time_step
        self.simulation_id = simulation_id
        self.client_rank = client_rank
        self.data_size = data_size
        self.field = field
        self.data = data

    @classmethod
    def from_msg(cls, msg, learning):
        time_step: int
        simulation_id: int
        client_rank: int
        data_size: int
        field: bytearray

        # unpack metadata
        size_metadata: int = 4 * 4 + cls.MAX_FIELD_NAME_SIZE
        time_step, simulation_id, client_rank, data_size, field = struct.unpack(
            f"4i{cls.MAX_FIELD_NAME_SIZE}s", msg[: size_metadata]
        )
        field_name: str = field.split(b"\x00")[0].decode("utf-8")

        # unpack data array (float32 for DL and float64 for SA)
        if learning > 0:
            data: npt.ArrayLike = np.frombuffer(msg, offset=size_metadata, dtype="f")
        else:
            data: npt.ArrayLike = np.frombuffer(msg, offset=size_metadata, dtype="d")

        return cls(time_step, simulation_id, client_rank, data_size, field_name, data)

    def is_matching(self, simulation_data: "PartialSimulationData") -> bool:
        return (
            (self.field != simulation_data.field)
            and (self.simulation_id == simulation_data.simulation_id)
            and (self.time_step == simulation_data.time_step)
        )

    def __repr__(self) -> str:
        return (
            f"<{self.__class__.__name__}: simulation {self.simulation_id}, "
            f"time step {self.time_step}, field {self.field}"
        )


class SimulationDataStatus(Enum):
    PARTIAL = 0
    COMPLETE = 1
    ALREADY_RECEIVED = 2
    EMPTY = 3


class SimulationStatus(Enum):
    CONNECTED = 0
    RUNNING = 1
    FINISHED = 2


class Simulation:
    def __init__(self, id: int, n_time_steps: int, fields: List[str], parameters: List[Any]):
        self.id = id
        self.n_time_steps = n_time_steps
        self.fields = fields
        self.received_simulation_data: Dict[
            int, Dict[int, Dict[str, Optional[Union[int, PartialSimulationData]]]]
        ] = {}  # client_rank, time_step, field, int (SA) or PartialSimulationData (DL)
        self.received_time_steps: Dict[int, npt.NDArray[np.bool_]] = {}
        self.parameters: List[Any] = parameters
        self.n_received_time_steps: int = 0
        self.n_failures: int = 0
        self.last_message: Optional[float] = None

    def crashed(self) -> bool:
        return False

    def has_already_received(self, client_rank: int, time_step: int, field: str) -> bool:
        field_idx = self.fields.index(field)
        return self.received_time_steps[client_rank][field_idx, time_step]

    def is_complete(self, time_step: int) -> bool:
        for client_rank in self.received_simulation_data.keys():
            if time_step not in self.received_simulation_data[client_rank]:
                return False
            elif len(
                [
                    field
                    for field in self.received_simulation_data[client_rank][time_step].values()
                    if field is not None
                ]
            ) != len(self.fields):
                return False
        return True

    def _mark_as_received(self, client_rank: int, time_step: int, field: str):
        field_idx = self.fields.index(field)
        self.received_time_steps[client_rank][field_idx, time_step] = True
        self.last_message = time.time()

    def finished(self) -> bool:
        return self.n_received_time_steps == self.n_time_steps


class Group:
    def __init__(self, group_id: int):
        self.group_id = group_id
        self.simulations: Dict[int, Simulation] = {}
        self.n_failures: int = 0
