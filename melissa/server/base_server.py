from datetime import timedelta
import logging
import os
import socket
import threading
import time
from abc import ABC, abstractmethod
from enum import Enum
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Union

import numpy as np
import zmq
from mpi4py import MPI

from melissa.launcher import config, message
from melissa.scheduler import job
from melissa.server.fault_tolerance import FaultTolerance
from melissa.server.message import ConnectionRequest, ConnectionResponse
from melissa.server.simulation import (Group, PartialSimulationData,
                                       Simulation, SimulationData,
                                       SimulationDataStatus)
from melissa.utility.networking import (LengthPrefixFramingDecoder,
                                        LengthPrefixFramingEncoder,
                                        connect_to_launcher,
                                        get_rank_and_num_server_proc)
from melissa.utility.timer import Timer

logger = logging.getLogger(__name__)


class ServerStatus(Enum):
    CHECKPOINT = 1
    TIMEOUT = 2


def select_protocol():
    try:
        protocol = os.environ["MELISSA_LAUNCHER_PROTOCOL"]
    except KeyError:
        raise Exception("Undefined protocol")

    if protocol == "SCTP":
        return socket.IPPROTO_SCTP, "SCTP"
    elif protocol == "TCP":
        return socket.IPPROTO_TCP, "TCP"
    else:
        raise Exception(f"Unsupported server/launcher communication protocol {protocol}")


class BaseServer(ABC):
    def __init__(
        self,
        config: Dict[str, Any],
        checkpoint_file: str = "checkpoint.pkl",
        data_hwm: int = 4096,
        restart: bool = False,
    ):
        """
        Initializes the server, opens all connections and
        launches the study simulations
        """
        rank, nb_proc_server = get_rank_and_num_server_proc()
        self.config: Dict[str, Any] = config
        self.sweep_params: Dict[str, Any] = config.get("sweep_params", {})
        self.checkpoint_file: str = checkpoint_file
        self.num_server_proc: int = nb_proc_server
        self.study_options: Dict[str, Any] = self.config["study_options"]
        self.user_data_path = Path("user_data")
        self.rank: int = rank
        self._is_receiving: bool = False
        self._is_online: bool = False
        self.catch_error: bool = False

        # Variables for final report
        self.total_bytes_recv: int = 0
        self.t0: float = time.time()

        # MPI initialization
        self.sobol_op: int = 0
        self.comm = MPI.COMM_WORLD
        self.rank = self.comm.Get_rank()
        self.comm_size = self.comm.Get_size()
        self.client_comm_size: int = 0

        # Scan study options dictionary
        self.crashes_before_redraw = self.study_options.get("crashes_before_redraw", 1)
        self.max_delay = self.study_options.get("simulation_timeout", 60)
        self.rm_script = self.study_options.get("remove_client_scripts", False)
        try:
            self.fields = self.study_options["field_names"]
            self.nb_parameters = self.study_options["nb_parameters"]
            self.num_samples = self.study_options["num_samples"]
            self.parameter_sweep_size = self.study_options["parameter_sweep_size"]
            self.num_clients: int = self.parameter_sweep_size  # introduced for semantic clarity
        except Exception as e:
            logger.error(f"Incorrect study conf file: {e}")
            self.catch_error = True
        self.group_size = self.study_options.get("group_size", 1)
        self.zmq_hwm = self.study_options.get("zmq_hwm", 0)

        # Termination condition arguments
        self.number_of_groups: int = self.num_clients // self.group_size
        self.n_submitted_simulations: int = 0
        self.n_finished_simulations: int = 0
        self.n_connected_simulations: int = 0

        # Fault-Tolerance initialization
        self.no_fault_tolerance = (
            True if os.environ["MELISSA_FAULT_TOLERANCE"] == "OFF" else False
        )
        logger.info("fault-tolerance " + ("OFF" if self.no_fault_tolerance else "ON"))
        self.ft = FaultTolerance(
            self.no_fault_tolerance,
            self.max_delay,
            self.crashes_before_redraw,
            self.number_of_groups,
        )
        self.groups: Dict[int, Group] = {}

        # TODO: maybe this should be an MPI gather on rank 0.
        self.verbose_level = 0

        if restart:
            # TODO implement generic restart/checkpoint
            raise Exception("Error, this needs to be implemented")

    def initialize_connections(self):
        self.initialize_ports()
        # 0. Connect to launcher
        self.connect_to_launcher()
        # 1. Set up sockets
        self.setup_sockets()
        # 2. Setup poller
        self.setup_poller()
        # 3. Setup TimeMonitor object
        self.time_monitor = TimeMonitor(time.monotonic(), 30.)

        if self.config.get("vscode_debugging", False):
            self.start_debugger()

    def start_debugger(self):
        import debugpy
        # 5678 is the default attach port that we recommend users to set
        # in the documentation.
        debugpy.listen(5678)
        logger.warning("Waiting for debugger attach, please start the "
                       "debugger by navigating to debugger pane ctrl+shift+d "
                       "and selecting\n"
                       "Python: Remote Attach")
        debugpy.wait_for_client()
        logger.info("Debugger successfully attached.")
        # send message to launcher to ensure debugger doesnt timeout
        snd_msg = self.encode_msg(message.StopTimeoutMonitoring())
        self.launcherfd.send(snd_msg)

    def initialize_ports(self, connection_port: int = 2003, data_puller_port: int = 2006):
        # Ports initialization
        logger.info("Rank {} | Initializing server...".format(self.rank))
        self.node_name = socket.gethostname()
        self.connection_port = connection_port
        self.data_puller_port = str(data_puller_port) + str(self.rank)
        self.data_puller_port_name = "tcp://{}:{}".format(
            self.node_name,
            self.data_puller_port,
        )
        self.port_names = self.comm.allgather(self.data_puller_port_name)
        logger.debug("port_names {}".format(self.port_names))

    def connect_to_launcher(self):
        # Setup communication instances
        self.protocol, prot_name = select_protocol()
        if self.rank == 0:
            logger.info(f"Server/launcher communication protocol: {prot_name}")
            self.launcherfd: socket.socket = connect_to_launcher()
            logger.debug(f"Launcher fd set up: { self.launcherfd.fileno()}")
            self.launcherfd.send(self.encode_msg(message.GroupSize(self.group_size)))
            logger.debug(f"Group size {self.group_size} sent to launcher")

        # if an error was caught earlier the connection is immediately closed
        # this will prevent the server to be relaunched infinitely
        if self.catch_error:
            raise Exception("Incorrect configuration file")

    def setup_sockets(self):
        self.context = zmq.Context()
        # Simulations (REQ) <-> Server (REP)
        self.connection_responder = self.context.socket(zmq.REP)
        if self.rank == 0:
            logger.info("Rank {}: binding..".format(self.rank))
            addr = "tcp://*:{port}".format(port=self.connection_port)
            try:
                self.connection_responder.bind(addr)
            except Exception as e:
                logger.error("Rank {}: could not bind to {}".format(self.rank, addr))
                raise e
        # Simulations (PUSH) -> Server (PULL)
        self.data_puller = self.context.socket(zmq.PULL)
        self.data_puller.setsockopt(zmq.RCVHWM, self.zmq_hwm)
        self.data_puller.setsockopt(zmq.LINGER, -1)
        addr = "tcp://*:{}".format(self.data_puller_port)
        logger.info("Rank {}: connecting puller to: {}".format(self.rank, addr))
        self.data_puller.bind(addr)
        # Time-out checker (creates thread)
        self.timerfd_0, self.timerfd_1 = socket.socketpair(
            socket.AF_UNIX, socket.SOCK_STREAM
        )
        self.timer = Timer(self.timerfd_1, timedelta(seconds=self.max_delay))
        self.t_timer = threading.Thread(target=lambda: self.timer.run(), daemon=True)
        self.t_timer.start()

    def setup_poller(self):
        self.poller = zmq.Poller()
        self.poller.register(self.data_puller, zmq.POLLIN)
        self.poller.register(self.timerfd_0, zmq.POLLIN)
        if self.rank == 0:
            self.poller.register(self.launcherfd, zmq.POLLIN)
            self.poller.register(self.connection_responder, zmq.POLLIN)

    def launch_first_groups(self):
        """
        Launches the study groups
        """
        # Get current working directory containing the client script template
        client_script = os.path.abspath("client.sh")

        if not os.path.isfile(client_script):
            raise Exception("error client script not found")

        # Generate all client scripts
        self.generate_client_scripts(0, self.num_clients)

        # Launch every group
        # note that the launcher message stacking feature does not work
        for grp_id in range(self.number_of_groups):
            self.launch_group(grp_id)

    def generate_client_scripts(self, first_id, number_of_scripts, default_parameters=None):
        """
        Creates all required client.X.sh scripts and set up dict
        for fault tolerance
        """
        for sim_id in range(first_id, first_id + number_of_scripts):
            # if the number of scripts to be generated becomes too significant
            # the server may spend too much time in this loop hence causing
            # the launcher to believe that the server timed out if no PING is
            # received
            if number_of_scripts > 10000 and (sim_id - first_id) % 10000 == 0:
                self.time_monitor.check_clock(time.monotonic(), self)
            if default_parameters is not None:
                parameters = default_parameters
            else:
                parameters = self.draw_parameters()
            if self.rank == 0:
                client_script_i = os.path.abspath(f"./client_scripts/client.{str(sim_id)}.sh")
                Path('./client_scripts').mkdir(parents=True, exist_ok=True)
                with open(client_script_i, "w") as f:
                    print("#!/bin/sh", file=f)
                    print("exec env \\", file=f)
                    print(f"  MELISSA_SIMU_ID={sim_id} \\", file=f)
                    print(f"  MELISSA_SERVER_NODE_NAME={self.node_name} \\", file=f)
                    # str() conversion causes problems with scientific notations
                    # and should not be used
                    if self.rm_script:
                        print(
                            "  "
                            + " ".join(
                                [os.path.join(os.getcwd(), "client.sh")]
                                + [
                                    np.format_float_positional(x) if type(x) is not str
                                    else x for x in parameters
                                ]
                            )
                            + " &",
                            file=f,
                        )
                        print("  wait", file=f)
                        print('  rm "$0"', file=f)
                    else:
                        print(
                            "  "
                            + " ".join(
                                [os.path.join(os.getcwd(), "client.sh")]
                                + [
                                    np.format_float_positional(x) if type(x) is not str
                                    else x for x in parameters
                                ]
                            ),
                            file=f,
                        )

                os.chmod(client_script_i, 0o744)

            logger.info(
                f"Rank {self.rank}: created client.{sim_id}.sh with parameters {parameters}"
            )

            # Fault-tolerance dictionary creation and update
            group_id = sim_id // self.group_size
            if group_id not in self.groups:
                group = Group(group_id)
                self.groups[group_id] = group
            if sim_id not in self.groups[group_id].simulations:
                self.n_submitted_simulations += 1
                simulation = Simulation(
                    sim_id, self.num_samples, self.fields, parameters
                )
                self.groups[group_id].simulations[sim_id] = simulation

    def launch_group(self, group_id):
        """
        Launches group group_id
        """
        if self.rank == 0:
            # Job submission message to launcher (initial_id,num_jobs)
            snd_msg = self.encode_msg(message.JobSubmission(group_id, 1))
            self.launcherfd.send(snd_msg)
            logger.info(
                f"Rank {self.rank}: group "
                f"{group_id+1}/{self.number_of_groups} "
                "submitted to launcher"
            )

    def relaunch_group(self, group_id: int, new_grp: bool) -> None:
        """
        Relaunch a failed group with or without new parameters
        """
        if new_grp:
            del self.groups[group_id]
            for sim_id in range(self.group_size):
                self.generate_client_scripts(
                    group_id * self.group_size + sim_id, 1, None
                )
        else:
            for sim_id in range(self.group_size):
                self.generate_client_scripts(
                    group_id * self.group_size + sim_id,
                    1,
                    self.groups[group_id].simulations[sim_id].parameters,
                )

        self.launch_group(group_id)

    def handle_simulation_connection(self, msg):
        """
        New simulation connection. Executed by rank 0 only
        """
        request = ConnectionRequest.recv(msg)
        self.client_comm_size = request.comm_size
        logger.debug(
            f"Rank {self.rank}: [Connection] received connection message "
            f"from simulation {request.simulation_id} (client comm size {self.client_comm_size})"
        )
        logger.debug(
            f"Rank {self.rank}: [Connection] sending response to simulation {request.simulation_id}"
            f" with learning set to {self.learning}"
        )
        response = ConnectionResponse(
            self.comm_size,
            self.sobol_op,
            self.learning,
            self.nb_parameters,
            self.verbose_level,
            self.port_names,
        )
        self.connection_responder.send(response.encode())
        self.n_connected_simulations += 1
        logger.info(
            f"Rank {self.rank}: [Connection] connection established "
            f"with simulation {request.simulation_id}"
        )
        return "Connection"

    def run(self, timeout=10):
        """
        main function which handles the incoming messages from
        the launcher, the timer and the clients
        """
        # Checkpoint: this needs to be implemented specifically for melissa-dl

        # 1. Poll sockets
        # ZMQ sockets
        sockets = dict(self.poller.poll(timeout))
        if not sockets:
            return ServerStatus.TIMEOUT

        if self.rank == 0:
            # 2.  Look for connections  from new simulations (just at rank 0)
            if (
                self.connection_responder in sockets
                and sockets[self.connection_responder] == zmq.POLLIN
            ):
                msg = self.connection_responder.recv()
                logger.debug(f"Rank {self.rank}: Handle client connection request")
                self.handle_simulation_connection(msg)

            # 3. Handle launcher message
            # this is a TCP socket not a zmq one, so handled differently
            if self.launcherfd.fileno() in sockets:
                logger.debug(f"Rank {self.rank}: Handle launcher message")
                self.handle_fd()

        # 4. Handle simulation data message
        if self.data_puller in sockets and sockets[self.data_puller] == zmq.POLLIN:
            logger.debug(f"Rank {self.rank}: Handle simulation data")
            msg = self.data_puller.recv()
            self.total_bytes_recv += len(msg)
            return self.handle_simulation_data(msg)

        # 5. Handle timer message
        if self.timerfd_0.fileno() in sockets:
            logger.debug(f"Rank {self.rank}: Handle timer message")
            self.handle_timerfd()

    def handle_timerfd(self):
        """
        Handles timer messages
        """
        self.timerfd_0.recv(1)
        self.ft.check_time_out(self.groups)

        [
            self.relaunch_group(grp_id, new_grp)
            for grp_id, new_grp in self.ft.restart_grp.items()
        ]
        self.ft.restart_grp = {}

    def handle_fd(self):
        """
        Handles the launcher's messages through the filedescriptor
        """
        bs = self.launcherfd.recv(256)
        rcvd_msg = self.decode_msg(bs)

        for msg in rcvd_msg:
            # 1. Launcher sent JOB_UPDATE message (msg.job_id <=> group_id)
            if isinstance(msg, message.JobUpdate):
                group = self.groups[msg.job_id]
                group_id = group.group_id

                # React to simulation status
                if msg.job_state in [job.State.ERROR, job.State.FAILED]:
                    logger.debug("Launcher indicates job failure")
                    new_grp = self.ft.handle_failed_group(group_id, group)
                    self.relaunch_group(group_id, new_grp)

        # 2. Server sends PING
        logger.debug(
            "Server got message from launcher and sends PING back"
        )
        snd_msg = self.encode_msg(message.Ping())
        self.launcherfd.send(snd_msg)

    def decode_msg(self, byte_stream):
        msg_list = []
        if self.protocol == socket.IPPROTO_TCP:
            packets = LengthPrefixFramingDecoder(
                config.TCP_MESSAGE_PREFIX_LENGTH
            ).execute(byte_stream)
            for p in packets:
                msg_list.append(message.deserialize(p))
            logger.debug(f"Decoded launcher messages {msg_list}")
            return msg_list
        elif self.protocol == socket.IPPROTO_SCTP:
            msg_list.append(message.deserialize(byte_stream))
            logger.debug(f"Decoded launcher messages {msg_list}")
            return msg_list
        else:  # impossible case of figure
            raise Exception("Unsupported protocol")

    def encode_msg(self, msg):
        if self.protocol == socket.IPPROTO_TCP:
            encoded_packet = LengthPrefixFramingEncoder(
                config.TCP_MESSAGE_PREFIX_LENGTH
            ).execute(msg.serialize())
            return encoded_packet
        elif self.protocol == socket.IPPROTO_SCTP:
            return msg.serialize()
        else:  # impossible case of figure
            raise Exception("Unsupported protocol")

    def all_done(self) -> bool:
        """
        Checks whether all clients data were received
        """
        if self.n_finished_simulations == self.n_submitted_simulations:
            # join thread and close timer sockets
            logger.info(f"Rank {self.rank}: closes timer sockets")
            self.timerfd_0.close()
            self.poller.unregister(self.timerfd_0)
            self.t_timer.join(timeout=1)
            if self.t_timer.is_alive():
                logger.warning("timer thread did not terminate")
            else:
                self.timerfd_1.close()
            return True
        else:
            return False

    def close_connection(self, exit: int = 0):
        """
        Signals to the launcher that the study has ended.
        """
        snd_msg = self.encode_msg(message.Exit(exit))
        # 1. Syn server processes
        self.comm.Barrier()

        # 2. Send msg to the launcher
        if self.rank == 0:
            if exit > 0:
                logger.error("The server failed with an error")
            else:
                logger.info(f"Rank {self.rank}: turns the launcher off")
            self.launcherfd.send(snd_msg)
            return

    def write_final_report(self) -> None:
        """
        Write miscellaneous information about the analysis
        """
        # Number of simulation
        if self.rank == 0:
            logger.info(f" - Number of simulations: {self.num_clients}")
        # Number of simulation processes
        if self.rank == 0:
            logger.info(f" - Number of simulation processes: {self.client_comm_size}")
        # Number of analysis processes
        if self.rank == 0:
            logger.info(f" - Number of server processes: {self.comm_size}")
        # Total time
        total_time = np.zeros(1, dtype=float)
        self.comm.Allreduce(
            [np.array([time.time() - self.t0], dtype=float), MPI.DOUBLE],
            [total_time, MPI.DOUBLE],
            op=MPI.SUM
        )
        if self.rank == 0:
            logger.info(f" - Total time: {total_time[0] / self.comm_size} s")
        # Total MB received
        total_b = np.zeros(1, dtype=int)
        self.comm.Allreduce(
            [np.array([self.total_bytes_recv], dtype=int), MPI.INT],
            [total_b, MPI.INT],
            op=MPI.SUM
        )
        if self.rank == 0:
            logger.info(f" - MB received: {total_b[0] / 1024**2}  MB")

    # abstract methods that children must override.

    @abstractmethod
    def check_group_size(self):
        """
        Checks if the group size was correctly set
        """

    @abstractmethod
    def handle_simulation_data(self, msg) -> Union[SimulationData, PartialSimulationData]:
        """
        Parses and validates the incoming data messages from simulations. Unique to melissa flavors.
        """

    @abstractmethod
    def check_simulation_data(
        self, simulation: Simulation, simulation_data: PartialSimulationData
    ) -> Tuple[
        SimulationDataStatus,
        Union[Optional[SimulationData], Optional[PartialSimulationData]],
    ]:
        """
        Look for duplicated messages,
        update received_simulation_data and the simulation_data status.
        """
        return SimulationDataStatus.EMPTY, None

    @abstractmethod
    def setup_environment(self):
        """
        Any necessary setup methods go here. DeepMelissa needs
        e.g. dist.init_process_group
        """
        return

    @abstractmethod
    def receive(self) -> None:
        """
        Handle data coming from the server object
        """

    @abstractmethod
    def process_simulation_data(cls, msg: SimulationData, config: dict):
        """
        method used to custom process data. See example in
        LorenzLearner
        """
        return

    @abstractmethod
    def draw_parameters(self) -> List:
        """
        User defined method for defining the experimental study
        returns a parameter set
        """

    @abstractmethod
    def start(self):
        """
        The high level organization of events.
        """

    def other_processes_finished(self, batch_number: int) -> bool:
        """
        Ensure distributed server processes are coordinated for
        _is_receiving
        """
        return False

    @property
    def is_receiving(self):
        return self._is_receiving


class TimeMonitor():
    """This class implements a time monitor object to make sure that
    during the offline phase of the training the server keeps sending
    PINGs to the launcher at least every time_delay (units in sec)
    """

    def __init__(self, time: float, time_delay: float) -> None:
        self.last_ping = time
        self.time_delay = time_delay

    def check_clock(self, time: float, server: BaseServer) -> None:
        if time - self.last_ping > self.time_delay:
            server.run(10)
            self.last_ping = time
        else:
            pass
