from abc import ABC, abstractmethod
from typing import Deque, List
from collections import deque

import numpy as np
from scipy.stats import qmc


class ParameterGenerator(ABC):
    def __init__(self, seed: int):
        self.rng = np.random.default_rng(seed=seed)
        self.parameters: Deque = deque()
        self.populate_parameters()

    @abstractmethod
    def populate_parameters(self):
        """Create the parameters of the first simulations to run."""

    def draw_parameters(self):
        return self.parameters.popleft()

    def append_parameters(self, parameters):
        self.parameters.append(parameters)


class HaltonGenerator(ParameterGenerator):
    """
    Deterministic sample generator based on scipy Halton sequence
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.qmc.Halton.html
    """
    def __init__(self, nb_parameters: int, bounds: List[List[float]], parameter_sweep_size: int):
        self.parameters: Deque = deque()
        self.dimension = nb_parameters
        self.parameter_sweep_size = parameter_sweep_size
        # parameter boundaries are specified for each or the same interval is used for all
        if len(bounds) == 1 and self.dimension != 1:
            bounds = bounds * self.dimension
        else:
            if len(bounds) != self.dimension:
                raise Exception("Error, parameter_range must be consistent with nb_parameters")
        self.l_bounds = [b[0] for b in bounds]
        self.u_bounds = [b[1] for b in bounds]
        # sampler is a sequence generator that must be kept the same for the whole study
        self.sampler = qmc.Halton(d=self.dimension, scramble=False)
        self.populate_parameters()

    def populate_parameters(self):
        for _ in range(self.parameter_sweep_size):
            parameters = qmc.scale(self.sampler.random(1), self.l_bounds, self.u_bounds).tolist()
            self.parameters.extend(parameters)

    def add_parameters(self):
        parameters = qmc.scale(self.sampler.random(1), self.l_bounds, self.u_bounds).tolist()
        self.parameters.extend(parameters)

    def draw_parameters(self):
        try:
            return self.parameters.popleft()
        except IndexError:
            # this situation can arise if multiple failures require to draw a new parameter
            # for a given client or if additional clients are launched later in the study
            self.add_parameters()
            return self.parameters.popleft()


class LHSGenerator(ParameterGenerator):
    """
    Non-deterministic sample generator based on scipy LHS sampling
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.qmc.LatinHypercube.html
    """
    def __init__(
            self,
            seed: int,
            nb_parameters: int,
            bounds: List[List[float]],
            parameter_sweep_size: int
    ):
        self.rng = np.random.default_rng(seed=seed)
        self.parameters: Deque = deque()
        self.dimension = nb_parameters
        self.parameter_sweep_size = parameter_sweep_size
        # parameter boundaries are specified for each or the same interval is used for all
        if len(bounds) == 1 and self.dimension != 1:
            bounds = bounds * self.dimension
        else:
            if len(bounds) != self.dimension:
                raise Exception("Error, parameter_range must be consistent with nb_parameters")
        self.l_bounds = [b[0] for b in bounds]
        self.u_bounds = [b[1] for b in bounds]
        # sampler is a sequence generator that must be kept the same for the whole study
        self.sampler = qmc.LatinHypercube(d=self.dimension, scramble=True, seed=self.rng)
        self.populate_parameters()

    def populate_parameters(self):
        for _ in range(self.parameter_sweep_size):
            parameters = qmc.scale(self.sampler.random(1), self.l_bounds, self.u_bounds).tolist()
            self.parameters.extend(parameters)

    def add_parameters(self):
        parameters = qmc.scale(self.sampler.random(1), self.l_bounds, self.u_bounds).tolist()
        self.parameters.extend(parameters)

    def draw_parameters(self):
        try:
            return self.parameters.popleft()
        except IndexError:
            # this situation can arise if multiple failures require to draw a new parameter
            # for a given client or if additional clients are launched later in the study
            self.add_parameters()
            return self.parameters.popleft()
