import copy
import logging
import os
import time
from typing import Any, Dict, List, Optional, Tuple, Union
from pathlib import Path

import numpy as np
import numpy.typing as npt
from mpi4py import MPI

from melissa.launcher import message
from melissa.server.base_server import BaseServer
from melissa.server.simulation import (Group, PartialSimulationData, Simulation,
                                       SimulationData, SimulationDataStatus)
from melissa.server.sensitivity_analysis.iterative_statistics import (StatisticalMoments,
                                                                      IterativeSobolMartinez)

logger = logging.getLogger(__name__)


class SensitivityAnalysisServer(BaseServer):
    """
    Server to be used for Sensitivity Analysis studies.
    """

    def __init__(self, config: Dict[str, Any]):
        super().__init__(config)
        if self.num_samples == 0:
            raise Exception("Error, in case of an SA study num_samples must be set by the user")

        self.learning: int = 0
        self.sa_config: Dict[str, Any] = config["sa_config"]

        self.sobol_op = 1 if self.sa_config.get("sobol_indices", False) else 0
        self.check_group_size()

        self.mean = self.sa_config.get("mean", False)
        self.variance = self.sa_config.get("variance", False)
        self.skewness = self.sa_config.get("skewness", False)
        self.kurtosis = self.sa_config.get("kurtosis", False)
        self.min = self.sa_config.get("min", False)
        self.max = self.sa_config.get("max", False)
        self.threshold_exceedance = self.sa_config.get("threshold_exceedance", False)
        self.threshold_values = self.sa_config.get("threshold_values", [0.7, 0.8])
        self.quantiles = self.sa_config.get("quantiles", False)
        self.quantile_values = self.sa_config.get(
            "quantile_values", [0.05, 0.25, 0.5, 0.75, 0.95]
        )
        Path('./results/').mkdir(parents=True, exist_ok=True)

        # Instantiate the melissa statistical data structures
        self.max_order: int = 0
        self.melissa_moments: Dict[str, Dict] = {}  # {field, {clt_rank, {t, StatisticalMoments}}}
        if self.sobol_op:
            self.pick_freeze_matrix: List[List[Union[int, float]]] = []
            self.melissa_sobol: Dict[str, Dict] = {}  # {field, {clt_rank, {t, IterSobolMartinez}}}

        if self.kurtosis:
            self.max_order = 4
        elif self.skewness:
            self.max_order = 3
        elif self.variance:
            self.max_order = 2
        elif self.mean:
            self.max_order = 1
        else:
            self.max_order = 0

        if self.min or self.max:
            logging.warning("min max not implemented")
        if self.threshold_exceedance:
            logging.warning("threshold not implemented")
        if self.quantiles:
            logging.warning("quantiles not implemented")

        self.first_stat_computation: bool = True
        self.seen_ranks: List[int] = []  # list of seen client ranks

    def check_group_size(self):
        if self.sobol_op:
            self.group_size = self.nb_parameters + 2
            self.number_of_groups = self.parameter_sweep_size
            self.num_clients = self.group_size * self.parameter_sweep_size
        elif not self.sobol_op and self.group_size > 1 and self.num_clients % self.group_size != 0:
            logger.error("Incorrect group_size, please remove or adjust this option")
            self.catch_error = True
        else:
            pass

    def generate_client_scripts(self, first_id, number_of_scripts, default_parameters=None):
        """
        Creates all required client.X.sh scripts and set up dict
        for fault tolerance
        """
        for sim_id in range(first_id, first_id + number_of_scripts):
            # if the number of scripts to be generated becomes too significant
            # the server may spend too much time in this loop hence causing
            # the launcher to believe that the server timed out if no PING is
            # received
            if number_of_scripts > 10000 and (sim_id - first_id) % 10000 == 0:
                self.time_monitor.check_clock(time.monotonic(), self)
            if default_parameters is not None:
                parameters = default_parameters
            else:
                if not self.sobol_op:
                    parameters = self.draw_parameters()
                else:
                    parameters = self.draw_from_pick_freeze()
            if self.rank == 0:
                client_script_i = os.path.abspath(f"./client_scripts/client.{str(sim_id)}.sh")
                Path('./client_scripts').mkdir(parents=True, exist_ok=True)
                with open(client_script_i, "w") as f:
                    print("#!/bin/sh", file=f)
                    print("exec env \\", file=f)
                    print(f"  MELISSA_SIMU_ID={sim_id} \\", file=f)
                    print(f"  MELISSA_SERVER_NODE_NAME={self.node_name} \\", file=f)
                    # str() conversion causes problems with scientific notations
                    # and should not be used
                    if self.rm_script:
                        print(
                            "  "
                            + " ".join(
                                [os.path.join(os.getcwd(), "client.sh")]
                                + [
                                    np.format_float_positional(x) if type(x) is not str
                                    else x for x in parameters
                                ]
                            )
                            + " &",
                            file=f,
                        )
                        print("  wait", file=f)
                        print('  rm "$0"', file=f)
                    else:
                        print(
                            "  "
                            + " ".join(
                                [os.path.join(os.getcwd(), "client.sh")]
                                + [
                                    np.format_float_positional(x) if type(x) is not str
                                    else x for x in parameters
                                ]
                            ),
                            file=f,
                        )

                os.chmod(client_script_i, 0o744)

            logger.info(
                f"Rank {self.rank}: created client.{sim_id}.sh with parameters {parameters}"
            )

            # Fault-tolerance dictionary creation and update
            group_id = sim_id // self.group_size
            if group_id not in self.groups:
                group = Group(group_id)
                self.groups[group_id] = group
            if sim_id not in self.groups[group_id].simulations:
                self.n_submitted_simulations += 1
                simulation = Simulation(
                    sim_id, self.num_samples, self.fields, parameters
                )
                self.groups[group_id].simulations[sim_id] = simulation

    def draw_from_pick_freeze(self) -> List:
        """
        Returns a row from the pick-freeze matrix
        """
        if len(self.pick_freeze_matrix) > 0:
            return self.pick_freeze_matrix.pop(0)
        else:
            logging.debug("Build pick-freeze matrix")
            self.build_pick_freeze_matrix()
            return self.pick_freeze_matrix.pop(0)

    def build_pick_freeze_matrix(self):
        """
        Builds the pick-freeze matrix for one group
        """
        param_set_a: List = self.draw_parameters()
        param_set_b: List = self.draw_parameters()

        param_set: List = []
        param_set.append(copy.deepcopy(param_set_a))
        param_set.append(copy.deepcopy(param_set_b))
        for i in range(self.nb_parameters):
            param_set.append(copy.deepcopy(param_set[0]))
            param_set[i + 2][i] = copy.deepcopy(param_set[1][i])
        self.pick_freeze_matrix = param_set

    def start(self):
        """
        The main execution method
        """
        self.launch_first_groups()

        self.setup_environment()

        self.server_online()

        self.server_offline()

        self.server_finalize()

    def server_online(self):
        """
        Method where user controls the data handling while
        server is online.
        """
        self.receive()
        return

    def receive(self):
        """Handle data from the server."""
        self._is_receiving = True
        received_samples = 0
        while not self.all_done():
            status = self.run()
            if status is not None:
                if isinstance(status, PartialSimulationData):
                    logger.debug(
                        f"receive message: sim_id {status.simulation_id}, "
                        f"timestep {status.time_step}",
                    )
                    received_samples += 1

                    # compute the statistics on the received data
                    self.compute_stats(status)

        self._is_receiving = False

    def handle_simulation_data(self, msg):
        """
        Parses and validates the incoming data messages from simulations
        """
        # 1. Deserialize message
        msg_data: PartialSimulationData = PartialSimulationData.from_msg(msg, self.learning)
        logger.debug(
            f"Rank {self.rank} received {msg_data}  from rank {msg_data.client_rank} "
            f"(vect_size: {len(msg_data.data)})"
        )
        # 2. Apply filters
        if not (0 <= msg_data.time_step < self.num_samples):
            logger.warning(
                f"Rank {self.rank}: bad timestep {msg_data.time_step}"
            )
            return None
        if msg_data.field not in self.fields:
            logger.warning(f"Rank {self.rank}: bad field {msg_data.field}")
            return None

        # when sobol_op=1 the results of each simulation in the group are gathered on
        # the ranks of its first simulation and are sent at once by each rank
        # which means that len(msg_data.data) = group_size * data_size
        # in addition msg_data.simulation_id is actually the group_id
        for sim in range(len(msg_data.data) // msg_data.data_size):
            if not self.sobol_op:
                group_id = msg_data.simulation_id // self.group_size
                sim_id = msg_data.simulation_id
            else:
                group_id = msg_data.simulation_id
                sim_id = group_id * self.group_size + sim
            simulation = self.groups[group_id].simulations[sim_id]

            simulation_status, simulation_data = self.check_simulation_data(
                simulation, msg_data
            )
            if simulation_status == SimulationDataStatus.COMPLETE:
                logger.debug(
                    f"Rank {self.rank}: assembled time-step {simulation_data.time_step} "
                    f"- simulationID {simulation_data.simulation_id}"
                )
            elif simulation_status == SimulationDataStatus.ALREADY_RECEIVED:
                logger.warning(f"Rank {self.rank}: duplicate simulation data {msg_data}")

            # Check if simulation has finished
            if (
                simulation_status == SimulationDataStatus.COMPLETE
                or simulation_status == SimulationDataStatus.EMPTY
            ) and simulation.finished():
                logger.info(f"Rank {self.rank}: simulation {simulation.id} finished")
                self.n_finished_simulations += 1

        return simulation_data

    def check_simulation_data(
        self, simulation: Simulation, simulation_data: PartialSimulationData
    ) -> Tuple[
        SimulationDataStatus,
        Union[Optional[SimulationData], Optional[PartialSimulationData]],
    ]:
        """
        Look for duplicated messages,
        update received_simulation_data and the simulation_data status.
        """
        if simulation_data.client_rank not in simulation.received_simulation_data:
            simulation.received_simulation_data[simulation_data.client_rank] = {}
            simulation.received_time_steps[simulation_data.client_rank] = (
                np.zeros((len(self.fields), self.num_samples), dtype=bool)
            )
        # Data have already been received
        if simulation.has_already_received(
            simulation_data.client_rank, simulation_data.time_step, simulation_data.field
        ):
            return SimulationDataStatus.ALREADY_RECEIVED, None
        # Time step has never been seen
        if simulation_data.time_step not in simulation.received_simulation_data[
            simulation_data.client_rank
        ]:
            simulation.received_simulation_data[simulation_data.client_rank][
                simulation_data.time_step
            ] = {field: None for field in simulation.fields}
        # Update the entry
        # for SA it is more memory efficient not to keep track of the whole simulation_data
        simulation.received_simulation_data[simulation_data.client_rank][
            simulation_data.time_step
        ][simulation_data.field] = 1
        simulation._mark_as_received(
            simulation_data.client_rank, simulation_data.time_step, simulation_data.field
        )
        if simulation.is_complete(simulation_data.time_step):
            # All fields have been received for the time step
            simulation.n_received_time_steps += 1
            # Check there is actual data
            is_empty = simulation_data.data_size == 0
            if is_empty:
                # Data have been set to another device, fields are empty
                del simulation.received_simulation_data[simulation_data.client_rank][
                    simulation_data.time_step
                ]
                return SimulationDataStatus.EMPTY, None

            del simulation.received_simulation_data[simulation_data.client_rank][
                simulation_data.time_step
            ]
            return SimulationDataStatus.COMPLETE, simulation_data
        else:
            # Not all fields have been received yet
            return SimulationDataStatus.PARTIAL, simulation_data

    def setup_environment(self):
        return super().setup_environment()

    def server_offline(self):
        """
        Post processing goes here. Not required.
        """
        self.melissa_write_stats()
        return

    def server_finalize(self):
        """
        All finalization methods go here.
        """
        logger.info("stop server")
        self.write_final_report()
        self.close_connection()
        return

    def process_simulation_data(cls, msg: SimulationData, config: dict):
        """
        method used to custom process sa-data
        """
        return

    def compute_stats(self, pdata: PartialSimulationData) -> None:
        """
        Link into stats lib for computing online statistics.
        """
        if self.first_stat_computation:
            self.first_stat_computation = False
            for field in self.fields:
                self.melissa_moments[field] = {}
                if self.sobol_op:
                    self.melissa_sobol[field] = {}

        # Progressive initialization of the client_rank entry
        # so that we do not iterate over client_comm_size which
        # has not been broadcasted yet
        if pdata.client_rank not in self.seen_ranks:
            self.seen_ranks.append(pdata.client_rank)
            for field in self.fields:
                self.melissa_moments[field][pdata.client_rank] = {}
                if self.sobol_op:
                    self.melissa_sobol[field][pdata.client_rank] = {}
                for t in range(self.num_samples):
                    self.melissa_moments[field][pdata.client_rank][t] = (
                        StatisticalMoments(pdata.data_size, self.max_order)
                    )
                    if self.sobol_op:
                        self.melissa_sobol[field][pdata.client_rank][t] = (
                            IterativeSobolMartinez(pdata.data_size, self.nb_parameters)
                        )

        # when sobol_op=1, results are grouped thus pdata.data contains the solution
        # vectors of each simulation in the group and must be reshaped
        # since only the first two solutions are used to compute the moments
        np_data = pdata.data.reshape(-1, pdata.data_size)
        self.melissa_moments[
            pdata.field
        ][pdata.client_rank][pdata.time_step].increment_moments(np_data[0])

        if self.sobol_op:
            # increment the sobol data structure
            self.melissa_sobol[
                pdata.field
            ][pdata.client_rank][pdata.time_step].increment(np_data)
            # increment the moments with the second solution
            self.melissa_moments[
                pdata.field
            ][pdata.client_rank][pdata.time_step].increment_moments(np_data[1])

    def melissa_write_stats(self):
        """
        Write the computed statistics on file.
        """
        # Turn server monitoring off
        if self.rank == 0:
            snd_msg = self.encode_msg(message.StopTimeoutMonitoring())
            self.launcherfd.send(snd_msg)

        # Brodcast client_comm_size to all server ranks
        client_comm_size: int = self.client_comm_size
        if self.rank == 0:
            self.comm.bcast(client_comm_size, root=0)
        else:
            client_comm_size = self.comm.bcast(client_comm_size, root=0)
        self.client_comm_size = client_comm_size
        logger.info(f"Rank: {self.rank}, gathered client comm size: {self.client_comm_size}")

        # Update melissa_moments with missing client ranks
        for field in self.fields:
            for client_rank in range(self.client_comm_size):
                if client_rank not in self.melissa_moments[field]:
                    self.melissa_moments[field][client_rank] = {}
                    for t in range(self.num_samples):
                        self.melissa_moments[field][client_rank][t] = (
                            StatisticalMoments(0, self.max_order)
                        )

        temp_offset: int = 0
        local_vect_sizes: npt.ArrayLike = np.zeros(self.comm_size, dtype=int)
        vect_size: npt.ArrayLike = np.zeros(1, dtype=int)
        global_vect_size: int = 0

        # Compute the global vect size
        field = self.fields[0]
        for client_rank in self.melissa_moments[field].keys():
            vect_size += np.size(self.melissa_moments[field][client_rank][0].m1)

        self.comm.Allgather([vect_size, MPI.INT], [local_vect_sizes, MPI.INT])
        global_vect_size = np.sum(local_vect_sizes)
        logger.info(f"global_vect: {global_vect_size}")

        d_buffer = np.zeros(global_vect_size)

        if self.mean:
            self.comm.Barrier()
            for field in self.fields:
                for t in range(self.num_samples):
                    file_name = "./results/results.{}_{}.{}".format(
                        field,
                        "mean",
                        str(t + 1).zfill(len(str(self.num_samples)))
                    )
                    if self.rank == 0:
                        logger.info(f"file name: {file_name}")
                    for rank in range(self.client_comm_size):
                        if np.size(self.melissa_moments[field][rank][t].m1) > 0:
                            d_buffer[
                                temp_offset:temp_offset
                                + np.size(self.melissa_moments[field][rank][t].m1)
                            ] = self.melissa_moments[field][rank][t].m1
                            temp_offset += np.size(self.melissa_moments[field][rank][t].m1)
                    temp_offset = 0
                    d_buffer = self.gather_data(local_vect_sizes, d_buffer)
                    if self.rank == 0:
                        np.savetxt(file_name, d_buffer)

        if self.variance:
            self.comm.Barrier()
            for field in self.fields:
                for t in range(self.num_samples):
                    file_name = "./results/results.{}_{}.{}".format(
                        field,
                        "variance",
                        str(t + 1).zfill(len(str(self.num_samples)))
                    )
                    if self.rank == 0:
                        logger.info(f"file name: {file_name}")
                    for rank in range(self.client_comm_size):
                        if np.size(self.melissa_moments[field][rank][t].m1) > 0:
                            d_buffer[
                                temp_offset:temp_offset
                                + np.size(self.melissa_moments[field][rank][t].theta2)
                            ] = self.melissa_moments[field][rank][t].theta2
                            temp_offset += np.size(self.melissa_moments[field][rank][t].theta2)
                    temp_offset = 0
                    d_buffer = self.gather_data(local_vect_sizes, d_buffer)
                    if self.rank == 0:
                        np.savetxt(file_name, d_buffer)

        if self.skewness:
            self.comm.Barrier()
            for field in self.fields:
                for t in range(self.num_samples):
                    file_name = "./results/results.{}_{}.{}".format(
                        field,
                        "skewness",
                        str(t + 1).zfill(len(str(self.num_samples)))
                    )
                    if self.rank == 0:
                        logger.info(f"file name: {file_name}")
                    for rank in range(self.client_comm_size):
                        if np.size(self.melissa_moments[field][rank][t].m1) > 0:
                            crank_skewness = self.melissa_moments[field][rank][t].compute_skewness()
                            d_buffer[
                                temp_offset:temp_offset
                                + np.size(crank_skewness)
                            ] = crank_skewness
                            temp_offset += np.size(crank_skewness)
                    temp_offset = 0
                    d_buffer = self.gather_data(local_vect_sizes, d_buffer)
                    if self.rank == 0:
                        np.savetxt(file_name, d_buffer)
            # free memory
            del crank_skewness

        if self.kurtosis:
            self.comm.Barrier()
            for field in self.fields:
                for t in range(self.num_samples):
                    file_name = "./results/results.{}_{}.{}".format(
                        field,
                        "kurtosis",
                        str(t + 1).zfill(len(str(self.num_samples)))
                    )
                    if self.rank == 0:
                        logger.info(f"file name: {file_name}")
                    for rank in range(self.client_comm_size):
                        if np.size(self.melissa_moments[field][rank][t].m1) > 0:
                            crank_kurtosis = self.melissa_moments[field][rank][t].compute_kurtosis()
                            d_buffer[
                                temp_offset:temp_offset
                                + np.size(crank_kurtosis)
                            ] = crank_kurtosis
                            temp_offset += np.size(crank_kurtosis)
                    temp_offset = 0
                    d_buffer = self.gather_data(local_vect_sizes, d_buffer)
                    if self.rank == 0:
                        np.savetxt(file_name, d_buffer)
            # free memory
            del crank_kurtosis

        if self.sobol_op:
            self.comm.Barrier()
            for field in self.fields:
                for param in range(self.nb_parameters):
                    for t in range(self.num_samples):
                        file_name = "./results/results.{}_{}{}.{}".format(
                            field,
                            "sobol",
                            str(param),
                            str(t + 1).zfill(len(str(self.num_samples)))
                        )
                        if self.rank == 0:
                            logger.info(f"file name: {file_name}")
                        for rank in range(self.client_comm_size):
                            if np.size(
                                self.melissa_sobol[field][rank][t].
                                first_order[param]
                            ) > 0:
                                d_buffer[
                                    temp_offset:temp_offset
                                    + np.size(
                                        self.melissa_sobol[field][rank][t].
                                        first_order[param]
                                    )
                                ] = (
                                    self.melissa_sobol[field][rank][t].
                                    first_order[param]
                                )
                                temp_offset += np.size(
                                    self.melissa_sobol[field][rank][t].
                                    first_order[param]
                                )
                        temp_offset = 0
                        d_buffer = self.gather_data(local_vect_sizes, d_buffer)
                        if self.rank == 0:
                            np.savetxt(file_name, d_buffer)

            for field in self.fields:
                for param in range(self.nb_parameters):
                    for t in range(self.num_samples):
                        file_name = "./results/results.{}_{}{}.{}".format(
                            field,
                            "sobol_tot",
                            str(param),
                            str(t + 1).zfill(len(str(self.num_samples)))
                        )
                        if self.rank == 0:
                            logger.info(f"file name: {file_name}")
                        for rank in range(self.client_comm_size):
                            if np.size(
                                self.melissa_sobol[field][rank][t].
                                total_order[param]
                            ) > 0:
                                d_buffer[
                                    temp_offset:temp_offset
                                    + np.size(
                                        self.melissa_sobol[field][rank][t].
                                        total_order[param]
                                    )
                                ] = (
                                    self.melissa_sobol[field][rank][t].
                                    total_order[param]
                                )
                                temp_offset += np.size(
                                    self.melissa_sobol[field][rank][t].
                                    total_order[param]
                                )
                        temp_offset = 0
                        d_buffer = self.gather_data(local_vect_sizes, d_buffer)
                        if self.rank == 0:
                            np.savetxt(file_name, d_buffer)

    def gather_data(
        self,
        local_vect_sizes: npt.NDArray[np.int_],
        d_buffer: npt.NDArray[np.float_]
    ) -> npt.NDArray[np.float_]:
        """
        Gather data on rank 0.
        """
        temp_offset: int = 0
        if self.rank == 0:
            for rank in range(1, self.comm_size):
                temp_offset += local_vect_sizes[rank - 1]
                if local_vect_sizes[rank] > 0:
                    d_buffer[
                        temp_offset:temp_offset
                        + local_vect_sizes[rank]
                    ] = self.comm.recv(source=rank)
            temp_offset = 0
        else:
            if local_vect_sizes[self.rank] > 0:
                self.comm.send(d_buffer[:local_vect_sizes[self.rank]], dest=0)

        return d_buffer
