import copy
import numpy as np
import numpy.typing as npt

from typing import List


class StatisticalMoments:
    """
    This class implements a data structure of moments similar to the one
    in the original Melissa version
    """
    def __init__(self, np_data_size: int, max_order: int):
        self.max_order: int = max_order
        self.increment: int = 0

        self.m1: npt.NDArray[np.float_] = np.zeros(np_data_size)
        if self.max_order > 1:
            self.m2: npt.NDArray[np.float_] = np.zeros(np_data_size)
            self.theta2 = np.zeros(np.size(np_data_size))
        if self.max_order > 2:
            self.m3: npt.NDArray[np.float_] = np.zeros(np_data_size)
            self.theta3 = np.zeros(np.size(np_data_size))
        if self.max_order > 3:
            self.m4: npt.NDArray[np.float_] = np.zeros(np_data_size)
            self.theta4 = np.zeros(np_data_size)

    def increment_moments(self, np_data: npt.NDArray[np.float_]):
        """
        This function increment the various orders of moments
        """
        self.increment += 1

        if self.max_order < 1:
            return

        # update the mean
        self.m1 = self.increment_moments_mean(self.m1, np_data, 1)
        if self.max_order > 1:
            self.m2 = self.increment_moments_mean(self.m2, np_data, 2)
        if self.max_order > 2:
            self.m3 = self.increment_moments_mean(self.m3, np_data, 3)
        if self.max_order > 3:
            self.m4 = self.increment_moments_mean(self.m4, np_data, 4)

        # update the thetas
        if self.increment > 1:
            if self.max_order > 1:
                self.theta2 = self.m2 - np.square(self.m1)
            if self.max_order > 2:
                self.theta3 = self.m3 - 3 * self.m1 * self.m2 + 2 * np.power(self.m1, 3)
            if self.max_order > 3:
                self.theta4 = (
                    self.m4 - 4 * self.m1 * self.m3 + 6 * np.square(self.m1) * self.m2
                    - 3 * np.power(self.m1, 4)
                )

    def increment_moments_mean(
        self, moment: npt.NDArray[np.float_], np_data, power
    ) -> npt.NDArray[np.float_]:
        """
        This function computes the iterative mean of the given moment
        """
        return moment + (np.power(np_data, power) - moment) / self.increment

    def compute_skewness(self) -> npt.NDArray[np.float_]:
        return self.theta3 / np.power(self.theta2, 1.5)

    def compute_kurtosis(self) -> npt.NDArray[np.float_]:
        return self.theta4 / np.power(self.theta3, 2)


class IterativeMean:
    """
    This class implements the iterative mean computation
    """
    def __init__(self, np_data_size: int):
        self.iteration: int = 0
        self.stat: npt.NDArray[np.float_] = np.zeros(np_data_size)

    def increment(self, np_data: npt.NDArray[np.float_]):
        self.iteration += 1
        self.stat = (
            self.stat + (np_data - self.stat)
            / self.iteration
        )


class IterativeVariance:
    """
    This class implements the iterative variance
    """
    def __init__(self, np_data_size: int):
        self.iteration: int = 0
        self.stat: npt.NDArray[np.float_] = np.zeros(np_data_size)
        self.mean = IterativeMean(np_data_size)
        self.centered_squares: npt.NDArray[np.float_] = np.zeros(np_data_size)

    def increment(self, np_data: npt.NDArray[np.float_]):
        self.iteration += 1
        if self.iteration > 1:
            self.centered_squares += (
                (self.iteration - 1) * np.square(np_data - self.mean.stat) / self.iteration
            )

        self.mean.increment(np_data)

        if self.iteration > 1:
            self.stat = self.centered_squares / (self.iteration - 1)


class IterativeCovariance:
    """
    This class implements the iterative variance
    """
    def __init__(self, np_data_size: int):
        self.iteration: int = 0
        self.stat: npt.NDArray[np.float_] = np.zeros(np_data_size)
        self.mean_1 = IterativeMean(np_data_size)
        self.mean_2 = IterativeMean(np_data_size)

    def increment(
            self,
            in_vect1: npt.NDArray[np.float_],
            in_vect2: npt.NDArray[np.float_]
    ):
        # increment means
        self.iteration += 1
        prev_mean_1 = copy.deepcopy(self.mean_1.stat)
        prev_mean_2 = copy.deepcopy(self.mean_2.stat)
        self.mean_1.increment(in_vect1)
        self.mean_2.increment(in_vect2)

        # update covariance
        if self.iteration > 1:
            self.stat *= (self.iteration - 2)
            x = np.multiply(in_vect1 - prev_mean_1, in_vect2 - prev_mean_2)
            diff_mean = np.multiply(prev_mean_1 - self.mean_1.stat, prev_mean_2 - self.mean_2.stat)
            self.stat += x - diff_mean * self.iteration
            # self.stat += (
            #     np.multiply(in_vect1 - prev_mean_1, in_vect2 - prev_mean_2)
            #     - np.multiply(prev_mean_1 - self.mean_1.stat, prev_mean_2 - self.mean_2.stat)
            #     * self.iteration
            # )
            self.stat /= (self.iteration - 1)


class IterativeSobolMartinez:
    """
    This class implements the Sobol indices computation with Martinez formula
    """
    def __init__(self, np_data_size: int, nb_parameters: int):
        self.iteration: int = 0
        self.nb_parameters: int = nb_parameters
        self.first_order: npt.NDArray[np.float_] = np.zeros((nb_parameters, np_data_size))
        self.total_order: npt.NDArray[np.float_] = np.zeros((nb_parameters, np_data_size))
        self.variance_a: IterativeVariance = IterativeVariance(np_data_size)
        self.variance_b: IterativeVariance = IterativeVariance(np_data_size)
        self.variance_e: List[IterativeVariance] = [
            IterativeVariance(np_data_size) for _ in range(self.nb_parameters)
        ]
        self.covariance_ae: List[IterativeCovariance] = [
            IterativeCovariance(np_data_size) for _ in range(self.nb_parameters)
        ]
        self.covariance_be: List[IterativeCovariance] = [
            IterativeCovariance(np_data_size) for _ in range(self.nb_parameters)
        ]

    def increment(self, np_group_data: npt.NDArray[np.float_]):
        epsilon: float = 1e-12
        sample_a: npt.NDArray[np.float_] = np_group_data[0]
        sample_b: npt.NDArray[np.float_] = np_group_data[1]
        sample_e: npt.NDArray[np.float_] = np_group_data[2:]

        self.iteration += 1
        self.variance_a.increment(sample_a)
        self.variance_b.increment(sample_b)

        for p in range(self.nb_parameters):
            self.variance_e[p].increment(sample_e[p])

            # increment 1st order
            self.covariance_be[p].increment(sample_b, sample_e[p])

            n_idx = np.logical_or(
                self.variance_b.stat < epsilon,
                self.variance_e[p].stat < epsilon
            )
            self.first_order[p][np.logical_not(n_idx)] = np.divide(
                self.covariance_be[p].stat[np.logical_not(n_idx)],
                np.sqrt(
                    np.multiply(
                        self.variance_b.stat[np.logical_not(n_idx)],
                        self.variance_e[p].stat[np.logical_not(n_idx)]
                    )  # noqa: E501
                )
            )
            self.first_order[p][n_idx] = 0.

            # increment total order
            self.covariance_ae[p].increment(sample_a, sample_e[p])

            n_idx = np.logical_or(
                self.variance_a.stat < epsilon,
                self.variance_e[p].stat < epsilon
            )
            self.total_order[p][np.logical_not(n_idx)] = 1 - np.divide(
                self.covariance_ae[p].stat[np.logical_not(n_idx)],
                np.sqrt(
                    np.multiply(
                        self.variance_a.stat[np.logical_not(n_idx)],
                        self.variance_e[p].stat[np.logical_not(n_idx)]
                    )  # noqa: E501
                )
            )
            self.total_order[p][n_idx] = 0.
