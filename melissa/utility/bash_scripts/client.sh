#!/bin/sh
set -x

# Melissa will paste the `preprocessing_instructions`
# here

# melissa-launcher will find and replace 'melissa_set_env_file'
# automatically, do not change this line.
. {melissa_set_env_file}

# User can set this part of the client script up automatically
# by ensuring that the keys in their `client_config` dictionary
# match the keywords below.  
# For example:
# melissa-launcher will search and replace "executable_command"
# automatically with the "executable_command" set in the 
# client_config file.

exec {executable_command} "$@"

