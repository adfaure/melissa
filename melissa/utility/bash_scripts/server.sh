#!/bin/sh
set -x

# Melissa will paste the `preprocessing_instructions`
# here

# the remainder of this file should be left untouched. 
# melissa-launcher will find and replace values in 
# curly brackets (e.g. {melissa_set_env_file}) with 
# the proper values.
. {melissa_set_env_file}

echo "DATE                      =$(date)"
echo "Hostname                  =$(hostname -s)"
echo "Working directory         =$(pwd)"
echo ""
echo $PYTHONPATH

set -e

exec melissa-server --project_dir {path_to_usecase} --config_name {config_name}
