import logging


def configure_logger(log_filename: str, log_level: int = logging.INFO):
    logger = logging.getLogger("melissa")
    logger.setLevel(log_level)
    file_handler = logging.FileHandler(filename=log_filename, mode="w")
    formatter = logging.Formatter(
        style="{",
        fmt="{asctime}:{name}:{levelname} {message:}",
    )
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)


def get_log_level_from_verbosity(verbosity: int) -> int:
    if verbosity >= 3:
        return logging.DEBUG
    elif verbosity == 2:
        return logging.INFO
    elif verbosity == 1:
        return logging.WARNING
    elif verbosity == 0:
        return logging.ERROR
    else:
        return logging.DEBUG
