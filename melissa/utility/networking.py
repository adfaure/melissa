#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__all__ = [
    "ConnectionId", "LengthPrefixFramingEncoder", "LengthPrefixFramingDecoder",
    "Socket", "connect_to_launcher", "connect_to_server",
    "get_available_protocols", "make_passive_socket", "pipe", "protocol2str",
    "socketpair", "str2protocol"
]

import os
import errno
import logging
import socket
from socket import socket as Socket
from typing import List, Optional, Tuple

logger = logging.getLogger(__name__)

ConnectionId = int


class LengthPrefixFramingDecoder:
    def __init__(self, prefix_length: int) -> None:
        assert prefix_length <= 8
        if prefix_length <= 0:
            raise ValueError(
                "prefix length must be positive, got {:d}".
                format(prefix_length)
            )
        self._prefix_length = prefix_length
        self._buffer = b''

    def execute(self, bs: bytes) -> List[bytes]:
        xs = self._buffer + bs

        messages = []  # type: List[bytes]
        while len(xs) >= self._prefix_length:
            num_message_bytes = int.from_bytes(
                xs[:self._prefix_length], byteorder="little"
            )

            if self._prefix_length + num_message_bytes > len(xs):
                break

            messages.append(
                xs[self._prefix_length:self._prefix_length + num_message_bytes]
            )
            xs = xs[self._prefix_length + num_message_bytes:]

        self._buffer = xs
        return messages


class LengthPrefixFramingEncoder:
    def __init__(self, prefix_length: int) -> None:
        assert prefix_length <= 8
        if prefix_length <= 0:
            raise ValueError(
                "prefix length must be positive, got {:d}".
                format(prefix_length)
            )
        self._prefix_length = prefix_length

    def execute(self, bs: bytes) -> bytes:
        n = len(bs)
        bits_per_byte = 8
        max_message_length = 1 << (self._prefix_length * bits_per_byte)
        if n >= max_message_length:
            raise RuntimeError(
                "message of length {:d} too large for length"
                "prefix framing with a prefix of length {:d}"
                .format(n, self._prefix_length)
            )

        prefix = n.to_bytes(self._prefix_length, byteorder="little")
        return prefix + bs


def _getenv(key: str) -> str:
    """Like os.getenv() but throws if the environment variable is not set."""
    assert key

    value = os.getenv(key)
    if value is None:
        raise RuntimeError("environment variable %s not set" % key)
    return value


def _fix_hostname(hostname: str) -> str:
    """
    This function returns "localhost" if the hostname is the name of the
    machine running this code; otherwise it returns the hostname unchanged.

    Since 2005, Debian-based distributions make the hostname an alias to
    127.0.1.1 (take a look a `/etc/hosts`) and in some cases, this leads to a
    hanging connect(2) when server and client are on the same host (is this a
    bug?). The problem may occur with SCTP and TCP.

    Related:
    * https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=316099
    """
    return "localhost" if hostname == socket.gethostname() else hostname


def connect_to_launcher() -> socket.socket:
    launcher_host_raw = _getenv("MELISSA_LAUNCHER_HOST")
    launcher_host = _fix_hostname(launcher_host_raw)
    launcher_port_str = _getenv("MELISSA_LAUNCHER_PORT")
    launcher_port = int(launcher_port_str)
    protocol_str = _getenv("MELISSA_LAUNCHER_PROTOCOL")
    protocol = str2protocol(protocol_str)

    logger.debug(
        f"connecting to launcher host={launcher_host} port={launcher_port} protocol={protocol_str}"
    )

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, protocol)
    try:
        s.connect((launcher_host, launcher_port))
    except Exception as e:
        s.close()
        raise Exception(f'Exception caught during connection {e}')
    return s


def connect_to_server() -> Socket:
    server_host_raw = _getenv("MELISSA_SERVER_HOST")
    server_host = _fix_hostname(server_host_raw)
    server_port_str = _getenv("MELISSA_SERVER_PORT")
    server_port = int(server_port_str)

    logger.debug(
        f"connecting to server host={server_host} port={server_port}"
    )

    return socket.create_connection((server_host, server_port))


def get_available_protocols() -> List[int]:
    family = socket.AF_INET
    type = socket.SOCK_STREAM

    def is_supported(proto: int) -> bool:
        try:
            socket.socket(family, type, proto).close()
        except OSError as e:
            if e.errno != errno.EPROTONOSUPPORT:
                raise
            return False
        return True

    protocols = [socket.IPPROTO_SCTP, socket.IPPROTO_TCP]
    return [p for p in protocols if is_supported(p)]


def make_passive_socket(
    node: Optional[str] = None,
    *,
    protocol: int,
    backlog: Optional[int] = None
) -> Socket:
    # The IP compatibility layer on Infiniband is usually bound to an IPv4
    # address.
    family = socket.AF_INET
    type = socket.SOCK_STREAM
    sockfd = socket.socket(family, type, protocol)
    try:
        sockfd.bind(("" if node is None else node, 0))
        sockfd.listen() if backlog is None else sockfd.listen(backlog)

        listen_addr = sockfd.getsockname()
        logger.info(f"listening for connections on {listen_addr[0]}:{listen_addr[1]}")
    except Exception as e:
        sockfd.close()
        raise Exception(f'Exception caught listening for connection {e}')

    return sockfd


# use Socket everywhere in order to avoid having to bother about the
# distinction between Socket objects and plain integer file descriptors
def pipe() -> Tuple[Socket, Socket]:
    fd_r, fd_w = socketpair()
    fd_r.shutdown(socket.SHUT_WR)
    fd_w.shutdown(socket.SHUT_RD)
    return fd_r, fd_w


def protocol2str(proto: int) -> str:
    if proto == socket.IPPROTO_SCTP:
        return "SCTP"
    if proto == socket.IPPROTO_TCP:
        return "TCP"
    raise ValueError("unknown protocol {:d}".format(proto))


def socketpair() -> Tuple[Socket, Socket]:
    return socket.socketpair(socket.AF_UNIX, socket.SOCK_STREAM)


def str2protocol(name: str) -> int:
    if name == "SCTP":
        return socket.IPPROTO_SCTP
    if name == "TCP":
        return socket.IPPROTO_TCP
    raise ValueError("unknown protocol {:s}".format(name))


def get_rank_and_num_server_proc() -> Tuple[int, int]:

    try:
        rank = int(os.environ["SLURM_PROCID"])
        nb_proc_server = int(os.environ["SLURM_NTASKS"])
    except KeyError:
        from mpi4py import MPI
        rank = MPI.COMM_WORLD.Get_rank()
        nb_proc_server = MPI.COMM_WORLD.Get_size()

    return rank, nb_proc_server
