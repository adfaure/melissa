
import logging
import os
from melissa.utility import networking
import socket
from melissa.scheduler.slurm import SlurmScheduler
from melissa.scheduler.slurm_global import SlurmGlobalScheduler
from melissa.scheduler.slurm_semiglobal import SlurmSemiGlobalScheduler
from melissa.scheduler.oar import OarScheduler
from melissa.scheduler.oar_hybrid import OarHybridScheduler
from melissa.scheduler.openmpi import OpenMpiScheduler
from typing import Tuple
import shutil

logger = logging.getLogger(__name__)


def homogenize_arguments(config_path: str) -> Tuple[str, str]:

    head_tail = os.path.split(config_path)
    conf_name = head_tail[1]
    if config_path is not None and config_path.startswith("/"):
        project_dir = head_tail[0]
        logger.info(f"Using absolute path for project_dir {project_dir}")
    else:
        project_dir = f"{os.getcwd()}/{head_tail[0]}"
        logger.info(f"Found relative path, new project_dir {project_dir}")

    if ".json" in conf_name:
        conf_name = conf_name.split(".")[0]

    return conf_name, project_dir


def check_protocol_choice(lconfig: dict):
    # check protocol choice
    protocols = networking.get_available_protocols()
    if protocols == []:
        return "no supported communication protocols available"
    if lconfig['protocol'] == "sctp":
        if socket.IPPROTO_SCTP not in protocols:
            return "SCTP unavailable"
        protocol = socket.IPPROTO_SCTP
    elif lconfig['protocol'] == "tcp":
        if socket.IPPROTO_TCP not in protocols:
            return "TCP unavailable"
        protocol = socket.IPPROTO_TCP
    elif lconfig['protocol'] == "auto":
        protocol = protocols[0]
    del protocols

    logger.info(
        f"using protocol {networking.protocol2str(protocol)} "
        "for server/launcher communication"
    )

    return protocol


def setup_scheduler(lconfig: dict):
    # FIXME: the architecture here is quite difficult to debug for mypy (see type: ignore).
    if lconfig['scheduler'] == "oar":
        scheduler_impl = OarScheduler
        sched_client_cmd = "mpirun"
        sched_server_cmd = "mpirun"
    elif lconfig['scheduler'] == "oar-hybrid":
        scheduler_impl = OarHybridScheduler  # type: ignore
        sched_client_cmd = "mpirun"
        sched_server_cmd = "mpirun"
    elif lconfig['scheduler'] == "openmpi":
        scheduler_impl = OpenMpiScheduler  # type: ignore
        sched_client_cmd = "mpirun"
        sched_server_cmd = "mpirun"
    elif lconfig['scheduler'] == "slurm":
        scheduler_impl = SlurmScheduler  # type: ignore
        sched_client_cmd = "srun"
        sched_server_cmd = "srun"
    elif lconfig['scheduler'] == "slurm-global":
        scheduler_impl = SlurmGlobalScheduler  # type: ignore
        sched_client_cmd = "srun"
        sched_server_cmd = "srun"
    elif lconfig['scheduler'] == "slurm-semiglobal":
        scheduler_impl = SlurmSemiGlobalScheduler  # type: ignore
        sched_client_cmd = "srun"
        sched_server_cmd = "srun"
    else:
        raise RuntimeError("BUG unknown scheduler {:s}".format(lconfig['scheduler']))

    # set up the scheduler command (e.g. mpirun, srun)
    sched_client_cmd = lconfig.get("scheduler_client_command", sched_client_cmd)
    sched_server_cmd = lconfig.get("scheduler_server_command", sched_server_cmd)

    return scheduler_impl, sched_client_cmd, sched_server_cmd


def locate_executable(path_raw: str) -> str:
    maybe_path = shutil.which(path_raw)
    if maybe_path is None:
        raise RuntimeError(f"executable {path_raw} not found")
    return os.path.abspath(maybe_path)
