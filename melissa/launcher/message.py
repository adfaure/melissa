#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__all__ = [
    "BYTES_PER_INT", "Type", "Message", "InvalidMessage", "Ping", "GroupSize",
    "JobSubmission", "JobUpdate", "JobCancellation", "Exit", "deserialize"
]

import enum

from melissa.utility.message import InvalidMessage, Message, bytes2int, int2bytes
from melissa.scheduler.job import State

BYTES_PER_INT = 4


def _int2bytes(x: int) -> bytes:
    return int2bytes(x, num_bytes=BYTES_PER_INT)


class Type(enum.IntEnum):
    PING = 0
    JOB_SUBMISSION = 1
    JOB_UPDATE = 2
    JOB_CANCELLATION = 3
    GROUP_SIZE = 4
    NO_MONITORING = 5
    EXIT = 255

    def __str__(self) -> str:
        return self.name


class Ping(Message):
    def _serialize(self) -> bytes:
        return _int2bytes(Type.PING)

    def __eq__(self, other: object) -> bool:
        return isinstance(other, Ping)


class Exit(Message):
    def __init__(self, status: int) -> None:
        assert status >= 0
        assert status <= 127
        self.status = status

    def _serialize(self) -> bytes:
        return _int2bytes(Type.EXIT) + _int2bytes(self.status)

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Exit):
            return self.status == other.status
        return NotImplemented


class JobCancellation(Message):
    def __init__(self, job_id: int) -> None:
        self.job_id = job_id

    def _serialize(self) -> bytes:
        return _int2bytes(Type.JOB_CANCELLATION) + _int2bytes(self.job_id)

    def __eq__(self, other: object) -> bool:
        if isinstance(other, JobCancellation):
            return self.job_id == other.job_id
        return NotImplemented


class JobSubmission(Message):
    def __init__(self, initial_id: int, num_jobs: int) -> None:
        assert num_jobs > 0
        self.initial_id = initial_id
        self.num_jobs = num_jobs

    def _serialize(self) -> bytes:
        return _int2bytes(Type.JOB_SUBMISSION) \
            + _int2bytes(self.initial_id) \
            + _int2bytes(self.num_jobs)

    def __eq__(self, other: object) -> bool:
        if isinstance(other, JobSubmission):
            return self.initial_id == other.initial_id \
                and self.num_jobs == other.num_jobs
        return NotImplemented


class JobUpdate(Message):
    def __init__(self, job_id: int, job_state: State) -> None:
        self.job_id = job_id
        self.job_state = job_state

    def _serialize(self) -> bytes:
        return _int2bytes(Type.JOB_UPDATE) \
            + _int2bytes(self.job_id) \
            + _int2bytes(self.job_state)

    def __eq__(self, other: object) -> bool:
        if isinstance(other, JobUpdate):
            return self.job_id == other.job_id \
                and self.job_state == other.job_state
        return NotImplemented


class GroupSize(Message):
    def __init__(self, group_size: int) -> None:
        self.group_size = group_size

    def _serialize(self) -> bytes:
        return _int2bytes(Type.GROUP_SIZE) \
            + _int2bytes(self.group_size)

    def __eq__(self, other: object) -> bool:
        if isinstance(other, GroupSize):
            return self.group_size == other.group_size
        return NotImplemented


class StopTimeoutMonitoring(Message):
    def _serialize(self) -> bytes:
        return _int2bytes(Type.NO_MONITORING)

    def __eq__(self, other: object) -> bool:
        return isinstance(other, StopTimeoutMonitoring)


def deserialize(bs: bytes) -> Message:
    if len(bs) < BYTES_PER_INT:
        return InvalidMessage(
            bs, "expected a packet of size at least {:d} bytes, got {:d} bytes".
            format(BYTES_PER_INT, len(bs))
        )

    if len(bs) % BYTES_PER_INT != 0:
        return InvalidMessage(
            bs,
            "expected integers but packet size {:d} bytes is not divisible by {:d}"
            .format(len(bs), BYTES_PER_INT)
        )

    def b2i(index: int) -> int:
        first = index * BYTES_PER_INT
        last = (index + 1) * BYTES_PER_INT
        return bytes2int(bs[first:last])

    msg_type = b2i(0)
    # yapf: disable
    expected_num_bytes = (
        1 * BYTES_PER_INT if msg_type == Type.PING else
        3 * BYTES_PER_INT if msg_type == Type.JOB_SUBMISSION else
        3 * BYTES_PER_INT if msg_type == Type.JOB_UPDATE else
        2 * BYTES_PER_INT if msg_type == Type.JOB_CANCELLATION else
        2 * BYTES_PER_INT if msg_type == Type.GROUP_SIZE else
        1 * BYTES_PER_INT if msg_type == Type.NO_MONITORING else None
    )
    # yapf: enable

    if expected_num_bytes is not None and len(bs) != expected_num_bytes:
        return InvalidMessage(
            bs, "expected {:s} message of length {:d} bytes, got {:d}".format(
                str(Type(msg_type)), expected_num_bytes, len(bs)
            )
        )

    if msg_type == Type.PING:
        return Ping()

    if msg_type == Type.JOB_SUBMISSION:
        initial_job_id = b2i(1)
        num_jobs = b2i(2)
        return JobSubmission(initial_job_id, num_jobs)

    if msg_type == Type.JOB_UPDATE:
        job_id = b2i(1)
        job_state = b2i(2)
        return JobUpdate(job_id, State(job_state))

    if msg_type == Type.JOB_CANCELLATION:
        job_id = b2i(1)
        return JobCancellation(job_id)

    if msg_type == Type.GROUP_SIZE:
        group_size = b2i(1)
        return GroupSize(group_size)

    if msg_type == Type.NO_MONITORING:
        return StopTimeoutMonitoring()

    if msg_type == Type.EXIT:
        status = b2i(1)
        return Exit(status)

    return InvalidMessage(bs, "unknown message type {:d}".format(msg_type))
