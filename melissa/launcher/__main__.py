#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import argparse
import contextlib
import logging
import os
import stat
import shutil
import signal
import socket
import sys
import threading
from types import FrameType
from typing import Optional, Union, Dict, Any

from melissa.utility import networking, time, timer
from melissa.scheduler.scheduler import Options as SchedulerOptions

from datetime import datetime
# from jsonschema import Draft4Validator, validators
# from jsonschema.exceptions import ValidationError
from pathlib import Path
import rapidjson
from melissa.utility.logger import configure_logger, get_log_level_from_verbosity
from melissa.launcher.parser import (homogenize_arguments,
                                     check_protocol_choice,
                                     setup_scheduler,
                                     locate_executable)

from . import event, state_machine, monitoring, processor
from . import __version__ as launcher_version
from .io import IoMaster
from .queue import Queue
from .monitoring import RestHttpServer
from melissa.launcher.schema import validate_config, CONFIG_PARSE_MODE, print_options

logger = logging.getLogger(__name__)


def main() -> Union[int, str]:
    parser = argparse.ArgumentParser(
        prog="melissa-launcher", description="Melissa Launcher"
    )

    # CLI flags

    parser.add_argument(
        "--config_name",
        "-c",
        help="User defined configuration file. Path can be "
        "relative or absolute.",
        type=str
    )

    parser.add_argument(
        "--version",
        "-v",
        action="version",
        help="show the Melissa version",
        version="%(prog)s {:s}".format(launcher_version)
    )

    parser.add_argument(
        "--print-options",
        help="Show the available configuration options",
        action="store_true",
        default=None
    )

    print_head()

    args = parser.parse_args()

    if args.print_options:
        print_options()

    conf_name, project_dir = homogenize_arguments(args.config_name)

    with open(Path(project_dir) / f"{conf_name}.json") as json_file:
        config_dict = rapidjson.load(json_file, parse_mode=CONFIG_PARSE_MODE)

    args, config_dict = validate_config(args, config_dict)
    lconfig = config_dict['launcher_config']

    # make the output directory path
    output_dir = make_and_configure_output_dir(config_dict, project_dir, conf_name)

    log_level = get_log_level_from_verbosity(lconfig.get("verbosity", 3))
    configure_logger(str(output_dir / "melissa_launcher.log"), log_level)

    logger.info(f"melissa-launcher PID={os.getpid()}")

    try:
        server_executable = locate_executable(output_dir / lconfig['server_executable'])
    except Exception as e:
        return str(e)

    logger.debug(f"server executable {server_executable}")

    protocol = check_protocol_choice(lconfig)

    scheduler_impl, sched_client_cmd, sched_server_cmd = setup_scheduler(lconfig)

    # set up options to pass directly to the scheduler command
    sched_client_cmd_opt = lconfig["scheduler_client_command_options"]
    sched_server_cmd_opt = lconfig["scheduler_server_command_options"]

    scheduler_is_available, info = scheduler_impl.is_available()
    if scheduler_is_available:
        logger.info(f"found {info[0]} (version {info[1]}")
        if lconfig['scheduler'] == "oar":
            scheduler = scheduler_impl("openmpi")
        elif lconfig['scheduler'] == "oar-hybrid":
            container_options = lconfig['scheduler_arg_container']
            container_client_size = lconfig['container_client_size']
            besteffort_alloc_freq = lconfig['besteffort_allocation_frequency']
            scheduler = scheduler_impl(
                "openmpi",
                container_options,
                container_client_size,
                besteffort_alloc_freq
            )  # type: ignore
        else:
            scheduler = scheduler_impl()  # type: ignore
    else:
        logger.error(f"scheduler {lconfig['scheduler']} unusable: {info}")
        return 1

    del scheduler_impl

    client_options = SchedulerOptions(
        sched_client_cmd,
        sched_client_cmd_opt,
        lconfig['scheduler_arg'] + lconfig['scheduler_arg_client']
    )
    server_options = SchedulerOptions(
        sched_server_cmd,
        sched_server_cmd_opt,
        lconfig['scheduler_arg'] + lconfig['scheduler_arg_server']
    )

    # set up std output files option
    std_output = lconfig["std_output"]
    logger.info(f"std outputs {std_output}")

    # set up launcher scheduling constraints
    job_limit = lconfig["job_limit"]
    timer_delay = lconfig.get("timer_delay", 5)
    job_update_interval = lconfig.get("timer_delay", 30)

    # CHANGE DIRECTORY
    # KEEP THIS IN MIND WHEN HANDLING RELATIVE USER-PROVIDED PATHS.
    os.chdir(output_dir)

    # set up sockets
    signalfd_r, signalfd_w = networking.pipe()
    timerfd_0, timerfd_1 = networking.socketpair()
    eventfd_r, eventfd_w = networking.pipe()
    listenfd = networking.make_passive_socket(node=lconfig['bind'], protocol=protocol)

    # do not limit the queue size because the thread reading from the queue
    # will also put items into it
    event_fifo = Queue(eventfd_w)
    event_fifo.put(event.Timeout())

    def handle_signal(signo: int, _: Optional[FrameType]) -> None:
        signame = signal.Signals(signo).name
        logger.info(f"received signal {signo} {signame}")
        bs = signo.to_bytes(1, byteorder=sys.byteorder)
        signalfd_w.send(bs)

    with contextlib.ExitStack() as cm:
        # reset the signal handlers before closing the file descriptors
        signalfd_r = cm.enter_context(signalfd_r)
        signalfd_w = cm.enter_context(signalfd_w)
        timerfd_0 = cm.enter_context(timerfd_0)
        # timerfd_1 is closed manually after Timer thread stopped
        eventfd_r = cm.enter_context(eventfd_r)
        eventfd_w = cm.enter_context(eventfd_w)
        listenfd = cm.enter_context(listenfd)

        signals = [signal.SIGINT, signal.SIGPIPE, signal.SIGTERM]
        for s in signals:
            signal.signal(s, handle_signal)
            # reset the signal handlers before closing the signal file
            # descriptors
            cm.callback(signal.signal, s, signal.SIG_DFL)

        # set up state machine
        _, port = listenfd.getsockname()
        logger.info(f"fault-tolerance {bool(lconfig['fault_tolerance'])}")
        sm_config = state_machine.Configuration(
            cwd=os.getcwd(),
            launcher_host=socket.gethostname(),
            launcher_port=port,
            launcher_protocol=networking.protocol2str(protocol),
            server_executable=server_executable,
            no_fault_tolerance=not lconfig['fault_tolerance'],
            server_ping_interval=lconfig.get('server_timeout', 60.) / 2.,
            job_update_interval=job_update_interval,
        )
        initial_state = state_machine.State()

        # set up web server, processor
        http_server_address = (
            "" if lconfig['bind'] is None else lconfig['bind'], lconfig['http_port']
        )
        web_token = monitoring.generate_token(
        ) if not lconfig['http_token'] else lconfig['http_token']
        logger.info(f"generated web token {web_token}")

        print("Access the terminal-based Melissa monitor by opening a new "
              "terminal and executing:\n\n"
              "melissa-monitor "
              f"--http_bind={lconfig['bind']} "
              f"--http_port={lconfig['http_port']} "
              f"--http_token={web_token} "
              f"--output_dir={output_dir} \n"
              )

        print(f"All output for current run will be written to {output_dir}\n")
        if config_dict.get("vscode_debugging", False):
            print("**`vscode_debugger` active in user config**\n"
                  "Waiting for debugger attach, please start the "
                  "debugger by navigating to debugger pane, "
                  "ctrl+shift+d, and selecting:\n"
                  "Python: Remote Attach")

        web_server = RestHttpServer(
            http_server_address, web_token, initial_state
        )
        event_processor = processor.DefaultProcessor(
            sm_config, initial_state, web_server
        )
        webfd = socket.fromfd(
            web_server.fileno(), socket.AF_INET, socket.SOCK_STREAM
        )

        _, web_port = webfd.getsockname()
        logger.info(f"webserver listening on port {web_port}")

        timer_thread = timer.Timer(timerfd_1, interval=time.Time(seconds=timer_delay))
        t_timer = threading.Thread(
            target=lambda: timer_thread.run(), daemon=True
        )
        io_master = IoMaster(
            listenfd, signalfd_r, timerfd_0, webfd, eventfd_r, event_fifo,
            scheduler, client_options, server_options, event_processor, protocol,
            job_limit=job_limit, std_output=std_output
        )
        t_timer.start()
        status = io_master.run()

    t_timer.join(timeout=1)
    if t_timer.is_alive():
        logger.warning("timer thread did not terminate")
    else:
        timerfd_1.close()

    return status


def make_and_configure_output_dir(config: Dict[str, Any],
                                  project_dir: str, conf_name: str) -> Path:
    """
    Create the user defined output directory, copy the client/server templates to the directory,
    then
    modify the scripts to point to the proper executables.
    """

    default_out_dir = datetime.now().strftime('melissa-%Y%m%dT%H%M%S')
    output_dir = config.get('output_dir', default_out_dir)
    logger.info(f'Writing output to {str(output_dir)}')

    # allow relative or absolute path passing for output dir
    proj_path = Path(project_dir)
    if output_dir.startswith('/'):
        out_path = Path(output_dir)
    else:
        out_path = Path(proj_path) / output_dir

    out_path.mkdir(parents=True, exist_ok=True)
    melissa_src_path = Path(__file__).parent.parent.parent.resolve()
    melissa_server_bin_path = config['server_config'].get(
        'melissa_server_env',
        Path(__file__).parent.parent.parent.resolve()
    )
    melissa_client_bin_path = config['client_config'].get(
        'melissa_client_env',
        Path(__file__).parent.parent.parent.resolve()
    )
    bash_script_path = melissa_src_path / 'melissa/utility/bash_scripts'

    # create output dir structure
    Path(out_path / 'client_scripts').mkdir(parents=True, exist_ok=True)
    Path(out_path / 'stdout').mkdir(parents=True, exist_ok=True)

    # copy all the launch files to the output dir
    shutil.copy(proj_path / f"{conf_name}.json", out_path / f"{conf_name}.json")
    shutil.copy(bash_script_path / "server.sh", out_path / "server.sh")
    os.chmod(out_path / "server.sh", 0o765)

    shutil.copy(bash_script_path / "client.sh", out_path / "client.sh")
    os.chmod(out_path / "client.sh", 0o765)

    with open(out_path / "client.sh", "r") as f:
        data = f.read()
        data = data.replace(
            '{melissa_set_env_file}', str(melissa_client_bin_path) + '/melissa_set_env.sh'
        )
        data = data.replace('{executable_command}', config['client_config']['executable_command'])

    with open(out_path / "client.sh", "w") as f:
        f.write(data)

    preproc_instr = config['client_config'].get("preprocessing_commands")
    set_user_defined_preprocessing_instructions(preproc_instr, out_path / "client.sh")

    with open(out_path / "server.sh", "r") as f:
        data = f.read()
        data = data.replace(
            '{melissa_set_env_file}', str(melissa_server_bin_path) + '/melissa_set_env.sh'
        )
        data = data.replace('{path_to_usecase}', project_dir)
        data = data.replace('{config_name}', conf_name)

    with open(out_path / "server.sh", "w") as f:
        f.write(data)

    preproc_instr = config['server_config'].get("preprocessing_commands")
    set_user_defined_preprocessing_instructions(preproc_instr, out_path / "server.sh")

    return out_path


def set_user_defined_preprocessing_instructions(instructions: list, file_path: Path):
    lines = open(file_path, 'r').readlines()
    out = open(file_path, 'w')
    lines[4] = "\n".join(instructions) + "\n"
    out.writelines(lines)
    out.close()


def print_head():
    print("")
    print("$! " + "-" * 46 + " $!")
    print(
        "  __  __ ______ _      _____  _____ _____           "  # noqa: E501,W605
    )
    print(
        " |  \/  |  ____| |    |_   _|/ ____/ ____|  /\      "  # noqa: E501,W605
    )
    print(
        " | \  / | |__  | |      | | | (___| (___   /  \     "  # noqa: E501,W605
    )
    print(
        " | |\/| |  __| | |      | |  \___ \\\\___ \ / /\ \  "  # noqa: E501,W605
    )
    print(
        " | |  | | |____| |____ _| |_ ____) |___) / ____ \   "  # noqa: E501,W605
    )
    print(
        " |_|  |_|______|______|_____|_____/_____/_/    \_\  "  # noqa: E501,W605
    )
    print("")
    print("$! " + "-" * 46 + " $!")
    print("")


if __name__ == "__main__":
    sys.exit(main())
