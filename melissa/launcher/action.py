#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from typing import Generic, Dict, List, Optional, TypeVar

from melissa.utility.networking import ConnectionId

from melissa.utility.message import Message

JobT = TypeVar("JobT")


class Action:
    pass


class ConnectionClosure(Action):
    def __init__(self, cid: ConnectionId) -> None:
        self.cid = cid


class Exit(Action):
    def __init__(self, status: int) -> None:
        assert status >= 0
        self.status = status


class JobCancellation(Generic[JobT], Action):
    def __init__(self, jobs: List[JobT]) -> None:
        self.jobs = jobs


class JobSubmission(Action):
    def __init__(
        self,
        working_directory: str,
        commands: List[List[str]],
        environment: Dict[str, Optional[str]],
        job_name: str,
    ) -> None:
        self.working_directory = working_directory
        self.commands = commands
        self.environment = environment
        self.job_name = job_name
        self.client_id: int = 0


class ServerJobSubmission(JobSubmission):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)


class ClientJobSubmission(JobSubmission):
    def __init__(self, client_id: int, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.client_id = client_id


class JobUpdate(Generic[JobT], Action):
    def __init__(self, jobs: List[JobT]) -> None:
        self.jobs = jobs


class MessageSending(Action):
    def __init__(self, cid: Optional[ConnectionId], message: Message) -> None:
        self.cid = cid
        self.message = message


class PostponeSubmission(Action):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
