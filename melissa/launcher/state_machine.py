#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
A state machine for a job launcher.

The state machine is built on the following assumptions:
* The maximum(!) time between two server/launcher pings is the same in both
  directions.
* A server disconnect is always interpreted as a server termination. Whether
  the termination is considered to be an error depends on the exit status of
  the server.
* After a successful job cancellation (e.g., `scancel` exits with status zero)
  the job is assumed to have terminated. In practice, this is not true when
  using a batch scheduler. If this is a problem, then the state machine and/or
  the server must be adapted.

This paragraph describes the state machine from a high level. In the
SERVER_DEAD phase, all existing jobs are cancelled and all job updates are
waited for. Once this is done, either the state machine stops or a new server
job is submitted with the state machine transitioning to the SERVER_READY
phase. Once the job submission succeeds, the state machine will begin to
execute job updates. The state machine transition into the SERVER_RUNNING phase
if the server connects to the launcher or if the batch scheduler marks the
server job as running. In the SERVER_RUNNING phase, the server is expected to
ping the launcher regularly. Keep in mind that the launcher considers a job
terminated as soon as the job cancellation was successful (e.g., `scancel` or
`oarcancel`).

The launcher transitions to the SERVER_DEAD phase again if
* a job update marks the server job as dead,
* the server closes the connection to the launcher, or
* SIGTERM or SIGINT is received.

The launcher stops if
* the server job exited successfully, or
* SIGTERM or SIGINT were received.
"""

from copy import copy
import enum
import logging
import signal
from typing import Iterable, List, Optional, Tuple, Union

from melissa.utility.networking import ConnectionId
from melissa.utility.functools import partition
from melissa.utility.identifier import Id
from melissa.scheduler.job import Job
from melissa.scheduler.job import State as JobState
from melissa.utility.time import Time

from . import action, event, message

logger = logging.getLogger(__name__)


class Configuration:
    def __init__(
        self,
        cwd: str,
        launcher_host: str,
        launcher_port: int,
        launcher_protocol: str,
        server_executable: str,
        no_fault_tolerance: bool = False,
        server_ping_interval: float = 30,
        job_update_interval: float = 30,
    ) -> None:
        assert launcher_port > 0
        assert launcher_port <= 65535

        self.working_directory = cwd
        self.launcher_host = launcher_host
        self.launcher_port = launcher_port
        self.launcher_protocol = launcher_protocol
        self.server_executable = server_executable
        self.no_fault_tolerance = no_fault_tolerance
        self.server_ping_interval = Time(seconds=server_ping_interval)
        self.job_update_interval = Time(seconds=job_update_interval)
        self.group_size = 1

        # time-out monitor switch
        self.time_out_monitor: bool = True
        assert self.server_ping_interval >= self.job_update_interval


@enum.unique
class Phase(enum.Enum):
    SERVER_DEAD = 1
    SERVER_READY = 2
    SERVER_RUNNING = 3
    STOP = 4


class State:
    def __init__(self) -> None:
        self.phase = Phase.SERVER_DEAD
        self.job_submissions: List[Union[action.JobSubmission, action.ClientJobSubmission]] = []
        self.jobs = []  # type: List[Job]
        self.jobs_to_cancel: List[Union[str, int]] = []
        self.jobs_cancelling: List[Union[str, int]] = []
        self.server_job_uid: Union[str, int, None] = None
        self.server_cid = None  # type: Optional[ConnectionId]
        self.cid_to_uid_map = []  # type: List[Tuple[int, Id]]
        self.last_server_message = Time()
        self.last_server_ping = Time()
        self.last_job_update = Time()
        self.job_update_in_progress = False
        self.stop = False
        self.exit_status = 0


def _uid_to_cid(s: State, the_uid: Union[str, int]) -> int:
    cids = [cid for (cid, uid) in s.cid_to_uid_map if uid == the_uid]
    if len(cids) > 1:
        raise RuntimeError(
            f"UID {the_uid} is associated with {len(cids)} CIDs")

    if cids:
        return cids[0]
    raise RuntimeError(f"unknown UID {the_uid}")


def _cid_to_uid(s: State, the_cid: int) -> Optional[int]:
    uids = [uid for (cid, uid) in s.cid_to_uid_map if cid == the_cid]
    if len(uids) > 1:
        raise RuntimeError(
            "CID %d is associated with %d UIDs" % (the_cid, len(uids))
        )

    return uids[0] if uids else None


def _kill_server(cfg: Configuration, s0: State, reason: str,
                 *args: object) -> Tuple[State, List[action.Action]]:
    logger.error(reason, *args)
    s1 = copy(s0)
    s1.phase = Phase.SERVER_DEAD
    s1.stop = True
    return _transition_common(cfg, s0)


def _make_client_job_submission(
    cfg: Configuration, client_id: int
) -> Union[action.ClientJobSubmission, action.JobSubmission]:
    commands = [[f"./client_scripts/client.{client_id*cfg.group_size}.sh"]]
    if cfg.group_size > 1:
        # the server submits one request per group
        # the client_id corresponds to the group_id
        for i in range(1, cfg.group_size):
            commands.append([f"./client_scripts/client.{cfg.group_size*client_id + i}.sh"])
    return action.ClientJobSubmission(
        working_directory=cfg.working_directory,
        commands=commands,
        environment={
            "MELISSA_JOB_ID": str(client_id),
        },
        job_name="melissa-client",
        client_id=client_id
    )


def _make_server_job_submission(
    cfg: Configuration
) -> action.ServerJobSubmission:
    return action.ServerJobSubmission(
        working_directory=cfg.working_directory,
        commands=[[cfg.server_executable]],
        environment={
            "MELISSA_LAUNCHER_HOST": cfg.launcher_host,
            "MELISSA_LAUNCHER_PORT": str(cfg.launcher_port),
            "MELISSA_LAUNCHER_PROTOCOL": cfg.launcher_protocol,
            "MELISSA_FAULT_TOLERANCE": "OFF" if cfg.no_fault_tolerance else "ON",
        },
        job_name="melissa-server",
    )


def _submit_client_jobs(
    cfg: Configuration, s0: State, initial_id: int, num_jobs: int
) -> Tuple[State, List[action.Action]]:
    assert s0.phase == Phase.SERVER_RUNNING
    assert s0.jobs != []
    assert initial_id >= 0
    assert num_jobs > 0

    client_ids = [initial_id + i for i in range(num_jobs)]
    submissions = [_make_client_job_submission(cfg, cid) for cid in client_ids]

    s1 = copy(s0)
    s1.job_submissions = s0.job_submissions + submissions

    # copy for silencing mypy
    return s1, [s for s in submissions]


def _send_update(s: State, j: Job) -> action.MessageSending:
    assert s.phase == Phase.SERVER_RUNNING

    cid = _uid_to_cid(s, j.unique_id())
    update = message.JobUpdate(cid, j.state())
    return action.MessageSending(s.server_cid, update)


def _submit_server_job(cfg: Configuration,
                       s0: State) -> Tuple[State, List[action.Action]]:
    assert s0.phase == Phase.SERVER_DEAD
    assert s0.job_submissions == []
    assert s0.jobs == []
    assert s0.jobs_cancelling == []

    s1 = copy(s0)
    s1.phase = Phase.SERVER_READY
    sub = _make_server_job_submission(cfg)
    s1.job_submissions = [sub]

    return s1, [sub]


def _transition_common(cfg: Configuration,
                       s0: State) -> Tuple[State, List[action.Action]]:
    assert s0.phase != Phase.STOP
    assert not s0.jobs or len(s0.cid_to_uid_map) + 1 == len(s0.jobs)

    if s0.phase == Phase.SERVER_DEAD:
        if s0.jobs and not s0.job_submissions and not s0.jobs_cancelling:
            s1 = copy(s0)
            s1.jobs_cancelling = [j.unique_id() for j in s0.jobs]
            s1.jobs_to_cancel = []
            return s1, [action.JobCancellation(s0.jobs)]

        if not s0.jobs and \
                not s0.job_submissions and \
                not s0.jobs_cancelling and \
                not s0.job_update_in_progress:
            assert not s0.cid_to_uid_map
            if s0.stop:
                s1 = copy(s0)
                s1.phase = Phase.STOP
                return s1, [action.Exit(s0.exit_status)]

            return _submit_server_job(cfg, s0)

    return s0, []


# required to recognize obsolete job updates
assert JobState.WAITING.value < JobState.RUNNING.value
assert JobState.RUNNING.value < JobState.TERMINATED.value
assert JobState.RUNNING.value < JobState.ERROR.value
assert JobState.RUNNING.value < JobState.FAILED.value


def _transition_job_update(
    cfg: Configuration, s0: State, now: Time, ev: event.JobUpdate[Job]
) -> Tuple[State, List[action.Action]]:
    assert s0.phase != Phase.STOP
    assert s0.job_update_in_progress

    def try_find(j: Job, jobs: List[Job]) -> Optional[Job]:
        for k in jobs:
            if j == k:
                return k
        return None

    def update(j: Job) -> Job:
        k = try_find(j, ev.jobs)

        if k is None:
            return j

        # depending on the order of the job update and server message
        # reception, the job update may actually indicate an outdated state.
        # The check below tries to catch such a job state transition.
        if j.state().value >= k.state().value:
            return j

        return k

    job_error_states = [JobState.ERROR, JobState.FAILED]
    # I considered naming this variable "stop states" but this is misleading
    # because jobs can actually be stopped when running jobs locally and when
    # using a batch scheduler.
    job_dead_states = job_error_states + [JobState.TERMINATED]

    def is_alive(j: Job) -> bool:
        return j.state() not in job_dead_states

    updated_jobs = [update(j) for j in s0.jobs]
    live_jobs, dead_jobs = partition(is_alive, updated_jobs)

    # warn about failed client jobs
    for j in dead_jobs:
        if j.state() in job_error_states:
            logger.warning(
                f"job failure id={j.id()} uid={j.unique_id()} state={j.state()}"
            )

    s1 = copy(s0)
    s1.jobs = live_jobs
    live_uids = [j.unique_id() for j in live_jobs]
    s1.cid_to_uid_map = [
        (cid, uid) for (cid, uid) in s0.cid_to_uid_map if uid in live_uids
    ]
    s1.last_job_update = now
    s1.job_update_in_progress = False

    # diagnose server job
    def find_server_job(jobs: Iterable[Job]) -> Job:
        if s0.server_job_uid is None:
            raise RuntimeError("BUG server job unique ID is None")

        for j in jobs:
            if j.unique_id() == s0.server_job_uid:
                return j
        raise RuntimeError("BUG there should be a server job")

    if s0.server_job_uid is not None:
        assert s0.phase != Phase.STOP

        new_server_job = find_server_job(updated_jobs)

        # in case the server terminated successfully
        if new_server_job.state() == JobState.TERMINATED:
            s1.stop = True

        if new_server_job.state() == JobState.FAILED and cfg.no_fault_tolerance:
            s1.exit_status = 1
            s1.stop = True

        if new_server_job.state() in job_dead_states:
            # if the server job terminates, the connection will be closed. it
            # is very unlikely that the launcher would detect the job
            # termination before the closure of the network connection. thus,
            # we do not handle this case here.
            assert s0.server_cid is None

            s1.phase = Phase.SERVER_DEAD
            s1.server_job_uid = None
        # this branch will be taken, e.g., if the user pressed Ctrl+C after a
        # server job was successfully submitted
        elif new_server_job.state() == JobState.RUNNING and \
                s0.phase == Phase.SERVER_READY:
            s1 = _mark_server_as_running(s1, now)

    # send updates to server
    if s1.phase == Phase.SERVER_RUNNING:
        updated_client_jobs = [
            j for j in updated_jobs if j != s0.server_job_uid and j in ev.jobs
        ]
        return s1, [_send_update(s0, j) for j in updated_client_jobs]

    return _transition_common(cfg, s1)


def _transition_message_reception(
    cfg: Configuration, s0: State, now: Time, ev: event.MessageReception
) -> Tuple[State, List[action.Action]]:
    msg = ev.message
    s1 = copy(s0)
    s1.last_server_message = now

    if isinstance(msg, message.Exit):
        logger.info(f"server sent EXIT message (status {msg.status})")
        s1.phase = Phase.SERVER_DEAD
        s1.stop = True

        if msg.status >= 0 and msg.status <= 127:
            s1.exit_status = msg.status
        else:
            s1.exit_status = 1
            logger.warning(
                f"expected server EXIT status in closed interval 0 to 127, got {msg.status}"
            )

    elif isinstance(msg, message.InvalidMessage):
        logger.warning(
            f"received invalid message from server ({len(msg.raw_bytes)} bytes): {msg.reason}"
        )

    elif isinstance(msg, message.JobCancellation):
        maybe_uid = _cid_to_uid(s0, msg.job_id)
        if maybe_uid is None:
            return _kill_server(
                cfg, s1, "request to cancel unknown client ID %d", msg.job_id
            )
        s1.jobs_to_cancel.append(maybe_uid)

    elif isinstance(msg, message.JobSubmission):
        if msg.initial_id < 0:
            return _kill_server(
                cfg, s1, "expected a nonnegative client ID, server sent %d",
                msg.initial_id
            )
        if msg.num_jobs <= 0:
            return _kill_server(
                cfg, s1, "server requested to submit %d jobs", msg.num_jobs
            )
        if msg.num_jobs > 1000:
            return _kill_server(
                cfg, s1, "server wants to submit %d jobs", msg.num_jobs
            )

        return _submit_client_jobs(cfg, s1, msg.initial_id, msg.num_jobs)

    elif isinstance(msg, message.JobUpdate):
        return _kill_server(
            cfg, s1, "server unexpectedly sent a job update for CID %d",
            msg.job_id
        )

    elif isinstance(msg, message.Ping):
        logger.debug("received server ping")
        return s1, []

    elif isinstance(msg, message.GroupSize):
        logger.debug(f"received group size {msg.group_size}")
        cfg.group_size = msg.group_size

    elif isinstance(msg, message.StopTimeoutMonitoring):
        logger.debug("Server time-out are not monitored anymore")
        cfg.time_out_monitor = False

    else:
        raise RuntimeError("unknown message type {}".format(type(msg)))

    return _transition_common(cfg, s1)


def _transition_timeout(
    cfg: Configuration, s0: State, now: Time, _: event.Timeout
) -> Tuple[State, List[action.Action]]:
    if s0.jobs:
        s1 = copy(s0)
        actions_1 = []  # type: List[action.Action]

        if s0.last_job_update + cfg.job_update_interval < now \
                and not s0.job_update_in_progress:
            s1.job_update_in_progress = True
            actions_1.append(action.JobUpdate([copy(j) for j in s0.jobs]))

        if s0.phase == Phase.SERVER_RUNNING:
            if (
                s0.last_server_message + 2 * cfg.server_ping_interval < now
                and cfg.time_out_monitor
            ):
                logger.warning(f"server time-out uid={s0.server_job_uid}")
                s1.phase = Phase.SERVER_DEAD
                if cfg.no_fault_tolerance:
                    s1.stop = True
                    s1.exit_status = 1
                s2, actions_2 = _transition_common(cfg, s1)
                return s2, actions_1 + actions_2

            # do not assume the connection exists because the server is running
            # and not timed out. the batch scheduler can also mark the server
            # as running.
            if s0.last_server_ping + cfg.server_ping_interval < now \
                    and s0.server_cid is not None:
                s1.last_server_ping = now
                actions_1.append(
                    action.MessageSending(s0.server_cid, message.Ping())
                )

        if s0.jobs_to_cancel and not s0.jobs_cancelling:
            s1.jobs_cancelling = s0.jobs_to_cancel
            s1.jobs_to_cancel = []
            cancel_jobs = [
                j for j in s0.jobs if j.unique_id() in s0.jobs_to_cancel
            ]
            actions_1.append(action.JobCancellation(cancel_jobs))

        return s1, actions_1

    return _transition_common(cfg, s0)


def _mark_server_as_running(s0: State, now: Time) -> State:
    assert s0.phase in [Phase.SERVER_READY, Phase.SERVER_RUNNING]
    s1 = copy(s0)
    s1.phase = Phase.SERVER_RUNNING
    s1.last_server_message = now
    s1.last_server_ping = now
    return s1


def transition(cfg: Configuration, s0: State, now: Time,
               ev: event.Event) -> Tuple[State, List[action.Action]]:
    assert s0.phase != Phase.STOP
    assert not s0.jobs or len(s0.cid_to_uid_map) + 1 == len(s0.jobs)

    if isinstance(ev, event.ActionFailure):
        logger.warning(f"scheduling action {ev.action} error: {ev.error}")

        s1 = copy(s0)
        if isinstance(ev.action, action.JobCancellation):
            # remove jobs that terminated in the meantime
            live_jobs_cancelling = [
                j for j in s0.jobs_cancelling if j in s0.jobs
            ]
            s1.jobs_cancelling = []
            s1.jobs_to_cancel = s0.jobs_to_cancel + live_jobs_cancelling
        elif isinstance(ev.action, action.JobSubmission):
            s1.job_submissions = [
                s for s in s0.job_submissions if s != ev.action
            ]
            # did the server submission fail? terminate immediately
            if s0.phase == Phase.SERVER_READY:
                logger.error("the server job submission failed. exiting.")
                s1.phase = Phase.SERVER_DEAD
                s1.stop = True
            elif s0.phase == Phase.SERVER_RUNNING:
                assert s0.server_cid is not None
                return s1, [
                    action.MessageSending(
                        s0.server_cid,
                        message.JobUpdate(ev.action.client_id, JobState.ERROR)
                    )
                ]
        elif isinstance(ev.action, action.JobUpdate):
            s1.job_update_in_progress = False
        elif isinstance(ev.action, action.MessageSending):
            s1.phase = Phase.SERVER_DEAD
        else:
            raise NotImplementedError("BUG")
        return _transition_common(cfg, s1)

    if isinstance(ev, event.ConnectionShutdown):
        assert s0.server_cid is not None
        assert s0.server_cid == ev.cid
        s1 = copy(s0)
        s1.phase = Phase.SERVER_DEAD
        if cfg.no_fault_tolerance:
            s1.stop = True
        s1.server_cid = None
        logger.info("server closed connection")
        return _transition_common(cfg, s1)

    if isinstance(ev, event.JobCancellation):
        assert all([j in s0.jobs_cancelling for j in ev.jobs])
        cancelled_jobs = ev.jobs
        s1 = copy(s0)
        s1.jobs = [j for j in s0.jobs if j not in cancelled_jobs]
        s1.jobs_cancelling = []
        s1.cid_to_uid_map = [x for x in s0.cid_to_uid_map if x[1] in s1.jobs]
        if s0.server_job_uid is not None \
                and s0.server_job_uid in cancelled_jobs:
            s1.server_job_uid = None
        return _transition_common(cfg, s1)

    if isinstance(ev, event.JobSubmission):
        assert ev.submission in s0.job_submissions
        s1 = copy(s0)
        s1.job_submissions = [
            s for s in s0.job_submissions if s != ev.submission
        ]
        s1.jobs = s0.jobs + [ev.job]
        if isinstance(ev.submission, action.ServerJobSubmission) \
                and s0.phase == Phase.SERVER_READY:
            assert s0.server_job_uid is None
            assert len(s0.jobs) == 0
            assert len(s0.job_submissions) == 1
            s1.server_job_uid = ev.job.unique_id()
            s1.last_job_update = now
            if ev.job.state() == JobState.RUNNING:
                s2 = _mark_server_as_running(s1, now)
                return _transition_common(cfg, s2)
        elif isinstance(ev.submission, action.ClientJobSubmission):
            s1.cid_to_uid_map.append(
                (ev.submission.client_id, ev.job.unique_id())
            )
            if s1.phase == Phase.SERVER_RUNNING:
                return s1, [
                    action.MessageSending(
                        s0.server_cid,
                        message.JobUpdate(
                            ev.submission.client_id, ev.job.state()
                        )
                    )
                ]

        return _transition_common(cfg, s1)

    if isinstance(ev, event.JobUpdate):
        return _transition_job_update(cfg, s0, now, ev)

    if isinstance(ev, event.MessageReception):
        return _transition_message_reception(cfg, s0, now, ev)

    if isinstance(ev, event.NewConnection):
        if s0.phase in [Phase.SERVER_READY, Phase.SERVER_RUNNING] \
                and s0.server_cid is None:
            s1 = _mark_server_as_running(s0, now)
            s1.server_cid = ev.cid
            return _transition_common(cfg, s1)
        else:
            logger.warning("ignoring new connection to launcher")
            return s0, [action.ConnectionClosure(ev.cid)]

    if isinstance(ev, event.Signal):
        if ev.signo in [signal.SIGINT, signal.SIGTERM]:
            if s0.stop and not cfg.no_fault_tolerance:
                print("The launcher is")
                if s0.jobs:
                    print("* waiting for %d job(s)" % len(s0.jobs))
                if s0.job_submissions:
                    print(
                        "* terminating %d job submission(s)" %
                        len(s0.job_submissions)
                    )
                if s0.jobs_cancelling:
                    print(
                        "* waiting for the cancellation of %d job(s)" %
                        len(s0.jobs_cancelling)
                    )
                if s0.job_update_in_progress:
                    print("* one job update")
                return s0, []
            else:
                s1 = copy(s0)
                s1.phase = Phase.SERVER_DEAD
                s1.stop = True
                return _transition_common(cfg, s1)
        else:
            return s0, []

    if isinstance(ev, event.Timeout):
        return _transition_timeout(cfg, s0, now, ev)

    fmt = "unknown state transition (state {:s} event {:s})"
    raise RuntimeError(fmt.format(s0.phase, type(ev)))
