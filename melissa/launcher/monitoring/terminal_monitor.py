import requests
import argparse
import plotext as plt
from typing import Dict, Any
import time
import os
import sys
from subprocess import check_output
from pathlib import Path

"""
Helper script for tracking job status live in the terminal
Dependencies:
sudo apt-get install gnuplot
pip3 install termplotlib termgraph
"""


def update_job_dict(full_job_dict: Dict[int, Dict[str, Any]],
                    response: dict,
                    args: argparse.Namespace, header: dict) -> Dict[str, int]:
    """
    Scan REST API for jobs, aggregate state information
    """
    for job in response['jobs']:
        job_dict = requests.get(
            f'http://{args.http_bind}:{args.http_port}/jobs/{job}', headers=header).json()
        uid = job_dict['unique_id']

        full_job_dict[uid] = job_dict

    # aggregate the job statuses
    state_dict = {'RUNNING': 0, 'TERMINATED': 0, 'WAITING': 0, 'FAILED': 0}
    fjd = full_job_dict
    for uid in fjd.keys():
        state_dict[fjd[uid]['state']] += 1

    return state_dict


def plot_state_dict(state_dict: Dict[str, int]):
    """
    Plot state_dict to terminal
    """
    plt.clear_figure()
    os.system('clear')
    print('-------------------------`melissa-monitor`-------------------------\n')
    plt.simple_bar(state_dict.keys(), state_dict.values(), color="green")
    plt.show()


def print_tail(fname, lines=5):
    """"
    Log files can become large in melissa, so
    we take extra precaution to only ever load the
    tail of the log files to memory
    """
    with open(fname, "rb") as f:
        f.seek(0, 2)
        bytes_in_file = f.tell()
        lines_found, total_bytes_scanned = 0, 0
        while (lines + 1 > lines_found
                and bytes_in_file > total_bytes_scanned):
            byte_block = min(
                4096,
                bytes_in_file - total_bytes_scanned)
            f.seek(-(byte_block + total_bytes_scanned), 2)
            total_bytes_scanned += byte_block
            lines_found += f.read(4096).count(str.encode('\n'))
        f.seek(-total_bytes_scanned, 2)
        line_list = list(f.readlines())
        print_list = [li.decode("utf-8").strip() for li in line_list]
        print(*print_list[-lines:], sep='\n')


def get_server_launcher_logs(args: argparse.Namespace):
    server_log_path = Path(args.output_dir) / 'melissa_server_0.log'
    launcher_log_path = Path(args.output_dir) / 'melissa_launcher.log'
    print('\n-------------------------Server log tail-------------------------\n')
    print_tail(server_log_path, 6)
    print('\n------------------------Launcher log tail------------------------\n')
    print_tail(launcher_log_path, 6)


def get_eacct_ear_output(jobs: dict):
    job_ids = [str(job_id) for job_id in jobs.keys()]
    args = ["eacct", "-n", str(len(job_ids))]
    out = check_output(args).decode()

    total_energy = 0.
    total_time = 0.
    total_jobs = 0.
    lines = out.splitlines()[1:]
    for line in lines:
        if not line == "":
            data = line.split()
            job_id = data[0].split("-")[0]
            if int(job_id) in jobs:
                total_jobs += 1
                try:
                    total_energy += float(data[10])
                    total_time += float(data[6])
                except Exception as e:
                    print(f"exception encountered {e}.")
                    pass

    print('\n---------------------------EAR Metrics---------------------------\n')
    print(f"Total energy consumed: {total_energy:.2f} (J), "
          f"Average client time: {total_time/total_jobs:.2f} (s)")


def get_parsed_args() -> argparse.Namespace:
    """
    Parse CLI args and return them
    """
    parser = argparse.ArgumentParser(
        prog="melissa-monitor",
        description="A helper tool for monitoring melissa-launcher job status"
    )

    parser.add_argument(
        "--http_bind",
        help="Host address of the melissa-launcher http server "
             "defined in `launcher_config` as 'http_bind",
        default="frontend"
    )

    parser.add_argument(
        "--http_port",
        help="Port on host to access REST API "
             "defined in `launcher_config` as 'http_port",
        default="8888"
    )

    parser.add_argument(
        "--http_token",
        help="Token set in `launcher_config to access "
             "REST API",
        default="study1324"
    )

    parser.add_argument(
        "--output_dir",
        help="Output dir for current simulation",
        default=""
    )

    parser.add_argument(
        "--report_eacct_metrics",
        help="Report eacct metrics if available."
    )

    return parser.parse_args()


def main():
    """
    Helper script for plotting job status to terminal
    """

    args = get_parsed_args()
    header = {'token': args.http_token}
    full_job_dict = {}

    while True:

        # Ping the launcher REST API to get the jobs list
        try:
            response = requests.get(f'http://{args.http_bind}:{args.http_port}/jobs',
                                    headers=header).json()
        except Exception:
            print('Melissa study completed.')
            break

        # Create a full job dict
        state_dict = update_job_dict(full_job_dict, response, args, header)

        # Create the terminal page
        # plot the job states as a bar chart
        plot_state_dict(state_dict)
        # parse and summarize ear info if available
        if args.report_eacct_metrics:
            try:
                get_eacct_ear_output(full_job_dict)
            except Exception as e:
                print(f'Unable to get eacct output {e}.')
        # print the tail of the launcher/server logs
        get_server_launcher_logs(args)

        time.sleep(5)


if __name__ == "__main__":
    sys.exit(main())
