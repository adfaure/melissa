#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import errno
import logging
import os
import select
import socket
import sys
import threading
import subprocess
from typing import cast, Dict, Generic, List, Optional, Tuple, TypeVar, Union
from pathlib import Path

from melissa.utility import networking, process
from melissa.utility.networking import Socket
from melissa.scheduler.scheduler import Scheduler, IndirectScheduler
from melissa.scheduler.scheduler import Options as SchedulerOptions
from melissa.launcher.state_machine import Phase

from . import action, event, message
from .processor import Processor, DefaultProcessor
from . import queue
from melissa.launcher import config

logger = logging.getLogger(__name__)

JobT = TypeVar("JobT")


class IoMaster(Generic[JobT]):
    # the event file descriptors are reading and writing ends of a pipe or a
    # UNIX domain socket; they are passed explicitly for testing purposes.
    #
    # the event FIFO should have unlimited size because the thread reading from
    # the queue will also be putting elements into the queue.
    def __init__(
        self, listenfd: Socket, signalfd: Socket, timerfd: Socket,
        webfd: Socket, eventfd_r: Socket, events: queue.Queue,
        scheduler: Scheduler[JobT], client_options: SchedulerOptions,
        server_options: SchedulerOptions, processor: Union[Processor, DefaultProcessor],
        protocol: int, job_limit: int = 1000, std_output: bool = True
    ) -> None:
        self._listenfd = listenfd
        self._protocol = protocol
        self._signalfd = signalfd
        self._timerfd = timerfd
        self._webfd = webfd
        self._eventfd_r = eventfd_r
        self._events = events
        self._scheduler = scheduler
        self._client_options = client_options
        self._server_options = server_options
        self._uid = 0
        self._peers = []  # type: List[Socket]
        self._decoders = {
        }  # type: Dict[int, networking.LengthPrefixFramingDecoder]
        self._processor = processor
        self._poller = select.poll()
        self._minions = []  # type: List[threading.Thread]
        self._child_processes = []  # type: List[process.Process[str]]
        self.job_limit = job_limit
        self.std_output: bool = std_output

        # this counter is used to keep track of the submitted jobs
        # that have not been added to the jobs list yet
        self.job_count: int = 0

        # a poller index is introduced to make sure the server socket (peerfd)
        # has a chance to be checked after each job postponement
        self.poller_idx: int = 0

        # a postponed_job list is added to avoid overloading the event queue
        # it will contain all postponed JobSubmission messages
        self.postponed_job_list: List[event.MessageReception] = []

    def _close_connection(self, fd: int) -> None:
        if fd in self._decoders:
            del self._decoders[fd]
        sockfd = [p for p in self._peers if p.fileno() == fd][0]
        self._poller.unregister(sockfd)
        self._peers.remove(sockfd)
        sockfd.close()

    def _next_uid(self) -> int:
        uid = self._uid
        self._uid += 1
        return uid

    def _handle_file_descriptor_readable(self, fd: int) -> List[action.Action]:
        if fd == self._listenfd.fileno():
            peerfd, peer_addr = self._listenfd.accept()
            logger.info(f"accepted connection from {peer_addr}")

            self._peers.append(peerfd)
            self._poller.register(peerfd, select.POLLIN)

            if self._protocol == socket.IPPROTO_SCTP:
                try:
                    # TODO query the system for the value
                    SCTP_NODELAY = 3
                    # send pings immediately
                    peerfd.setsockopt(socket.IPPROTO_SCTP, SCTP_NODELAY, 1)
                except Exception as e:
                    logger.warning(
                        f"failed to enable SCTP_NODELAY on socket {peerfd}, {e}"
                    )
                    peerfd.close()
                    raise
            elif self._protocol == socket.IPPROTO_TCP:
                self._decoders[peerfd.fileno()
                               ] = networking.LengthPrefixFramingDecoder(
                    config.TCP_MESSAGE_PREFIX_LENGTH
                )
            else:
                raise NotImplementedError(
                    f"no support for protocol {self._protocol}")

            cid = peerfd.fileno()
            return self._processor.execute(event.NewConnection(cid))

        if fd == self._signalfd.fileno():
            b = self._signalfd.recv(1)
            if len(b) == 0:
                return []
            signo = int.from_bytes(b, byteorder=sys.byteorder)
            return self._processor.execute(event.Signal(signo))

        if fd == self._timerfd.fileno():
            b = self._timerfd.recv(1)
            if len(b) == 0:
                return []
            return self._processor.execute(event.Timeout())

        if fd == self._eventfd_r.fileno():
            self._eventfd_r.recv(1)
            num_items = self._events.qsize()
            if num_items >= 16:
                logger.warning(f"event FIFO contains {num_items} items")
            ev = self._events.get()

            if isinstance(ev, event.ProcessCompletion_):
                return self._handle_process_completion(ev)

            if isinstance(ev, event.JobPostponing):
                # poller index alternates not to miss server pings (last socket)
                # signals and timeout (first sockets)
                self.poller_idx = -1 if self.poller_idx == 0 else 0
                return self._handle_job_postponing(ev)

            if isinstance(ev, event.JobSubmission):
                # a JobSubmission event indicate that a MessageReception
                # transition occurred and one job was added to the jobs list
                # the counter must then be decremented
                if self.job_count > 0:
                    self.job_count -= 1

            return self._processor.execute(ev)

        if fd == self._webfd.fileno():
            return self._processor.execute(event.HttpRequest_())

        raw_bytes = os.read(fd, 4096)

        if len(raw_bytes) == 0:
            self._close_connection(fd)
            return self._processor.execute(event.ConnectionShutdown(fd))

        if self._protocol == socket.IPPROTO_SCTP:
            dsz_msg = message.deserialize(raw_bytes)
            assert isinstance(self._processor, DefaultProcessor)
            if (
                isinstance(dsz_msg, message.JobSubmission)
                and len(self._processor._state.jobs) + self.job_count >= self.job_limit
            ):
                logger.debug(
                    f"job-limit was reached {len(self._processor._state.jobs)} + {self.job_count}, "
                    "job submission postponed"
                )
                if not self.postponed_job_list:
                    self._events.put(event.JobPostponing())
                self.postponed_job_list.append(event.MessageReception(fd, dsz_msg))
                return [action.PostponeSubmission()]
            else:
                logger.debug("execute message reception")
                return self._processor.execute(
                    event.MessageReception(fd, dsz_msg)
                )

        if self._protocol == socket.IPPROTO_TCP:
            byte_blocks = self._decoders[fd].execute(raw_bytes)
            actions = []  # type: List[action.Action]
            for bs in byte_blocks:
                dsz_msg = message.deserialize(bs)
                assert isinstance(self._processor, DefaultProcessor)
                if (
                    isinstance(dsz_msg, message.JobSubmission)
                    and len(self._processor._state.jobs) + self.job_count >= self.job_limit
                ):
                    logger.debug(
                        f"job-limit reached: {len(self._processor._state.jobs)} + {self.job_count},"
                        " job submission postponed"
                    )
                    if not self.postponed_job_list:
                        self._events.put(event.JobPostponing())
                    self.postponed_job_list.append(event.MessageReception(fd, dsz_msg))
                    actions.extend([action.PostponeSubmission()])
                else:
                    if isinstance(dsz_msg, message.JobSubmission):
                        self.job_count += 1
                    logger.debug("execute message reception")
                    actions.extend(
                        self._processor.execute(event.MessageReception(fd, dsz_msg))
                    )

            return actions

        raise NotImplementedError("BUG protocol {:d}".format(self._protocol))

    def _handle_process_completion(
        self, pc: event.ProcessCompletion_
    ) -> List[action.Action]:
        assert isinstance(self._scheduler, IndirectScheduler)

        stdout_filename, stderr_filename = self._make_io_redirect_filenames(
            self._scheduler, pc.id
        )
        with open(stdout_filename, "r") as f:
            stdout = f.read()
        if stdout == "":
            os.unlink(stdout_filename)

        with open(stderr_filename, "r") as f:
            stderr = f.read()
        if stderr == "":
            os.unlink(stderr_filename)

        exit_status = pc.process.returncode
        proc = process.CompletedProcess(exit_status, stdout, stderr)

        try:
            ev = event.Event()  # mypy Python 3.5 work-around
            if isinstance(pc.action, action.JobCancellation):
                self._scheduler.parse_cancel_jobs(pc.action.jobs, proc)
                ev = event.JobCancellation(pc.action.jobs)
                logger.debug(f"job cancellation succeeded (UID {pc.id})")

            elif isinstance(pc.action, action.JobSubmission):
                job = self._scheduler.make_job(proc, pc.id)
                ev = event.JobSubmission(pc.action, job)
                logger.debug(
                    f"job submission succeeded (UID {pc.id} ID {job.id()})"
                )
                # a JobSubmission event indicate that a MessageReception
                # transition occurred and one job was added to the jobs list
                # the counter must then be decremented
                if self.job_count > 0:
                    self.job_count -= 1

            elif isinstance(pc.action, action.JobUpdate):
                self._scheduler.parse_update_jobs(pc.action.jobs, proc)
                ev = event.JobUpdate(pc.action.jobs)
                logger.debug(f"job update succeeded (UID {pc.id})")

            else:
                raise NotImplementedError("BUG not implemented")

            # remove parsed std out and error files
            if not self.std_output:
                args = ["rm", f"{stdout_filename}", f"{stderr_filename}"]
                process.launch(args, {}, subprocess.DEVNULL, subprocess.DEVNULL)  # type: ignore

        except Exception as e:
            ev = event.ActionFailure(pc.action, e)
            logger.debug(f"scheduling action failed (UID {pc.id})")

        return self._processor.execute(ev)

    def _handle_job_cancellation(
        self, jc: action.JobCancellation[JobT]
    ) -> None:
        assert jc.jobs

        if self._scheduler.is_direct():
            t = threading.Thread(
                target=lambda: self._scheduler.cancel_jobs(jc.jobs)  # type: ignore
            )
            t.start()
            self._events.put(event.JobCancellation(jc.jobs))
            return

        sched = cast(IndirectScheduler[JobT], self._scheduler)
        uid = self._next_uid()
        args, env = sched.cancel_jobs(jc.jobs)
        proc = self._launch_process(uid, args, env)
        self._run_process_asynchronously(jc, uid, proc)
        logger.debug(f"cancelling jobs uid={uid}")

    def _handle_job_submission(self, sub: action.JobSubmission) -> None:
        if isinstance(sub, action.ServerJobSubmission):
            options = self._server_options
        else:
            options = self._client_options
        uid = self._next_uid()
        args, env = self._scheduler.submit_job(
            sub.commands, sub.environment, options, sub.job_name, uid
        )
        proc = self._launch_process(uid, args, env)

        if self._scheduler.is_direct():
            job = self._scheduler.make_job(proc, uid)  # type: ignore
            self._events.put(event.JobSubmission(sub, job))
            logger.debug(f"job launched uid={uid} id={job.id()}")
            return

        self._run_process_asynchronously(sub, uid, proc)
        logger.debug(f"submitting job uid {uid}")

    def _handle_job_update(self, ju: action.JobUpdate[JobT]) -> None:
        assert ju.jobs
        jobs = ju.jobs

        if self._scheduler.is_direct():
            self._scheduler.update_jobs(jobs)  # type: ignore
            self._events.put(event.JobUpdate(jobs))
            return

        sched = cast(IndirectScheduler[JobT], self._scheduler)
        uid = self._next_uid()
        args, env = sched.update_jobs(jobs)
        proc = self._launch_process(uid, args, env)
        self._run_process_asynchronously(ju, uid, proc)
        logger.debug(f"updating jobs uid {uid}")

    def _handle_message_sending(self, msg: action.MessageSending) -> None:
        peers = [p for p in self._peers if p.fileno() == msg.cid]
        assert len(peers) == 1
        peer = peers[0]
        try:
            if self._protocol == socket.IPPROTO_TCP:
                bs = networking.LengthPrefixFramingEncoder(
                    config.TCP_MESSAGE_PREFIX_LENGTH).execute(msg.message.serialize()
                                                              )
                ret = peer.send(bs, socket.MSG_DONTWAIT)
                assert ret == len(bs)
            else:
                bs = msg.message.serialize()
                ret = peer.send(bs, socket.MSG_DONTWAIT)
                assert ret == len(bs)
        except OSError as e:
            assert e.errno != errno.EMSGSIZE
            self._events.put(event.ActionFailure(msg, e))

    def _handle_job_postponing(
        self, ev: event.JobPostponing
    ) -> List[Union[action.Action, action.PostponeSubmission]]:
        assert isinstance(self._processor, DefaultProcessor)
        # make sure no additional client will be submitted if the server is dead
        if self._processor._state.phase == Phase.SERVER_DEAD:
            logger.warning("Server is dead, postponed job won't be submitted")
            self.postponed_job_list = []
            return [action.PostponeSubmission()]
        # postpone again if the job-limit is still reached
        elif len(self._processor._state.jobs) + self.job_count >= self.job_limit:
            logger.debug(
                f"job-limit reached: {len(self._processor._state.jobs)} + {self.job_count}, "
                "job submission postponed again"
            )
            self._events.put(ev)
            return [action.PostponeSubmission()]
        # submit job
        else:
            logger.debug(
                f"job-limit not reached: {len(self._processor._state.jobs)} + {self.job_count}, "
                "job will be submitted"
            )
            # as long as the JobSubmission event resulting from the transition below is not
            # processed the jobs list won't be updated so the counter must be incremented
            self.job_count += 1
            job_submission_message = self.postponed_job_list.pop(0)
            if len(self.postponed_job_list) > 0:
                self._events.put(ev)
            return self._processor.execute(
                job_submission_message
            )

    def run(self) -> int:
        for sock in [
            self._listenfd, self._signalfd, self._timerfd, self._webfd,
            self._eventfd_r
        ]:
            self._poller.register(sock, select.POLLIN)

        exit_status = None  # type: Optional[int]
        while exit_status is None:
            listfd = self._poller.poll()

            assert listfd != []

            def is_set(x: int, flag: int) -> bool:
                assert x >= 0
                assert flag >= 0
                return x & flag == flag

            # process only one event at a time. this is relevant, e.g., when
            # the state machine wants to stop.
            fd, revent = listfd[self.poller_idx]
            if is_set(revent, select.POLLIN):
                try:
                    actions = self._handle_file_descriptor_readable(fd)
                except Exception:
                    logger.warning("server job already inactive")
                    continue
            elif is_set(revent, select.POLLOUT):
                assert False
                continue
            elif is_set(revent, select.POLLERR):
                logger.warning(f"file descriptor {fd} read end closed")
                self._close_connection(fd)
                actions = self._processor.execute(event.ConnectionShutdown(fd))
            elif is_set(revent, select.POLLHUP):
                logger.warning("file descriptor {fd} write end closed")
                # continue
            elif is_set(revent, select.POLLNVAL):
                logger.error("file descriptor is closed {fd}")
                assert False
                continue

            for ac in actions:
                assert exit_status is None
                logger.debug(f"executing {type(ac)}")

                if isinstance(ac, action.ConnectionClosure):
                    self._close_connection(ac.cid)
                elif isinstance(ac, action.Exit):
                    exit_status = ac.status
                elif isinstance(ac, action.JobCancellation):
                    self._handle_job_cancellation(ac)
                elif isinstance(ac, action.JobSubmission):
                    self._handle_job_submission(ac)
                elif isinstance(ac, action.JobUpdate):
                    self._handle_job_update(ac)
                elif isinstance(ac, action.MessageSending):
                    self._handle_message_sending(ac)
                elif isinstance(ac, action.PostponeSubmission):
                    pass
                else:
                    logger.error(f"unhandled action {ac}")

            self._minions = [t for t in self._minions if t.is_alive()]
            self._child_processes = [
                p for p in self._child_processes if p.poll() is None
            ]

        for sock in self._peers:
            sock.close()

        if self._child_processes:
            logger.warning(
                f"{len(self._child_processes)} child processes still running"
            )

        if self._minions:
            logger.warning(
                "list of worker threads not empty (length {len(self._minions)})"
            )

        # indirect scheduler: the object in the queue is assumed to be a
        # ProcessCompletion_ instance from the job cancellation
        if (self._scheduler.is_direct() and not self._events.empty()) \
                or self._events.qsize() > 1:
            logger.warning(
                f"expected empty event queue, have {self._events.qsize()} queued items"
            )

        return exit_status

    @classmethod
    def _make_io_redirect_filenames(cls, scheduler: Scheduler[JobT],
                                    uid: int) -> Tuple[str, str]:
        """
        Generate names for the files storing standard output and standard error
        of a process.
        """
        Path("./stdout").mkdir(parents=True, exist_ok=True)

        def f(suffix: str) -> str:
            return f"./stdout/{scheduler.name()}.{uid}.{suffix}"

        return f("out"), f("err")

    def _launch_process(
        self,
        uid: int,
        args: process.ArgumentList,
        env: process.Environment,
    ) -> "process.Process[str]":
        logger.info(f"submission command: {' '.join(args)}")
        if not self.std_output and self._scheduler.is_direct():
            return process.launch(args, env, subprocess.DEVNULL, subprocess.DEVNULL)  # type: ignore
        stdout_filename, stderr_filename = self._make_io_redirect_filenames(
            self._scheduler, uid
        )
        with open(stdout_filename, "w") as stdout:
            with open(stderr_filename, "w") as stderr:
                return process.launch(args, env, stdout, stderr)

    def _run_process_asynchronously(
        self, ac: action.Action, uid: int, proc: "process.Process[str]"
    ) -> None:
        assert not self._scheduler.is_direct()

        minion = threading.Thread(
            target=self._wait_for_process, args=(self._events, ac, uid, proc)
        )
        self._child_processes.append(proc)
        self._minions.append(minion)
        minion.start()

    @classmethod
    def _wait_for_process(
        cls,
        events: queue.Queue,
        ac: action.Action,
        uid: int,
        proc: "process.Process[str]",
    ) -> None:
        try:
            proc.wait()
            events.put(event.ProcessCompletion_(ac, uid, proc))
        except Exception:
            # this situation would arise when the job submission number is restricted
            # e.g. if only n oarsub are allowed the additional ones will fail ultimately
            # causing the launcher to fail
            logger.error("wait_for_process thread crashed the launcher will stop")
