import rapidjson
from typing import Dict, Any, Tuple
import argparse
from jsonschema import Draft4Validator, validators
from jsonschema.exceptions import ValidationError
import logging
import sys

logger = logging.getLogger(__name__)

CONFIG_PARSE_MODE = rapidjson.PM_COMMENTS | rapidjson.PM_TRAILING_COMMAS


CONF_SCHEMA = {
    'type': 'object',
    'properties': {
        "server_filename": {"type": "string", "required": True, "message": "The name of the file containing the user defined server. Assumed to be in the same folder as the config."},
        "server_class": {"type": "string", "required": True, "message": "The class name of the user defined server inside the server_filename file."},
        "output_dir": {"type": "string", "required": True, "message": "The output dir to write results and logs. If relative path, then it is assumed relative to the CWD of the melissa-launcher command."},
        "study_options": {
            "type": "object",
            "message": "A custom dictionary which is accessible inside the server_class for users to parameterize their studies.",
            "properties": {
                "parameter_sweep_size": {"type": "integer", "message": "The number of clients to launch (or groups if using sobol indices)."},
                "num_samples": {"type": "integer", "default": 0, "message": "Number of samples expected to arrive from each client. When not given, it can be inferred by Melissa (DL server only)."},
                "verbosity": {"type": "integer", "default": 0, "message": "Set the logger verbosity. 3 includes all levels (including info, error, warning, and debug), 0 reduces to logging to minimum (error only)."}
            }
        },
        "dl_config": {
            "type": "object",
            "properties": {
                "simulation_timeout": {"type": "integer", "default": 400, "message": "Seconds of client inactivity between two messages before timing out the client."},
                "batch_size": {"type": "integer", "default": 10, "message": "Number of samples to build each batch."},
                "n_batches_update": {"type": "integer", "default": 10, "message": "Number of batches between validation checks and loss logging."},
                "buffer_size": {"type": "integer", "default": 10000, "message": "Maximum number of samples to store in the buffer (object used to generate batches for training)."},
                "per_server_watermark": {"type": "integer", "message": "Required number of samples in each server process buffer before batch creation and training can begin."},
                "tensorboard": {"type": "boolean", "default": True, "message": "Set to False to disable tensorboard logger entirely for production level runs where you do not wish to log metrics"},
                "get_buffer_statistics": {"type": "boolean", "default": False, "message": "Estimate buffer statistics each time a batch is generated and add to the tensorboard log. Requires custom server imlementation of `get_buffer_statistics()`."},
            },
            "message": "A custom dictionary which is accessible inside the server_class for users to customize their training loops and buffers."},
        "sa_config": {
            "type": "object",
            "properties": {
                "mean": {"type": "boolean", "default": True, "message": "Collect mean for all fields."},
                "variance": {"type": "boolean", "default": False, "message": "Collect variance for all fields."},
                "skewness": {"type": "boolean", "default": False, "message": "Collect skewness for all fields."},
                "kurtosis": {"type": "boolean", "default": False, "message": "Collect kurtosis for all fields."},
                "sobol_indices": {"type": "boolean", "default": False, "message": "Activate sobol indicies. Group count determined by study_options.parameter_sweep_size"},
            },
            "message": "A dictionary used to control the sensitivity analysis servers."
        },
        "server_config": {
            "type": "object",
            "default": {"preprocessing_commands": []},
            "properties": {
                "preprocessing_commands": {"type": "array", "default": [], "message": "Commands that will be preprocessed by bash prior to launching the server job."},
                "melissa_server_env": {"type": "string", "message": "Explicit path to the server installation. Typically does not need to be touched unless two different melissa installations are used."}
            },
            "message": "Special configuration for the server only.",
        },
        "client_config": {
            "type": "object",
            "properties": {
                "preprocessing_commands": {"type": "array", "default": [], "message": "Commands that will be preprocessed by bash prior to launching the client job."},
                "melissa_client_env": {"type": "string", "message": "Explicit path to find the client installation. Typically does not need to be touched unless two different melissa installations are used."}
            },
            "message": "Special configuration for the client only."},
        "launcher_config": {
            "type": "object",
            "properties": {
                "scheduler": {"type": "string", "required": True, "message": "Select scheduler, can be 'oar', 'slurm', 'openmpi'"},
                "server_executable": {"type": "string", "default": "server.sh", "message": "Experienced users only, used to modify the bash template."},
                "bind": {"type": "string", "default": "0.0.0.0", "message": "Address to bind the REST API."},
                "http_port": {"type": "integer", "default": 8888, "message": "Port to put the REST API."},
                "http_token": {"type": "string", "default": "", "message": "Token used to access REST API, leave empty to let Melissa generate a unique secure token on launch."},
                "fault_tolerance": {"type": "boolean", "default": True, "message": "Activate/deactivate fault tolerance."},
                "protocol": {"type": "string", "default": "auto", "message": "Experienced users only, Melissa determines best protocol automatically."},
                "std_output": {"type": "boolean", "default": True, "message": "Keep or delete the std out/err files from all jobs."},
                "scheduler_arg": {"type": "array", "default": [], "message": "Common arguments to pass to scheduler for both client and server."},
                "scheduler_arg_client": {"type": "array", "default": [], "message": "Arguments to pass to scheduler for client only."},
                "scheduler_arg_server": {"type": "array", "default": [], "message": "Arguments to pass to scheduler for server only."},
                "scheduler_server_command": {"type": "string", "message": "Option to change the execution command (e.g. in place of srun or mpirun)"},
                "scheduler_client_command": {"type": "string", "message": "Option to change the execution command (e.g. in place of srun or mpirun)"},
                "scheduler_server_command_options": {"type": "array", "default": [], "message": "Options to pass to the scheduler inside the client execution command. Example: ['mpi=pmi2'] which, with slurm, would yield an sbatch.X.sh file with srun mpi=pmi2 <other arguments>."},
                "scheduler_client_command_options": {"type": "array", "default": [], "message": "Options to pass to the scheduler inside the server execution command. Example: ['mpi=pmi2'] which, with slurm, would yield an sbatch.X.sh file with srun mpi=pmi2 <other arguments>."},
                "scheduler_arg_container": {"type": "array", "default": [], "message": "Arguments to pass to containers (e.g. oar-hybrid)."},
                "container_client_size": {"type": "integer", "default": 1, "message": "Size of the container."},
                "job_limit": {"type": "integer", "default": 1000, "message": "Maximum number of active jobs allowed."},
                "besteffort_allocation_frequency": {"type": "integer", "default": 1, "message": "The frequency of job submission to submit to best-effort queue."},
                "timer_delay": {"type": "integer", "message": "The minimal delay between two job status updates with the same value."},
                "server_timeout": {"type": "integer", "message": "Maximuma amount of seconds which defines a server timeout exit."},
                "verbosity": {"type": "integer", "default": 0, "message": "Set the logger verbosity. 3 includes all levels (including info, error, warning, and debug), 0 reduces to logging to minimum (error only)."}
            },
        }
    }
}


def _extend_validator(validator_class):
    """
    Extended validator for Melissa
    """
    validate_properties = validator_class.VALIDATORS['properties']

    def set_defaults(validator, properties, instance, schema):
        for prop, subschema in properties.items():
            if 'default' in subschema:
                instance.setdefault(prop, subschema['default'])

        for error in validate_properties(
            validator, properties, instance, schema,
        ):
            yield error

    return validators.extend(
        validator_class, {'properties': set_defaults}
    )


def validate_config(args: argparse.Namespace,
                    config: Dict[str, Any]) -> Tuple[argparse.Namespace,
                                                     Dict[str, Any]]:

    MelissaValidator = _extend_validator(Draft4Validator)
    try:
        MelissaValidator(CONF_SCHEMA).validate(config)
    except ValidationError as e:
        logger.critical(
            f"Invalid configuration. Reason: {e}"
        )

    return args, config


class bcolors:
    OKBLUE = '\033[32m'
    OKGREEN = '\033[91m'
    ENDC = '\033[0m'
    UNDERLINE = '\033[4m'


def print_options():

    print(f"{bcolors.UNDERLINE}Available config options{bcolors.ENDC}\n")
    for config in CONF_SCHEMA["properties"]:
        top_dict = CONF_SCHEMA["properties"][config]
        type = top_dict['type']
        try:
            message = top_dict["message"]
        except KeyError:
            message = ""
        print(f"{bcolors.OKBLUE}{config}{bcolors.ENDC}: {message} "
              f"Type {bcolors.UNDERLINE}{type}{bcolors.ENDC}.")

        if "properties" in CONF_SCHEMA["properties"][config]:
            conf_dict = CONF_SCHEMA["properties"][config]

            for property in conf_dict["properties"]:
                message = conf_dict['properties'][property]['message']
                type = conf_dict['properties'][property]['type']
                try:
                    default = conf_dict['properties'][property]['default']
                except KeyError:
                    default = "N/A"
                print(f"    {bcolors.OKGREEN}{property}{bcolors.ENDC}: {message} "
                      f"Default value {default}, Type {bcolors.UNDERLINE}{type}{bcolors.ENDC}.")
    sys.exit()
