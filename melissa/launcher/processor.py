#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from typing import List

from melissa.utility import time

from . import action, event
from . import state_machine as sm
from .monitoring import RestHttpServer


class Processor:
    def execute(self, ev: event.Event) -> List[action.Action]:
        raise NotImplementedError("Processor.execute()")


class DefaultProcessor(Processor):
    def __init__(
        self, cfg: sm.Configuration, initial_state: sm.State,
        rest_server: RestHttpServer
    ) -> None:
        self._config = cfg
        self._state = initial_state
        self._rest_server = rest_server

    def execute(self, ev: event.Event) -> List[action.Action]:
        if isinstance(ev, event.HttpRequest_):
            self._rest_server.handle_request()
            return []

        new_state, actions = sm.transition(
            self._config, self._state, time.monotonic(), ev
        )
        self._state = new_state
        self._rest_server.notify(ev, new_state, actions)
        return actions
