from typing import Protocol, Any, Union, List, TypeVar


class GetProtocol(Protocol):
    _is_reception_over: bool

    def _get_with_eviction(self, index: Union[List[int], int]) -> Any:
        ...

    def _get_without_eviction(self, index: Union[List[int], int]) -> Any:
        ...


class ThresholdProtocol(Protocol):
    threshold: int

    def _size(self) -> int:
        ...

    def _is_sampling_ready(self) -> bool:
        ...


Threshold = TypeVar("Threshold", bound=ThresholdProtocol)
