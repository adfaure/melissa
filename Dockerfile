# Copyright 2020 Institut National de Recherche en Informatique et en Automatique (https://www.inria.fr/)

ARG release=22.04

FROM ubuntu:$release

ENV PATH=/home/docker/.local/bin:$PATH
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ >/etc/timezone

COPY requirements.txt ./
COPY requirements_deep_learning.txt ./
COPY requirements_dev.txt ./

WORKDIR ./

RUN apt-get update && apt-get --yes install \
	build-essential \
	clang \
	cmake \
	gfortran \
	git \
	libboost-dev \
	libboost-numpy-dev \
	libhdf5-openmpi-dev \
	libopenblas-dev \
	libopenmpi-dev \
	libpython3-all-dev \
	libzmq5-dev \
	pkg-config \
	python3 \
	python3-pip \
	python3-numpy

RUN pip install --upgrade pip
RUN pip3 install -r requirements.txt -r requirements_dev.txt -r requirements_deep_learning.txt

ARG userid=1000
RUN adduser \
	--uid=$userid \
	--shell=/bin/bash \
	--gecos='Docker,,,,' \
	--disabled-password \
	docker
RUN echo "docker:docker" | chpasswd

USER docker
WORKDIR /home/docker

# OpenMPI workarounds
# Fix OMPI Vader: https://github.com/open-mpi/ompi/issues/4948
ENV OMPI_MCA_btl_vader_single_copy_mechanism=none

# Allow oversubscribing
ENV OMPI_MCA_rmaps_base_oversubscribe=1

# Disable busy waiting
ENV OMPI_MCA_mpi_yield_when_idle=1
