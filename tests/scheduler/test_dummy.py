#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import unittest

from melissa.utility import process

from melissa.scheduler.dummy import DummyJob, DummyScheduler
from melissa.scheduler.job import State
from melissa.scheduler.scheduler import Options


def submit_job(
    scheduler: DummyScheduler,
    caller_args: process.ArgumentList,
    options: Options,
    name: str,
    uid: int,
) -> DummyJob:
    args, env = scheduler.submit_job(
        [caller_args], env={}, options=options, name=name, unique_id=uid
    )
    with open("/dev/null", "w+") as stdout:
        with open("/dev/null", "w+") as stderr:
            proc = process.launch(args, env, stdout=stdout, stderr=stderr)
    return scheduler.make_job(proc, unique_id=uid)


class TestDummyScheduler(unittest.TestCase):
    def test_simple(self) -> None:
        scheduler = DummyScheduler()
        options = Options("", [])
        job = submit_job(scheduler, ["echo", "Hello, World!"], options, "hello", uid=0)

        self.assertEqual(job.state(), State.WAITING)
        scheduler.update_jobs([job])
        self.assertEqual(job.state(), State.RUNNING)
        scheduler.cancel_jobs([job])
        self.assertEqual(job.state(), State.FAILED)

        job = submit_job(scheduler, ["echo", "Hello, World!"], options, "hello2", uid=1)

        self.assertEqual(job.state(), State.WAITING)
        scheduler.cancel_jobs([job])
        self.assertEqual(job.state(), State.FAILED)
