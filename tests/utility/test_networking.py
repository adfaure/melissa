#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import contextlib
import os
import random
import socket
import threading
from typing import List
import unittest

from melissa.utility.networking import (
    get_available_protocols,
    connect_to_server,
    LengthPrefixFramingDecoder,
    LengthPrefixFramingEncoder,
    connect_to_launcher, make_passive_socket,
    protocol2str,
    str2protocol
)


class Test_prefix_length_framing(unittest.TestCase):
    all_bytes = bytes([b for b in range(256)])

    @classmethod
    def generate_message(cls, rng: random.Random, length: int) -> bytes:
        if hasattr(rng, "choices"):
            return bytes(rng.choices(cls.all_bytes, k=length))
        # TODO: remove when Python 3.5 compatibility is not needed
        return bytes([rng.choice(cls.all_bytes) for _ in range(length)])

    def test_sanity(self) -> None:
        self.assertRaises(
            ValueError, LengthPrefixFramingEncoder, prefix_length=0
        )
        self.assertRaises(
            ValueError, LengthPrefixFramingDecoder, prefix_length=0
        )

    def test_identity(self) -> None:
        rng = random.Random()
        for prefix_length in range(1, 5):
            for msglen in range(0, 20):
                encoder = LengthPrefixFramingEncoder(prefix_length)
                decoder = LengthPrefixFramingDecoder(prefix_length)
                msg = self.generate_message(rng, msglen)
                messages_dec = decoder.execute(encoder.execute(msg))
                self.assertEqual(len(messages_dec), 1)
                self.assertEqual(msg, messages_dec[0])
                self.assertEqual(decoder._buffer, b'')

    def test_bytewise_decode(self) -> None:
        rng = random.Random()
        for prefix_length in range(1, 5):
            num_messages = 5
            messages_in = [
                self.generate_message(rng, length=rng.randint(0, 10))
                for _ in range(num_messages)
            ]

            encoder = LengthPrefixFramingEncoder(prefix_length)
            bytes_in = b''.join([encoder.execute(m) for m in messages_in])

            messages_out = []  # type: List[bytes]
            decoder = LengthPrefixFramingDecoder(prefix_length)
            for i in range(len(bytes_in)):
                msgs = decoder.execute(bytes_in[i:i + 1])
                messages_out.extend(msgs)

            self.assertEqual(len(messages_in), len(messages_out))
            self.assertEqual(messages_in, messages_out)


class Test_connect_to_launcher(unittest.TestCase):
    @staticmethod
    def del_env(key: str) -> None:
        del os.environ[key]

    def test_missing_environment_variables(self) -> None:
        self.assertRaises(RuntimeError, connect_to_launcher)
        with contextlib.ExitStack() as cm:
            os.environ["MELISSA_LAUNCHER_HOST"] = "127.0.0.1"
            cm.callback(self.del_env, "MELISSA_LAUNCHER_HOST")
            self.assertRaises(RuntimeError, connect_to_launcher)

            os.environ["MELISSA_LAUNCHER_PORT"] = "1234"
            cm.callback(self.del_env, "MELISSA_LAUNCHER_PORT")
            self.assertRaises(RuntimeError, connect_to_launcher)

    def test_simple(self) -> None:
        protocol = socket.IPPROTO_TCP
        listenfd = make_passive_socket("localhost", protocol=protocol)
        with contextlib.ExitStack() as cm:
            listenfd = cm.enter_context(listenfd)
            host, port = listenfd.getsockname()

            os.environ["MELISSA_LAUNCHER_HOST"] = host
            os.environ["MELISSA_LAUNCHER_PORT"] = str(port)
            os.environ["MELISSA_LAUNCHER_PROTOCOL"] = protocol2str(protocol)
            cm.callback(self.del_env, "MELISSA_LAUNCHER_HOST")
            cm.callback(self.del_env, "MELISSA_LAUNCHER_PORT")
            cm.callback(self.del_env, "MELISSA_LAUNCHER_PROTOCOL")

            def client() -> None:
                with connect_to_launcher():
                    pass

            t = threading.Thread(target=client, daemon=True)
            t.start()

            listenfd.settimeout(1)
            peerfd, _ = listenfd.accept()
            peerfd.close()


class Test_connect_to_server(unittest.TestCase):
    @staticmethod
    def del_env(key: str) -> None:
        del os.environ[key]

    def test_missing_environment_variables(self) -> None:
        self.assertRaises(RuntimeError, connect_to_server)
        with contextlib.ExitStack() as cm:
            os.environ["MELISSA_SERVER_HOST"] = "127.0.1.1"
            cm.callback(self.del_env, "MELISSA_SERVER_HOST")
            self.assertRaises(RuntimeError, connect_to_server)

    def test_simple(self) -> None:
        listenfd = make_passive_socket("localhost", protocol=socket.IPPROTO_TCP)
        with contextlib.ExitStack() as cm:
            listenfd = cm.enter_context(listenfd)
            host, port = listenfd.getsockname()

            os.environ["MELISSA_SERVER_HOST"] = host
            os.environ["MELISSA_SERVER_PORT"] = str(port)
            cm.callback(self.del_env, "MELISSA_SERVER_HOST")
            cm.callback(self.del_env, "MELISSA_SERVER_PORT")

            def client() -> None:
                with connect_to_server():
                    pass

            t = threading.Thread(target=client, daemon=True)
            t.start()

            listenfd.settimeout(1)
            peerfd, _ = listenfd.accept()
            peerfd.close()


class Test_make_passive_socket(unittest.TestCase):
    def test_simple(self) -> None:
        for proto in get_available_protocols():
            with make_passive_socket(protocol=proto):
                pass

            with make_passive_socket(
                "localhost", protocol=proto, backlog=1
            ):
                pass


class Test_protocol2str(unittest.TestCase):
    def test_it(self) -> None:
        self.assertEqual(protocol2str(socket.IPPROTO_SCTP), "SCTP")
        self.assertEqual(protocol2str(socket.IPPROTO_TCP), "TCP")
        self.assertRaises(ValueError, protocol2str, -1)


class Test_str2protocol(unittest.TestCase):
    def test_simple(self) -> None:
        self.assertEqual(socket.IPPROTO_SCTP, str2protocol("SCTP"))
        self.assertEqual(socket.IPPROTO_TCP, str2protocol("TCP"))
        self.assertRaises(ValueError, str2protocol, -1)

    def test_identity(self) -> None:
        for name in ["SCTP", "TCP"]:
            self.assertEqual(name, protocol2str(str2protocol(name)))
