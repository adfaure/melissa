#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from copy import copy
import errno
import signal
from typing import cast, Optional
import unittest

from melissa.scheduler import job
from melissa.scheduler.dummy import DummyJob
from melissa.utility.time import Time

from melissa.launcher import action, event, message, state_machine as sm
from melissa.launcher.state_machine import Phase, transition


class Test_StateMachine(unittest.TestCase):
    def setUp(self) -> None:
        self.cfg = sm.Configuration(
            cwd="/nil",
            launcher_protocol="fantasy",
            launcher_host="localhost",
            launcher_port=1,
            server_executable="/bin/false",
        )

    def tearDown(self) -> None:
        del self.cfg

    def _make_running_state(
        self,
        num_client_jobs: int = 0,
        num_job_submissions: int = 0,
        server_cid: Optional[int] = None,
    ) -> sm.State:
        assert num_client_jobs >= 0
        assert num_job_submissions >= 0

        s0 = sm.State()
        t0 = Time()
        s1, actions_1 = sm.transition(self.cfg, s0, t0, event.Timeout())
        self.assertEqual(s1.phase, sm.Phase.SERVER_READY)
        server_job = DummyJob(100)
        server_job.set_state(job.State.RUNNING)
        s2, _ = sm.transition(
            self.cfg, s1, t0, event.JobSubmission(actions_1[0], server_job)
        )
        self.assertEqual(s2.phase, sm.Phase.SERVER_RUNNING)

        s2.server_cid = server_cid
        s2.server_job_uid = server_job.unique_id()

        for i in range(num_client_jobs):
            job_id = int(server_job.id()) + i + 1
            s2.jobs.append(DummyJob(job_id))
            s2.cid_to_uid_map.append((i, job_id))

        for i in range(num_job_submissions):
            s2.job_submissions.append(
                action.ClientJobSubmission(
                    working_directory="cwd",
                    commands=[["echo", "hello"]],
                    environment={},
                    job_name="hello-world",
                    client_id=num_client_jobs + i,
                )
            )

        return s2

    def _make_state(self, phase: sm.Phase) -> sm.State:
        if phase == sm.Phase.SERVER_READY:
            s0 = sm.State()
            t0 = Time()
            s1, _ = sm.transition(self.cfg, s0, t0, event.Timeout())
            assert s1.phase == sm.Phase.SERVER_READY
            return s1

        raise NotImplementedError("phase {:s} not implemented".format(phase))

    def test_action_failure_job_cancellation(self) -> None:
        s0 = self._make_running_state(num_client_jobs=3)
        assert s0.phase == sm.Phase.SERVER_RUNNING
        assert len(s0.jobs) == 4
        s0.jobs_cancelling = [s0.jobs[1].unique_id()]
        s0.jobs_to_cancel = [s0.jobs[2].unique_id()]

        # job cancellation failed
        t0 = Time(seconds=1)
        s1, actions_1 = sm.transition(
            self.cfg,
            s0,
            t0,
            event.ActionFailure(
                action.JobCancellation([s0.jobs[1]]), error=RuntimeError("foo")
            ),
        )

        self.assertEqual(s1.phase, sm.Phase.SERVER_RUNNING)
        self.assertEqual(len(s1.jobs), 4)
        self.assertFalse(s1.jobs_cancelling)
        self.assertEqual(len(s1.jobs_to_cancel), 2)
        self.assertIn(s0.jobs[1].unique_id(), s1.jobs_to_cancel)
        self.assertIn(s0.jobs[2].unique_id(), s1.jobs_to_cancel)
        self.assertFalse(actions_1)

        # retry job cancellation at next time-out
        t1 = t0 + Time(seconds=4)
        s2, actions_2 = sm.transition(self.cfg, s1, t1, event.Timeout())

        self.assertEqual(s2.phase, sm.Phase.SERVER_RUNNING)
        self.assertEqual(len(s2.jobs), 4)
        self.assertEqual(len(s2.cid_to_uid_map), 3)
        self.assertEqual(len(s2.jobs_cancelling), 2)
        self.assertFalse(s2.jobs_to_cancel)
        self.assertEqual(len(actions_2), 1)
        self.assertIsInstance(actions_2[0], action.JobCancellation)
        self.assertEqual(len(actions_2[0].jobs), 2)  # type: ignore
        # let job cancellation succeed this time
        t2 = t1 + Time(seconds=1)
        s3, _ = transition(
            self.cfg, s2, t2, event.JobCancellation(actions_2[0].jobs)  # type: ignore
        )

        self.assertEqual(s3.phase, Phase.SERVER_RUNNING)
        self.assertEqual(len(s3.jobs), 2)
        self.assertFalse(s3.jobs_to_cancel)
        self.assertFalse(s3.jobs_cancelling)
        self.assertEqual(len(s3.cid_to_uid_map) + 1, len(s3.jobs))

    def test_disconnect(self) -> None:
        """
        Server job is submitted, server connects, server disconnects.
        """
        s0 = self._make_running_state()
        s0.job_update_in_progress = True
        now_0 = Time(seconds=90)
        cid = 1234567890
        s1, actions_1 = sm.transition(self.cfg, s0, now_0, event.NewConnection(cid))

        self.assertEqual(s1.phase, sm.Phase.SERVER_RUNNING)
        self.assertEqual(s1.server_cid, cid)
        self.assertGreaterEqual(len(s1.job_submissions), 0)
        self.assertEqual(len(s1.jobs), 1)
        self.assertEqual(len(s1.jobs_cancelling), 0)
        self.assertEqual(len(actions_1), 0)

        t1 = now_0 + Time(seconds=1)
        s2, actions_2 = sm.transition(self.cfg, s1, t1, event.ConnectionShutdown(cid))

        self.assertEqual(s2.phase, sm.Phase.SERVER_DEAD)
        self.assertEqual(len(s2.jobs), 1)
        self.assertEqual(len(s2.jobs_cancelling), 1)
        self.assertFalse(s2.stop)

    def test_job_submission_event_after_connection_shutdown(self) -> None:
        s0 = self._make_running_state(num_job_submissions=1, server_cid=2022)
        t0 = Time()
        s1, actions_1 = transition(
            self.cfg, s0, t0, event.ConnectionShutdown(s0.server_cid)
        )
        self.assertEqual(s1.phase, Phase.SERVER_DEAD)
        self.assertIsNone(s1.server_cid)
        self.assertFalse(actions_1)

        t1 = t0 + Time(seconds=1)
        s2, actions_2 = transition(
            self.cfg, s1, t1, event.JobSubmission(s0.job_submissions[0], DummyJob(1234))
        )
        self.assertEqual(s2.phase, Phase.SERVER_DEAD)
        self.assertFalse(s2.job_submissions)
        self.assertEqual(len(s2.jobs), 2)
        self.assertEqual(len(actions_2), 1)
        self.assertIsInstance(actions_2[0], action.JobCancellation)

    def test_message_sending_connection_reset(self) -> None:
        # launcher attempts to send but sending fails
        s0 = self._make_running_state(server_cid=123)
        t0 = self.cfg.server_ping_interval + Time(seconds=1)
        s0.last_server_message = t0
        s0.last_job_update = t0
        s1, actions_1 = transition(self.cfg, s0, t0, event.Timeout())

        self.assertEqual(s1.phase, Phase.SERVER_RUNNING)
        self.assertEqual(len(actions_1), 1)
        self.assertIsInstance(actions_1[0], action.MessageSending)
        self.assertIsInstance(actions_1[0].message, message.Ping)  # type: ignore

        s2, _ = transition(
            self.cfg,
            s1,
            t0,
            event.ActionFailure(actions_1[0], OSError(errno.ECONNRESET)),
        )

        s3, actions_3 = transition(
            self.cfg, s2, t0, event.ConnectionShutdown(s0.server_cid)
        )

    def test_message_sending_job_submission(self) -> None:
        # server wants to submit two jobs
        s0 = self._make_running_state(
            num_client_jobs=0, num_job_submissions=0, server_cid=123
        )
        assert s0.server_cid
        t0 = Time(seconds=1)
        client_id_0 = 332211
        s1, actions_1 = sm.transition(
            self.cfg,
            s0,
            t0,
            event.MessageReception(
                s0.server_cid, message.JobSubmission(client_id_0, 2)
            ),
        )

        self.assertEqual(s1.phase, sm.Phase.SERVER_RUNNING)
        self.assertEqual(len(s1.job_submissions), 2)
        self.assertIsNotNone(s1.job_submissions[0].client_id)
        self.assertEqual(s1.job_submissions[0].client_id, client_id_0)
        self.assertIsNotNone(s1.job_submissions[1].client_id)
        self.assertEqual(s1.job_submissions[1].client_id, client_id_0 + 1)

        self.assertEqual(len(actions_1), 2)
        self.assertIsInstance(actions_1[0], action.JobSubmission)
        submission_0 = cast(action.JobSubmission, actions_1[0])
        self.assertEqual(submission_0.client_id, client_id_0)
        self.assertIsInstance(actions_1[1], action.JobSubmission)
        submission_1 = cast(action.JobSubmission, actions_1[1])
        self.assertEqual(submission_1.client_id, client_id_0 + 1)

        # second job submission succeeds
        t1 = t0 + Time(milliseconds=250)
        job_1 = DummyJob(11)
        s2, actions_2 = sm.transition(
            self.cfg, s1, t1, event.JobSubmission(submission_1, job_1)
        )

        self.assertEqual(s2.phase, sm.Phase.SERVER_RUNNING)
        self.assertEqual(len(s2.job_submissions), 1)
        self.assertEqual(len(s2.jobs), 2)
        self.assertEqual(len(s2.cid_to_uid_map), 1)
        self.assertEqual(s2.cid_to_uid_map[0][0], client_id_0 + 1)
        self.assertEqual(s2.cid_to_uid_map[0][1], job_1.unique_id())
        self.assertEqual(len(actions_2), 1)
        self.assertIsInstance(actions_2[0], action.MessageSending)
        msgsend_2 = cast(action.MessageSending, actions_2[0])
        self.assertIsInstance(msgsend_2.message, message.JobUpdate)
        job_update_2 = cast(message.JobUpdate, msgsend_2.message)
        self.assertEqual(job_update_2.job_id, client_id_0 + 1)
        self.assertEqual(job_update_2.job_state, job.State.WAITING)

        # first job submission fails
        t2 = t1 + Time(seconds=2)
        error = RuntimeError("could not connect to database")
        s3, actions_3 = sm.transition(
            self.cfg, s2, t2, event.ActionFailure(actions_1[0], error)
        )

        self.assertEqual(s3.phase, sm.Phase.SERVER_RUNNING)
        self.assertEqual(len(s3.job_submissions), 0)
        self.assertEqual(len(s3.jobs), 2)
        self.assertEqual(s3.cid_to_uid_map, s2.cid_to_uid_map)
        self.assertEqual(len(actions_3), 1)
        self.assertIsInstance(actions_3[0], action.MessageSending)
        msgsend_3 = cast(action.MessageSending, actions_3[0])
        self.assertIsInstance(msgsend_3.message, message.JobUpdate)
        job_update_3 = cast(message.JobUpdate, msgsend_3.message)
        self.assertEqual(job_update_3.job_id, client_id_0)
        self.assertEqual(job_update_3.job_state, job.State.ERROR)

    def test_regression_30(self) -> None:
        s0 = sm.State()
        t0 = Time()
        s1, actions_1 = transition(self.cfg, s0, t0, event.Timeout())

        self.assertEqual(s1.phase, Phase.SERVER_READY)
        self.assertEqual(len(actions_1), 1)
        self.assertIsInstance(actions_1[0], action.JobSubmission)

        server = DummyJob(89)
        server.set_state(job.State.RUNNING)
        t1 = t0 + Time(seconds=1)
        s2, actions_2 = transition(
            self.cfg, s1, t1, event.JobSubmission(actions_1[0], server)
        )
        self.assertEqual(s2.phase, Phase.SERVER_RUNNING)
        self.assertFalse(actions_2)

        cfg = copy(self.cfg)
        cfg.job_update_interval = Time(hours=1)
        t2 = t1 + cfg.server_ping_interval + Time(seconds=1)
        s3, actions_3 = transition(cfg, s2, t2, event.Timeout())
        self.assertEqual(s3.phase, Phase.SERVER_RUNNING)
        self.assertFalse(actions_3)

    def test_reorder_job_update_job_cancellation(self) -> None:
        s0 = self._make_running_state(server_cid=1)
        t0 = self.cfg.job_update_interval + Time(seconds=1)
        s0.last_server_message = t0
        s0.last_server_ping = t0

        s1, actions_1 = transition(self.cfg, s0, t0, event.Timeout())
        self.assertEqual(s1.phase, Phase.SERVER_RUNNING)
        self.assertTrue(s1.job_update_in_progress)
        self.assertEqual(len(actions_1), 1)
        self.assertIsInstance(actions_1[0], action.JobUpdate)
        self.assertEqual(len(actions_1[0].jobs), 1)  # type: ignore

        t1 = t0
        s2, actions_2 = transition(
            self.cfg, s1, t1, event.ConnectionShutdown(s0.server_cid)
        )
        self.assertEqual(s2.phase, Phase.SERVER_DEAD)
        self.assertEqual(len(s2.jobs), 1)
        self.assertEqual(len(s2.jobs_cancelling), 1)
        self.assertEqual(len(actions_2), 1)
        self.assertIsInstance(actions_2[0], action.JobCancellation)

        t2 = t1 + Time(seconds=1)
        server_job = copy(s0.jobs[0])
        server_job.set_state(job.State.TERMINATED)  # type: ignore
        s3, actions_3 = transition(self.cfg, s2, t2, event.JobUpdate([server_job]))
        self.assertEqual(s3.phase, Phase.SERVER_DEAD)
        self.assertFalse(s3.jobs)
        self.assertTrue(s3.jobs_cancelling)
        self.assertTrue(s3.stop)
        self.assertFalse(actions_3)

        t3 = t2 + Time(milliseconds=1)
        s4, actions_4 = transition(
            self.cfg, s3, t3, event.JobCancellation(actions_2[0].jobs)  # type: ignore
        )
        self.assertEqual(s4.phase, Phase.STOP)
        self.assertEqual(len(actions_4), 1)
        self.assertIsInstance(actions_4[0], action.Exit)

    def test_server_job_submission_fails(self) -> None:
        s0 = sm.State()
        now_0 = Time()
        s1, actions_1 = sm.transition(self.cfg, s0, now_0, event.Timeout())

        self.assertEqual(s1.phase, sm.Phase.SERVER_READY)
        self.assertEqual(len(actions_1), 1)
        self.assertIsInstance(actions_1[0], action.JobSubmission)

        t1 = now_0 + Time(seconds=1)
        ev_1 = event.ActionFailure(actions_1[0], RuntimeError("nonzero exit"))
        s2, actions_2 = sm.transition(self.cfg, s1, t1, ev_1)

        self.assertEqual(s2.phase, sm.Phase.STOP)
        self.assertEqual(len(actions_2), 1)
        self.assertIsInstance(actions_2[0], action.Exit)

    def test_simple(self) -> None:
        cfg = sm.Configuration(
            cwd="/nil",
            launcher_protocol="proto",
            launcher_host="localhost",
            launcher_port=1,
            server_executable="/bin/false",
        )
        s0 = sm.State()
        _, _ = sm.transition(cfg, s0, Time(seconds=0), event.Timeout())

    def test_transition_job_update_after_sigint(self) -> None:
        """
        Test the job update after job submission and after receiving SIGINT.
        """
        s0 = self._make_state(sm.Phase.SERVER_READY)
        self.assertEqual(len(s0.job_submissions), 1)
        self.assertIsNone(s0.server_cid)
        self.assertFalse(s0.job_update_in_progress)

        t0 = Time(seconds=1)
        s1, actions_1 = sm.transition(self.cfg, s0, t0, event.Signal(signal.SIGINT))
        self.assertEqual(s1.phase, sm.Phase.SERVER_DEAD)
        self.assertEqual(s1.job_submissions, s0.job_submissions)
        self.assertEqual(s1.jobs, s0.jobs)

        server = DummyJob(89)
        server.set_state(job.State.RUNNING)
        t1 = t0 + Time(seconds=2)
        s2, actions_2 = sm.transition(
            self.cfg, s1, t1, event.JobSubmission(s0.job_submissions[0], server)
        )
        self.assertEqual(s2.phase, sm.Phase.SERVER_DEAD)
        self.assertEqual(len(s2.jobs), 1)
        self.assertEqual(len(s2.jobs_cancelling), 1)

    def test_transition_job_update_no_clients(self) -> None:
        s0 = self._make_state(sm.Phase.SERVER_READY)

        server_job_running = DummyJob(1234567)
        server_job_running.set_state(job.State.RUNNING)
        now = Time(seconds=1)
        s1, _ = sm.transition(
            self.cfg,
            s0,
            now,
            event.JobSubmission(s0.job_submissions[0], server_job_running),
        )

        self.assertEqual(len(s1.jobs), 1)
        self.assertEqual(s1.jobs[0].unique_id(), s1.server_job_uid)
        self.assertEqual(s1.jobs[0].state(), job.State.RUNNING)
        self.assertEqual(s1.last_job_update, now)

        # test with outdated state
        s2 = copy(s1)
        s2.job_update_in_progress = True
        server_job_waiting = cast(DummyJob, copy(s2.jobs[0]))
        server_job_waiting.set_state(job.State.WAITING)
        now = Time(seconds=2)
        s3, _ = sm.transition(self.cfg, s2, now, event.JobUpdate([server_job_waiting]))

        self.assertEqual(len(s3.jobs), 1)
        self.assertEqual(s3.jobs[0].unique_id(), s1.server_job_uid)
        self.assertEqual(s3.jobs[0].state(), job.State.RUNNING)
        self.assertEqual(s3.last_job_update, now)
        self.assertFalse(s3.job_update_in_progress)
        self.assertIsNone(s3.server_cid)

        s4 = copy(s3)
        s4.job_update_in_progress = True
        server_job_terminated = cast(DummyJob, copy(s2.jobs[0]))
        server_job_terminated.set_state(job.State.TERMINATED)
        now = Time(seconds=3)
        s5, _ = sm.transition(
            self.cfg, s4, now, event.JobUpdate([server_job_terminated])
        )

        self.assertEqual(s5.phase, sm.Phase.STOP)
        self.assertTrue(s5.stop)

    def test_transition_job_update_server_timeout(self) -> None:
        """
        Server reported as RUNNING followed by a server time-out.
        """
        s0 = self._make_running_state()
        s0.job_update_in_progress = True

        server_job_running = cast(DummyJob, copy(s0.jobs[0]))
        server_job_running.set_state(job.State.RUNNING)
        t0 = Time(seconds=1)
        s1, actions_1 = sm.transition(
            self.cfg, s0, t0, event.JobUpdate([server_job_running])
        )

        self.assertEqual(len(s1.jobs), 1)
        self.assertEqual(s1.jobs[0].unique_id(), s1.server_job_uid)
        self.assertEqual(s1.jobs[0].state(), job.State.RUNNING)
        self.assertEqual(s1.last_job_update, t0)
        self.assertFalse(s1.job_update_in_progress)
        self.assertFalse(actions_1)

        t1 = t0 + self.cfg.server_ping_interval - Time(seconds=1)
        s2, actions_2 = sm.transition(self.cfg, s1, t1, event.Timeout())
        self.assertEqual(s2.phase, sm.Phase.SERVER_RUNNING)
        self.assertFalse(actions_2)

        t2 = t0 + 2 * self.cfg.server_ping_interval + Time(seconds=1)
        s3, actions_3 = sm.transition(self.cfg, s2, t2, event.Timeout())
        self.assertEqual(s3.phase, sm.Phase.SERVER_DEAD)
        self.assertEqual(len(actions_3), 2)
        self.assertEqual(len(s3.jobs_cancelling), 1)
        self.assertFalse(s3.job_submissions)

    def test_transition_job_update_with_clients(self) -> None:
        # set up state
        s0 = self._make_running_state(num_client_jobs=2, num_job_submissions=1)
        s0.job_update_in_progress = True

        # job submission succeeds
        t0 = Time(seconds=1)
        job_3 = DummyJob(123)
        job_3.set_state(job.State.RUNNING)
        s1, actions_1 = sm.transition(
            self.cfg, s0, t0, event.JobSubmission(s0.job_submissions[0], job_3)
        )

        self.assertEqual(s1.phase, sm.Phase.SERVER_RUNNING)
        self.assertFalse(s1.job_submissions)
        self.assertEqual(len(s1.jobs), 4)
        self.assertEqual(len(s1.cid_to_uid_map), 3)
        self.assertEqual(len(actions_1), 1)
        self.assertIsInstance(actions_1[0], action.MessageSending)
        self.assertIsInstance(actions_1[0].message, message.JobUpdate)  # type: ignore
        self.assertEqual(actions_1[0].message.job_id,  # type: ignore
                         s0.job_submissions[0].client_id)
        self.assertEqual(actions_1[0].message.job_state, job_3.state())  # type: ignore

        # perform job update
        new_job_1 = DummyJob(s0.jobs[1].id())
        new_job_1.set_state(job.State.RUNNING)

        t1 = t0 + Time(milliseconds=36)
        s2, actions_2 = sm.transition(
            self.cfg, s1, t1, event.JobUpdate([copy(s0.jobs[0]), new_job_1])
        )

        self.assertEqual(s2.phase, sm.Phase.SERVER_RUNNING)
        self.assertEqual(len(s2.jobs), 4)
        self.assertEqual(s2.jobs[1].state(), job.State.RUNNING)
        self.assertEqual(len(actions_2), 1)
        self.assertIsInstance(actions_2[0], action.MessageSending)
        self.assertIsInstance(actions_2[0].message, message.JobUpdate)  # type: ignore

    def test_transition_job_update_with_job_cancellation(self) -> None:
        s0 = self._make_running_state()
        s0.job_update_in_progress = True

        t0 = Time(seconds=1)
        cid = 255
        s1, _ = sm.transition(self.cfg, s0, t0, event.NewConnection(cid))
        self.assertEqual(s1.phase, sm.Phase.SERVER_RUNNING)
        self.assertIsNotNone(s1.server_cid)
        self.assertEqual(s1.server_cid, cid)

        t1 = t0 + Time(seconds=1)
        s2, actions_2 = sm.transition(self.cfg, s1, t1, event.ConnectionShutdown(cid))
        self.assertEqual(s2.phase, sm.Phase.SERVER_DEAD)
        self.assertFalse(s2.stop)
        self.assertIsNone(s2.server_cid)
        self.assertEqual(len(actions_2), 1)
        self.assertIsInstance(actions_2[0], action.JobCancellation)

        t2 = t1 + Time(seconds=2)
        s3, actions_3 = sm.transition(
            self.cfg, s2, t2, event.JobUpdate([copy(s0.jobs[0])])
        )
        self.assertEqual(s3.phase, sm.Phase.SERVER_DEAD)
        self.assertEqual(len(s3.jobs_cancelling), len(s2.jobs_cancelling))
        self.assertFalse(s3.job_update_in_progress)
        self.assertFalse(actions_3)

    def test_transition_timeout_job_update(self) -> None:
        s0 = self._make_running_state()
        assert not s0.job_update_in_progress
        t0 = self.cfg.job_update_interval + Time(seconds=1)
        s0.last_server_ping = t0
        s0.last_server_message = t0
        self.assertTrue(s0.jobs)
        s1, actions = sm.transition(self.cfg, s0, t0, event.Timeout())

        self.assertEqual(s1.phase, sm.Phase.SERVER_RUNNING)
        self.assertTrue(s1.job_update_in_progress)
        self.assertEqual(len(actions), 1)
        self.assertIsInstance(actions[0], action.JobUpdate)

        update = cast(action.JobUpdate[DummyJob], actions[0])

        self.assertEqual(
            [j.unique_id() for j in s0.jobs], [j.unique_id() for j in s1.jobs]
        )
        self.assertEqual(
            [j.unique_id() for j in s0.jobs], [j.unique_id() for j in update.jobs]
        )

    def test_transition_timeout_server_ping(self) -> None:
        s0 = self._make_running_state(
            num_client_jobs=1, num_job_submissions=1, server_cid=127
        )
        assert not s0.job_update_in_progress
        now = self.cfg.job_update_interval + Time(seconds=1)
        s0.last_server_message = now
        s0.job_update_in_progress = True
        s1, actions = sm.transition(self.cfg, s0, now, event.Timeout())

        self.assertEqual(s0.phase, s1.phase)
        self.assertTrue(s0.job_update_in_progress, s1.job_update_in_progress)
        self.assertEqual(s1.last_server_ping, now)
        self.assertEqual(len(actions), 1)
        self.assertIsInstance(actions[0], action.MessageSending)

        sending = cast(action.MessageSending, actions[0])
        self.assertEqual(s0.server_cid, sending.cid)
        self.assertIsInstance(sending.message, message.Ping)

    def test_transition_timeout_server_timeout(self) -> None:
        """test server time-out AFTER server connected to launcher"""
        s0 = self._make_running_state(num_client_jobs=1, num_job_submissions=1)
        now = 2 * self.cfg.server_ping_interval + Time(milliseconds=1)
        s0.last_server_ping = now
        s0.job_update_in_progress = True
        s1, actions = sm.transition(self.cfg, s0, now, event.Timeout())

        self.assertEqual(s1.phase, sm.Phase.SERVER_DEAD)
        self.assertEqual(len(s1.job_submissions), 1)
        self.assertEqual(s1.jobs, s0.jobs)
        self.assertFalse(actions)
