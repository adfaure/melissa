#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from copy import copy
import signal
import socket
import sys
from typing import cast, List, TypeVar
import unittest

from melissa.utility.networking import socketpair
from melissa.scheduler import job
from melissa.scheduler.scheduler import IndirectScheduler, Scheduler, Options
from melissa.scheduler.dummy import DummyJob, DummyScheduler

from melissa.launcher import action, event, message, queue
from melissa.launcher.processor import Processor
from melissa.launcher.io import IoMaster

JobT = TypeVar("JobT")


def is_IPPROTO_SCTP_available() -> bool:
    available: bool = True
    try:
        socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_SCTP)
    except OSError:
        available = False
    return available


class StopProcessor(Processor):
    def __init__(self, exit_status: int = 0) -> None:
        super().__init__()
        self.exit_status = exit_status
        self.events = []  # type: List[event.Event]

    def execute(self, ev: event.Event) -> List[action.Action]:
        self.events.append(ev)
        return [action.Exit(self.exit_status)]


@unittest.skipIf(not is_IPPROTO_SCTP_available(), "Protocol SCTP is not available with socket.")
class Test_IoThread(unittest.TestCase):
    def setUp(self) -> None:
        self.listenfd = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_SCTP)
        self.listenfd.bind(("localhost", 0))
        self.listenfd.listen(1)

        self.signalfd, self.signalfd_test = socketpair()
        self.timerfd, self.timerfd_test = socketpair()
        self.webfd, self.webfd_test = socketpair()
        self.eventfd, self.eventfd_test = socketpair()
        self.events = queue.Queue(self.eventfd_test)

    def tearDown(self) -> None:
        all_fds = [
            self.listenfd,
            self.signalfd,
            self.signalfd_test,
            self.timerfd,
            self.timerfd_test,
            self.webfd,
            self.webfd_test,
            self.eventfd,
            self.eventfd_test,
        ]
        for fd in all_fds:
            if fd.fileno() != -1:
                fd.close()

    def make_io_master(self, scheduler: Scheduler[JobT], processor: Processor) -> IoMaster[JobT]:
        options = Options("", [])
        return IoMaster(
            self.listenfd,
            self.signalfd,
            self.timerfd,
            self.webfd,
            self.eventfd,
            self.events,
            scheduler,
            options,
            options,
            processor,
            socket.IPPROTO_SCTP,
        )

    # tests are executed in lexicographical order
    # the tests below ensures that the callee exits when the processor tells it
    # to
    def test_0_exit_signal(self) -> None:
        signo = signal.SIGTERM
        b = signo.to_bytes(1, byteorder=sys.byteorder)
        self.signalfd_test.send(b)

        processor = StopProcessor()
        io = self.make_io_master(DummyScheduler(), processor)
        io.run()

        self.assertEqual(len(processor.events), 1)
        self.assertIsInstance(processor.events[0], event.Signal)
        sigev = cast(event.Signal, processor.events[0])
        self.assertEqual(sigev.signo, signo)

    def test_0_exit_timeout(self) -> None:
        processor = StopProcessor()
        io = self.make_io_master(DummyScheduler(), processor)
        self.timerfd_test.send(b"\0")
        io.run()
        self.assertEqual(len(processor.events), 1)
        self.assertIsInstance(processor.events[0], event.Timeout)

    def test_event(self) -> None:
        ev = event.ActionFailure(action.MessageSending(0, message.Ping()), RuntimeError("foo"))
        self.events.put(ev)
        exit_status = 12
        processor = StopProcessor(exit_status)
        io = self.make_io_master(DummyScheduler(), processor)
        ret = io.run()

        self.assertEqual(len(processor.events), 1)
        self.assertEqual(processor.events[0], ev)
        self.assertEqual(ret, exit_status)

    def test_accept_connection(self) -> None:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_SCTP) as clientfd:
            clientfd.connect(self.listenfd.getsockname())

            processor = StopProcessor()
            io = self.make_io_master(DummyScheduler(), processor)
            io.run()

            self.assertEqual(len(processor.events), 1)
            self.assertIsInstance(processor.events[0], event.NewConnection)

    class _IndirectScheduler(IndirectScheduler[DummyJob]):
        @classmethod
        def _name_impl(cls) -> str:
            return "_IndirectScheduler"

        def _submit_job_impl(self, cmds, env, *args, **kwargs):  # type: ignore
            assert len(cmds) == 1
            return cmds[0], env

        def _make_job_impl(self, proc, uid):  # type: ignore
            if proc.exit_status != 0:
                raise RuntimeError("scheduler failure")
            return DummyJob(uid)

        def _update_jobs_impl(self, jobs):  # type: ignore
            args = ["true"] + [str(j.id()) for j in jobs]
            return args, {}

        def _parse_update_jobs_impl(self, jobs, proc):  # type: ignore
            if proc.exit_status != 0:
                raise RuntimeError("scheduler failure")
            for j in jobs:
                j.set_state(job.State.RUNNING)

    def _test_indirect_scheduler(
        self, scheduler: Scheduler[DummyJob], processor: Processor
    ) -> None:
        self.events.put(event.Timeout())
        io = self.make_io_master(scheduler, processor)
        io.run()

    class _JobSubmitter(Processor):
        def __init__(self, test: unittest.TestCase, cmd: List[str]) -> None:
            assert len(cmd) == 1
            super().__init__()
            self._test = test
            self._submission = action.ServerJobSubmission(
                working_directory="/",
                commands=[cmd],
                environment={},
                job_name="job-submitter",
            )

        def execute(self, ev: event.Event) -> List[action.Action]:
            if isinstance(ev, event.Timeout):
                return [self._submission]

            if isinstance(ev, event.JobSubmission):
                self._test.assertEqual(ev.submission, self._submission)
                return [action.Exit(0)]

            if isinstance(ev, event.ActionFailure):
                self._test.assertEqual(ev.action, self._submission)
                return [action.Exit(0)]

            raise NotImplementedError("BUG")

    def test_indirect_scheduler_job_submission(self) -> None:
        processor = self._JobSubmitter(self, ["true"])
        self._test_indirect_scheduler(self._IndirectScheduler(), processor)

    def test_indirect_scheduler_job_submission_failure(self) -> None:
        processor = self._JobSubmitter(self, ["false"])
        self._test_indirect_scheduler(self._IndirectScheduler(), processor)

    def test_indirect_scheduler_job_update(self) -> None:
        class MyProcessor(Processor):
            def __init__(self) -> None:
                super().__init__()
                self._job = DummyJob(2 ** 64)

            def execute(self_, ev: event.Event) -> List[action.Action]:
                if isinstance(ev, event.Timeout):
                    return [action.JobUpdate([copy(self_._job)])]

                if isinstance(ev, event.JobUpdate):
                    self.assertEqual(len(ev.jobs), 1)
                    self.assertEqual(ev.jobs[0].id(), self_._job.id())
                    self.assertEqual(ev.jobs[0].unique_id(), self_._job.unique_id())
                    self.assertNotEqual(ev.jobs[0].state(), self_._job.state())
                    return [action.Exit(0)]

                raise RuntimeError("test failed")

        class MyScheduler(IndirectScheduler[DummyJob]):
            @classmethod
            def _name_impl(cls) -> str:
                return "mine"

            def _update_jobs_impl(self, jobs):  # type: ignore
                return ["true", str(jobs[0].id())], {}

            def _parse_update_jobs_impl(self, jobs, proc):  # type: ignore
                if proc.exit_status != 0:
                    raise RuntimeError("scheduler failure")
                jobs[0].set_state(job.State.RUNNING)

        scheduler = MyScheduler()
        processor = MyProcessor()
        self.events.put(event.Timeout())
        io = self.make_io_master(scheduler, processor)
        io.run()
