#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import unittest

from melissa.utility.message import int2bytes
from melissa.scheduler.job import State

from melissa.launcher.message import (
    deserialize,
    InvalidMessage,
    Type,
    BYTES_PER_INT,
    Ping,
    JobSubmission,
    JobUpdate,
    JobCancellation,
    Exit,
)


class Test_deserialize(unittest.TestCase):
    def test_too_short(self) -> None:
        for num_bytes in range(4):
            msg = deserialize(b"".join([b"\0" for _ in range(num_bytes)]))
            self.assertIsInstance(msg, InvalidMessage)

    def test_wrong_size(self) -> None:
        msg = deserialize(int2bytes(Type.PING, BYTES_PER_INT + 1))
        self.assertIsInstance(msg, InvalidMessage)

    def test_serialization(self) -> None:
        messages = [
            Ping(),
            JobSubmission(1, 3),
            JobUpdate(8, State.RUNNING),
            JobCancellation(127),
            Exit(123),
        ]

        for m in messages:
            bs = m.serialize()
            n = deserialize(bs)
            self.assertEqual(m, n)
