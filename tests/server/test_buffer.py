from unittest import TestCase
import time

import melissa.server.deep_learning.buffer as mb


class TestBuffer(TestCase):
    def test_random_queue(self):
        n_items = 10
        buffer = mb.RandomQueue(maxsize=n_items)
        self.assertRaises(mb.Empty, buffer.get, block=False)
        self.assertRaises(mb.Empty, buffer.get, timeout=1)
        put_items = list(range(n_items))
        for item in put_items:
            buffer.put(item)
        self.assertTrue(len(buffer) == 10)
        self.assertRaises(mb.Full, buffer.put, [n_items + 1], block=False)
        self.assertRaises(mb.Full, buffer.put, [n_items + 1], timeout=1)
        get_items = [buffer.get() for _ in range(n_items)]
        self.assertSetEqual(set(get_items), set(put_items))
        self.assertNotEquals(get_items, put_items)

    def test_threshold_queue(self):
        n_items = 4
        threshold = 2
        buffer = mb.ThresholdQueue(n_items, threshold)
        put_items = list(range(n_items))
        for item in put_items:
            buffer.put(item)

        for _ in range(n_items - threshold):
            buffer.get()

        self.assertRaises(mb.Empty, buffer.get, block=False)
        self.assertRaises(mb.Empty, buffer.get, timeout=1)
        self.assertEqual(len(buffer), threshold)
        self.assertEqual(buffer.threshold, threshold)
        buffer.signal_reception_over()
        self.assertEqual(buffer.threshold, 0)
        item = buffer.get()
        self.assertTrue(item in put_items)

    def test_reservoir_queue(self):
        n_items = 1_000
        buffer_size = 100
        buffer = mb.ReservoirQueue(buffer_size)
        for item in range(n_items):
            buffer.put(item)
            self.assertEqual(len(buffer), min(item + 1, buffer_size))

    def test_radom_evict_on_write_queue(self):
        buffer_size = 100
        buffer = mb.RandomEvictOnWriteQueue(buffer_size)
        for item in range(buffer_size):
            buffer.put(item)
        # Not item has been seen, should not be allowed to put again
        self.assertEqual(len(buffer), buffer_size)
        self.assertRaises(mb.Full, buffer.put, None, block=False)
        self.assertRaises(mb.Full, buffer.put, None, timeout=1)
        time.sleep(1)
        buffer.get()
        self.assertEqual(len(buffer.seen), 1)
        self.assertEqual(len(buffer), buffer_size)
        buffer.put(None)
        self.assertEqual(len(buffer), buffer_size)

    def test_batch_random_evict_on_write_queue(self):
        buffer_size = 100
        batch_size = 8
        threshold = 10
        buffer = mb.BatchThresholdEvictOnWriteQueue(buffer_size, threshold, batch_size)
        for i in range(batch_size):
            buffer.put(i)
        # Threshold is not passed yet
        self.assertRaises(mb.Empty, buffer.get, block=False)
        for i in range(batch_size, threshold + 1):
            buffer.put(i)
        items = buffer.get()
        self.assertEqual(len(items), batch_size)
        # All items are different, sampling without replacement
        self.assertEqual(len(set(items)), batch_size)

        # We have threshold + 1 elements in the buffer
        self.assertEqual(len(buffer), threshold + 1)
        buffer.signal_reception_over()
        items = buffer.get()
        self.assertEqual(len(items), batch_size)
        self.assertEqual(
            len(buffer), threshold + 1 - batch_size
        )  # Elements are evicted from the buffer
        items = buffer.get()
        self.assertEqual(
            len(items), threshold + 1 - batch_size
        )  # Requested batch size can be discarded


def check_performance(buffer, n_samples, batch_size=None):
    from time import monotonic as time
    import threading

    import numpy as np
    import torch
    from torch.utils.data import IterableDataset, DataLoader

    class Dataset(IterableDataset):
        def __init__(self, buffer: mb.ThresholdQueue, n_samples: int):
            super().__init__()
            self.buffer = buffer
            self.n_samples = n_samples
            self.recv = threading.Thread(target=self.recv_fn)
            self.recv.start()
            self.is_reception_over = False

        def recv_fn(self):
            for _ in range(self.n_samples):
                fake_data = (
                    np.random.rand(1_000, 3).astype(np.float32),
                    np.random.rand(5).astype(np.float32),
                )
                self.buffer.put(fake_data)
            self.is_reception_over = True
            self.buffer.signal_reception_over()

        def __iter__(self):
            while not (self.is_reception_over and self.buffer.empty()):
                try:
                    data = self.buffer.get(timeout=1)
                except mb.NotEnoughData:
                    continue
                else:
                    yield data

    dataset = Dataset(buffer, n_samples)
    start_time = time()
    counter = 0
    if batch_size:
        collate_fn = None
    else:

        def collate_fn(data):
            x = np.array([d[0] for d in data])
            y = np.array([d[1] for d in data])
            return torch.from_numpy(x), torch.from_numpy(y)

    for batch_idx, batch in enumerate(
        DataLoader(dataset, num_workers=0, batch_size=batch_size, collate_fn=collate_fn)
    ):
        x, y = batch
        counter += x.shape[0]
        assert isinstance(x, torch.Tensor)
        assert isinstance(y, torch.Tensor)
    dataset.recv.join()
    end_time = time()
    print(buffer)
    print(f"Received {counter} data in {end_time - start_time} for {n_samples} generated.")


if __name__ == "__main__":
    buffer_size = 1_000
    threshold = 200
    n_samples = 500_000
    check_performance(mb.ThresholdQueue(buffer_size, threshold), n_samples, 16)
    check_performance(mb.ThresholdReservoirQueue(buffer_size, threshold), n_samples, 16)
    check_performance(mb.BatchThresholdEvictOnWriteQueue(buffer_size, threshold, 16), n_samples)
