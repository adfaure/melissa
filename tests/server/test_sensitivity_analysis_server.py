from unittest.mock import MagicMock
import logging
import numpy as np
import os
import argparse
import sys
from melissa.server.main import get_resolved_server, validate_config
from melissa.server.base_server import BaseServer
from melissa.server.simulation import PartialSimulationData


def get_naked_SensitivityAnalysisServer(default_sa_config) -> BaseServer:
    sys.argv = ["--project_dir tests/server"]
    parser = argparse.ArgumentParser()
    parser.add_argument("--project_dir", type=str, default="tests/server")
    parser.add_argument("--server_class")
    args = parser.parse_args()
    args, config = validate_config(args, default_sa_config)
    os.environ["MELISSA_FAULT_TOLERANCE"] = "OFF"
    server = get_resolved_server(args, config)
    return server


def model_ishigami(input_array):
    x1 = input_array[:, 0]
    x2 = input_array[:, 1]
    x3 = input_array[:, 2]
    # output formula from
    # https://openturns.github.io/openturns/latest/user_manual/_generated/openturns.MartinezSensitivityAlgorithm.html
    model_output = (
        np.sin(np.pi * x1)
        + 7 * np.square(np.sin(np.pi * x2))
        + 0.1 * (
            np.power(np.pi * x3, 4)
            * np.sin(np.pi * x1)
        )
    )
    return model_output


def test_sensitivity_analysis_server(default_sa_config, caplog, tmp_path):
    with caplog.at_level(logging.DEBUG, logger="melissa"):
        server = get_naked_SensitivityAnalysisServer(default_sa_config)
        server.node_name = MagicMock()

        # test check_group_size with sobol_op
        server.check_group_size()
        assert server.group_size == server.nb_parameters + 2
        assert server.num_clients == server.group_size * server.parameter_sweep_size

        # test draw_from_pick_freeze
        input_parameters = []
        for _ in range(server.num_clients):
            input_parameters.append(server.draw_from_pick_freeze())
        input_parameters = np.array(input_parameters)
        assert np.size(input_parameters) == server.num_clients * server.nb_parameters
        output_values = model_ishigami(input_parameters)
        output_values = output_values.reshape(
            server.parameter_sweep_size,
            server.group_size
        )

        # test compute_stats
        vec_1 = np.array([])
        vec_2 = np.array([])
        for output_id, output in enumerate(output_values):
            pdata = PartialSimulationData(0, output_id, 0, 1, "field", output)
            vec_1 = np.append(vec_1, output[0])
            vec_2 = np.append(vec_2, output[1])
            server.compute_stats(pdata)

        # test results
        # output mean from the random uniform inputs
        mean = (np.sum(vec_1) / len(vec_1) + np.sum(vec_2) / len(vec_2)) / 2.
        assert (
            abs(server.melissa_moments["field"][0][0].m1 - mean)
            < 1e-6
        )
        # output variance from the random uniform inputs
        variance = (
            (np.sum(vec_1 * vec_1) / len(vec_1) + np.sum(vec_2 * vec_2) / len(vec_2)) / 2.
            - mean * mean
        )
        assert (
            abs(server.melissa_moments["field"][0][0].theta2 - variance)
            < 1e-6
        )
        # sobol indices
        first_order_indices = [
            server.melissa_sobol["field"][0][0].
            first_order[param] for param in range(server.nb_parameters)
        ]
        total_order_indices = [
            server.melissa_sobol["field"][0][0].
            total_order[param] for param in range(server.nb_parameters)
        ]
        print(f"first order indices: {first_order_indices}")
        print(f"total order indices: {total_order_indices}")
