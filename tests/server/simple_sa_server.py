import logging
import random

from melissa.server.sensitivity_analysis import SensitivityAnalysisServer

logger = logging.getLogger("melissa")
random.seed(123)


class OTServerSA(SensitivityAnalysisServer):
    """
    Use-case specific server
    """
    def draw_parameters(self):
        param_set = []
        for _ in range(self.study_options['nb_parameters']):
            param_set.append(random.uniform(-1.0, 1.0))
        return param_set
