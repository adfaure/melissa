import unittest

import numpy as np

try:
    import torch
    import torch.utils.data
except ImportError:
    is_torch_available = False
else:
    is_torch_available = True

if is_torch_available:
    from melissa.server.simulation import SimulationData
    from melissa.server.deep_learning.buffer import (
        BaseQueue,
        ThresholdQueue,
        BatchThresholdEvictOnWriteQueue,
    )
    from melissa.server.deep_learning.dataset import MelissaIterableDataset


@unittest.skipIf(not is_torch_available, "PyTorch is not available")
class TestBaseQueueDataset(unittest.TestCase):
    def setUp(self):
        buffer = BaseQueue(maxsize=10)
        self.dataset = MelissaIterableDataset(buffer)
        fake_simulation_data = [
            SimulationData(i, j, np.random.rand(1_000, 3).astype(np.float32), parameters=[i])
            for j in range(5)
            for i in range(2)
        ]
        for data in fake_simulation_data:
            buffer.put(data)
        self.dataset._is_receiving = False

    def test_dataset(self):
        self.assertEqual(len(self.dataset.buffer), 10)
        counter = 0
        for data in self.dataset:
            self.assertIsInstance(data, np.ndarray)
            counter += 1
        self.assertEqual(counter, 10)

    def test_dataloader(self):
        batch_idx = 0
        for batch_idx, batch in enumerate(
            torch.utils.data.DataLoader(self.dataset, batch_size=2, num_workers=0), start=1
        ):
            self.assertIsInstance(batch, torch.Tensor)
            self.assertEqual(batch.shape, (2, 1_000, 3))
        self.assertEqual(batch_idx, 5)


@unittest.skipIf(not is_torch_available, "PyTorch is not available")
class TestThresholdQueueDataset(unittest.TestCase):
    def setUp(self):
        buffer = ThresholdQueue(maxsize=10, threshold=2)
        self.dataset = MelissaIterableDataset(buffer)
        fake_simulation_data = [
            SimulationData(i, j, np.random.rand(1_000, 3).astype(np.float32), parameters=[i])
            for j in range(5)
            for i in range(2)
        ]
        for data in fake_simulation_data:
            buffer.put(data)
        buffer.signal_reception_over()
        self.dataset._is_receiving = False

    def test_dataloader_drop_last(self):
        batch_idx = 0
        for batch_idx, batch in enumerate(
            torch.utils.data.DataLoader(self.dataset, batch_size=4, num_workers=0, drop_last=True),
            start=1,
        ):
            self.assertIsInstance(batch, torch.Tensor)
            self.assertEqual(batch.shape, (4, 1_000, 3))
        self.assertEqual(batch_idx, 2)  # Last batch should have been dropped


@unittest.skipIf(not is_torch_available, "PyTorch is not available")
class TestBatchQueueDataset(unittest.TestCase):
    def setUp(self):
        buffer = BatchThresholdEvictOnWriteQueue(maxsize=10, threshold=4, batch_size=2)
        self.dataset = MelissaIterableDataset(buffer)
        fake_simulation_data = [
            SimulationData(i, j, np.random.rand(1_000, 3).astype(np.float32), parameters=[i])
            for j in range(5)
            for i in range(2)
        ]
        for data in fake_simulation_data:
            buffer.put(data)
        buffer.signal_reception_over()
        self.dataset._is_receiving = False

    def test_dataset(self):
        self.assertEqual(len(self.dataset.buffer), 10)
        counter = 0
        for data in self.dataset:
            self.assertIsInstance(data, list)
            self.assertEqual(len(data), 2)
            counter += 1
        self.assertEqual(counter, 5)

    def test_dataloader_drop_last(self):
        def collate_fn(data):
            return torch.from_numpy(np.stack(data))

        batch_idx = 0
        for batch_idx, batch in enumerate(
            torch.utils.data.DataLoader(
                self.dataset, batch_size=None, num_workers=0, collate_fn=collate_fn
            ),
            start=1,
        ):
            self.assertIsInstance(batch, torch.Tensor)
            self.assertEqual(batch.shape, (2, 1_000, 3))
        self.assertEqual(batch_idx, 5)  # Last batch should have been dropped
