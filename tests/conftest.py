import re
import pytest


@pytest.fixture(scope="function")
def default_config():
    return get_default_config()


def get_default_config():
    config = {
        "server_filename": "lorenz_server.py",
        "server_class": "LorenzServer",
        "study_options": {
            "field_names": ["preposition", "position"],
            "parameter_sweep_size": 4,
            "num_samples": 2000,
            "nb_parameters": 3,
            "simulation_timeout": 400,
            "checkpoint_interval": 300,
            "crashes_before_redraw": 1000,
            "verbosity": 3
        },
        "dl_config": {
            "n_offline_simulations": 4,
            "batch_size": 20,
            "per_server_watermark": 2000,
            "buffer_size": 32000,
            "log_step_frequency": 1
        },
        "sweep_params": {
            "r": 30,
            "sigma": 10,
            "beta": 2.667,
            "rho": 28,
            "T": 20.0,
            "dt": 0.01
        },
        "launcher_config": {
            "output_dir": "STUDY_OUT",
            "scheduler": "openmpi",
            "no_fault_tolerance": True,
            "client_executable": "/home/user/lorenz/lorenz.py"
        }
    }

    return config


@pytest.fixture(scope="function")
def default_sa_config():
    return get_default_sa_config()


def get_default_sa_config():
    config = {
        "server_filename": "simple_sa_server.py",
        "server_class": "OTServerSA",
        "study_options": {
            "field_names": ["field"],
            "parameter_sweep_size": 1000,
            "num_samples": 1,
            "nb_parameters": 3,
            "verbosity": 3
        },
        "sa_config": {
            "mean": True,
            "variance": True,
            "skewness": True,
            "kurtosis": True,
            "sobol_indices": True
        },
        "launcher_config": {
            "scheduler": "openmpi"
        }
    }

    return config


def log_has_re(line, logs):
    """Check if line matches some caplog's message."""
    return any(re.match(line, message) for message in logs.messages)
