#!/usr/bin/python3
# from typing import Callable, Any, Optional
import logging
import torch
import torch.utils.data
import numpy as np
from typing import Tuple, Dict, Any, List

from melissa.server.deep_learning.torch_server import TorchServer, checkpoint
from melissa.server.simulation import SimulationData
from melissa.server.deep_learning.buffer import ThresholdQueue
from melissa.server.deep_learning.dataset import MelissaIterableDataset
from melissa.server.parameters import ParameterGenerator

logger = logging.getLogger("melissa")


class LorenzParameterGenerator(ParameterGenerator):

    def __init__(self, seed: int, n_samples, r, sigma, beta, rho, tf, dt):
        self.n_samples = n_samples
        self.sigma = sigma
        self.beta = beta
        self.rho = rho
        self.tf = tf
        self.dt = dt
        self.r = r
        super().__init__(seed)

    def populate_parameters(self):
        for _ in range(self.n_samples):
            x0 = -self.r // 2 + self.r * self.rng.random((3))
            parameters = [self.sigma, self.rho, self.beta, *x0, self.tf, self.dt]
            self.parameters.append(parameters)


class LorenzServer(TorchServer):
    """
    Use-case specific server
    """

    def __init__(self, config: Dict[str, Any]):
        super().__init__(config)
        self.parameter_generator = LorenzParameterGenerator(
            123,
            self.num_clients,
            self.sweep_params["r"],
            self.sweep_params["sigma"],
            self.sweep_params["beta"],
            self.sweep_params["rho"],
            self.sweep_params["T"],
            self.sweep_params["dt"]
        )

    def set_model(self):
        self.model = self.MyModel(3, 512).to(self.device)

    def configure_data_collection(self):
        """
        function designed to instantiate the data collector and
        buffer
        """
        self.buffer = ThresholdQueue(
            self.buffer_size, self.per_server_watermark, self.pseudo_epochs)
        self.dataset = MelissaIterableDataset(
            buffer=self.buffer,
            tb_logger=self.tb_logger,
            config=self.config,
            transform=self.process_simulation_data
        )

    def train(self, model: torch.nn.Module):

        dataloader = torch.utils.data.DataLoader(
            self.dataset, batch_size=self.batch_size, drop_last=True, num_workers=0)

        # Set Model
        criterion = torch.nn.MSELoss()
        optimizer = torch.optim.Adam(
            model.parameters(), lr=1e-3, weight_decay=1e-4)
        learning_rate_scheduler = torch.optim.lr_scheduler.ExponentialLR(
            optimizer, 0.995)

        logger.info("Start Training")
        for batch, batch_data in enumerate(dataloader):

            if self.other_processes_finished(batch):
                # user adds this to enforce all server procs
                # will stop together and avoid a deadlock
                # in the gradient averaging
                break

            optimizer.zero_grad()
            x, y_target = batch_data
            x, y_target = normalize(x, y_target)
            x = x.float().to(self.device)
            y_target = y_target.float().to(self.device)
            y_pred = model(x)
            loss = criterion(y_pred, y_target)
            # Backprogation
            loss.backward()
            optimizer.step()

            effective_batch = (batch + 1) * self.num_server_proc
            if effective_batch > 0 and effective_batch % self.n_batches_update == 0:
                self.tb_logger.log_scalar("Loss/train", loss.item(), step=effective_batch)
                learning_rate_scheduler.step()
                # Learning rate
                lrs = []
                for grp in optimizer.param_groups:
                    lrs.append(grp["lr"])
                self.tb_logger.log_scalar("lr", lrs[0], effective_batch)

        logger.info("Finish training")
        checkpoint(model=model, optimizer=optimizer,
                   batch=batch, loss=loss.item())

    def draw_parameters(self) -> List:
        parameters = self.parameter_generator.draw_parameters()
        return parameters

    def process_simulation_data(self, msg: SimulationData, config: dict):
        # Set positions inputs: x(n), x(n-1)
        x_prev = msg.data[0]
        x_next = msg.data[1]
        input_train = x_prev
        output_train = (x_next - x_prev) / config["sweep_params"]['dt']
        return input_train, output_train


# Values computed beforehand
x_mu = np.array([0.29185456, 0.3032881, 24.386353], dtype=np.float32)
x_std = np.array([8.135049, 9.0794735, 8.009931], dtype=np.float32)
y_mu = np.array([0.11236616, 0.0756601, 1.1951811], dtype=np.float32)
y_std = np.array([40.364864, 64.21792, 75.33695], dtype=np.float32)


def x_normalize(x: np.ndarray) -> np.ndarray:
    x_normalized = (x - x_mu) / (x_std + np.array([1e-7], dtype=np.float32))
    return x_normalized


def y_normalize(y: np.ndarray) -> np.ndarray:
    y_normalized = (y - y_mu) / (y_std + np.array([1e-7], dtype=np.float32))
    return y_normalized


def normalize(x: np.ndarray, y: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    x_normalized = x_normalize(x)
    y_normalized = y_normalize(y)
    return x_normalized, y_normalized


def y_unormalize(y: np.ndarray) -> np.ndarray:
    y_unormalized = y * (y_std + np.array([1e-7], dtype=np.float32)) + y_mu
    return y_unormalized
