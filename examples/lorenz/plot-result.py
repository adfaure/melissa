#!/usr/bin/python3

# Copyright (c) 2021, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import argparse
import matplotlib.pyplot as plt
import random
import torch
import numpy as np
import os
import sys
from scipy.integrate import odeint
from time import process_time
from collections import OrderedDict


class FakeModel(torch.nn.Module):
    def __init__(self, input_dim, n_features=16):
        super().__init__()
        self.input_dim = input_dim
        self.output_dim = input_dim
        self.n_features = n_features
        self.net = torch.nn.Sequential(
            torch.nn.Linear(self.input_dim, self.n_features),
            torch.nn.SiLU(),
            torch.nn.Linear(self.n_features, self.n_features),
            torch.nn.SiLU(),
            torch.nn.Linear(self.n_features, self.output_dim),
        )

    def forward(self, x):
        y = self.net(x)
        return y


def draw_param_set(nb_param: int):
    param_set = []
    for i in range(nb_param):
        param_set.append(random.uniform(-15, 15))
    return param_set


def load_model(nb_parameters, hidden_layer):
    model = FakeModel(nb_parameters, hidden_layer)
    checkpoint = torch.load("model.ckpt")
    new_checkpoint = OrderedDict()
    for k, v in checkpoint['model_state_dict'].items():
        name = k[7:]
        new_checkpoint[name] = v
    model.load_state_dict(new_checkpoint)
    model.eval()
    return model


def lorenz_deriv(x_y_z, t):
    x, y, z = x_y_z
    return [sigma * (y - x), x * (rho - z) - y, x * y - beta * z]


def generate_data(x_0, t):
    x_t = np.asarray([odeint(lorenz_deriv, x0_j, t) for x0_j in x_0])
    return x_t


@torch.no_grad()
def rollout_data(model, x0, t):

    x_mu = np.array([-6.49169099e-03, 2.28862808e-03, 2.44355196e+01])
    x_std = np.array([8.14842587, 9.08076499, 7.95232701])
    v_mu = np.array([0.08752598, 0.08246981, 1.20425387])
    v_std = np.array([40.09308709, 63.74225461, 74.81161489])

    x_rollout = []
    x_rollout.append(x0)

    def foo(x, *args):
        x_input = (torch.from_numpy(x) - x_mu) / x_std
        x_input = x_input.float()
        dx_pred = model(x_input).cpu().numpy() * v_std + v_mu
        return x + dx_pred * dt

    x_prev = x0.copy()
    for _ in t[1:]:
        x_t = foo(x_prev)
        x_rollout.append(x_t)
        x_prev = x_t
    return np.asarray(x_rollout)


def main():
    parser = argparse.ArgumentParser(
        description="Plot Melissa deep surrogate results as figures"
    )
    parser.add_argument("directory", help="Melissa output directory")
    parser.add_argument(
        "--coefficients",
        action='store_true',
        help="Compute the Lyapunov exponent and correlation coefficient (requires nolitsa)"
    )

    args = parser.parse_args()

    if not os.path.isdir(args.directory):
        return "{:s} is not a directory".format(args.directory)

    os.chdir(args.directory)

    t1_start = process_time()

    # 1. Reference solution for 5 trajectories
    np.random.seed(125)
    n_trajectories = 5
    t = np.arange(0, tf + dt, dt)
    for i in range(n_trajectories):
        x0 = -15 + 30 * np.random.random((n_trajectories, 3))
        x_t_test = generate_data(x0, t)
    x_0_test = x_t_test[:, 0, :]

    # 2. Neural Network solution running
    model = load_model(3, 512)
    x_pred = rollout_data(model, x_0_test, t)
    x_pred = np.moveaxis(x_pred, 0, 1)

    # 3. Results plotting
    print("Plotting data...")
    # 3D trajectories
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for j, color in zip(range(x_t_test.shape[0]), ["blue", "red", "black", "orange", "purple"]):
        x, y, z = x_t_test[j, :, :].T
        xd, yd, zd = x_pred[j, :, :].T
        if j == 0:
            ax.plot(x, y, z, lw=.5, c=color, label='ref. solution')
            ax.plot(xd, yd, zd, '--', lw=1, c=color, label='model')
        else:
            ax.plot(x, y, z, lw=.5, c=color)
            ax.plot(xd, yd, zd, '--', lw=1, c=color)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.scatter(x[0], y[0], z[0], color='g')
    ax.view_init(18, -13)
    plt.legend()
    plt.savefig("3D_trajectories.png")

    # 1D trajectories
    fig, (ax1, ax2, ax3) = plt.subplots(nrows=3, figsize=(27, 10), dpi=200)
    for j, color in zip(range(x_t_test.shape[0]), ["blue", "red", "black", "orange", "purple"]):
        x, y, z = x_t_test[j, :, :].T
        xd, yd, zd = x_pred[j, :, :].T
        if j == 0:
            ax1.plot(t, x, c=color, label='ref. solution')
            ax1.plot(t, xd, '--', c=color, label='model')
            ax2.plot(t, y, lw=1, c=color, label='ref. solution')
            ax2.plot(t, yd, '--', c=color, label='model')
            ax3.plot(t, z, c=color, label='ref. solution')
            ax3.plot(t, zd, '--', c=color, label='model')
        else:
            ax1.plot(t, x, c=color)
            ax1.plot(t, xd, '--', c=color)
            ax2.plot(t, y, lw=1, c=color)
            ax2.plot(t, yd, '--', c=color)
            ax3.plot(t, z, c=color)
            ax3.plot(t, zd, '--', c=color)
    ax1.legend()
    ax2.legend()
    ax3.legend()
    plt.savefig("1D_trajectories.png")
    plt.show()

    # Reference solution
    x0 = np.array([2, 1, 1])
    target_traj = generate_data(np.array([x0]), np.arange(5000) * dt)[0]
    pred_traj = rollout_data(model, np.array(x0), np.arange(5000))

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    x, y, z = target_traj[:, :].T
    xd, yd, zd = pred_traj[:, :].T
    ax.plot(x, y, z, linewidth=1, label='ref. solution')
    ax.plot(xd, yd, zd, '--', lw=1, label='model')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.scatter(x[0], y[0], z[0], color='g')
    ax.view_init(18, -13)
    plt.legend()
    plt.savefig("reference_trajectory.png")
    plt.show()

    # Lyapunov exponent and correlation coefficient
    if args.coefficients:
        from nolitsa import d2, lyapunov
        sample = 0.01
        # Choose appropriate Theiler window.
        window = 50

        d = lyapunov.mle(target_traj, maxt=1000, window=window)
        d_pred = lyapunov.mle(pred_traj, maxt=1000, window=window)
        t = np.arange(1000)
        fig = plt.figure()
        plt.plot(t, d, label='ref. solution')
        plt.plot(t, d_pred, '--', label='model')
        plt.legend()
        plt.savefig("lyapunov.png")
        plt.show()

        coeff_ref = np.polyfit(np.arange(10, 350) * sample, d[10:350], 1)[0]
        coeff_pred = np.polyfit(np.arange(10, 350) * sample, d_pred[10:350], 1)[0]

        print(
            f"Lyapunov exponent ref: {coeff_ref:.3e}, "
            f"model: {coeff_pred:.3e}, "
            f"relartive error: {abs(coeff_ref-coeff_pred)/coeff_ref*100} %"
        )
        print("Literature reports 0.906")

        # Correlation sum coefficients
        r_target, c_target = d2.c2(target_traj, window=window, metric="euclidean", r=50)
        r_pred, c_pred = d2.c2(pred_traj, window=window, metric="euclidean", r=50)
        fig = plt.figure()
        plt.plot(np.log(r_target), np.log(c_target), label='ref. solution')
        plt.plot(np.log(r_pred), np.log(c_pred), '--', label='model')
        plt.legend()
        plt.savefig("correlation.png")
        plt.show()

        coeff_ref = np.polyfit(np.log(r_target[15:40]), np.log(c_target[15:40]), 1)[0]
        coeff_pred = np.polyfit(np.log(r_pred[15:40]), np.log(c_pred[15:40]), 1)[0]

        print(
            f"Correlation sum ref: {coeff_ref:.3e}, "
            f"model: {coeff_pred:.3e}, "
            f"relative error: {abs(coeff_ref-coeff_pred)/coeff_ref*100} %"
        )

    t1_stop = process_time()
    print('Temps CPU :', t1_stop - t1_start)


if __name__ == "__main__":
    dt = 0.01
    tf = 20.
    beta = 8. / 3.
    sigma = 10
    rho = 28
    sys.exit(main())
