"""Lorenz attractor simulation. """

import argparse
import os
import sys
import time
import logging
from typing import Tuple

import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def lorenz_gradient(X: np.ndarray, sigma: float, rho: float, beta: float):
    """Compute the gradient of the system for a given x of shape [3, N]."""
    x, y, z = X
    return np.array([sigma * (y - x), x * (rho - z) - y, x * y - beta * z])


# Values computed beforehand
x_mu = np.array([0.29185456, 0.3032881, 24.386353], dtype=np.float32)
x_std = np.array([8.135049, 9.0794735, 8.009931], dtype=np.float32)
y_mu = np.array([0.11236616, 0.0756601, 1.1951811], dtype=np.float32)
y_std = np.array([40.364864, 64.21792, 75.33695], dtype=np.float32)


def x_normalize(x: np.ndarray) -> np.ndarray:
    x_normalized = (x - x_mu) / (x_std + np.array([1e-7], dtype=np.float32))
    return x_normalized


def y_normalize(y: np.ndarray) -> np.ndarray:
    y_normalized = (y - y_mu) / (y_std + np.array([1e-7], dtype=np.float32))
    return y_normalized


def normalize(x: np.ndarray, y: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    x_normalized = x_normalize(x)
    y_normalized = y_normalize(y)
    return x_normalized, y_normalized


def y_unormalize(y: np.ndarray) -> np.ndarray:
    y_unormalized = y * (y_std + np.array([1e-7], dtype=np.float32)) + y_mu
    return y_unormalized


def lorenz(sigma, rho, beta, x0: np.ndarray, t0: float, tf: float, dt: float):
    """Integrate lorenz equation using RK45.
    Expose the for loop in
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html#scipy.integrate.solve_ivp

    """
    assert x0.size == 3
    t_eval = np.arange(t0, tf + dt, dt)

    solver = scipy.integrate.RK45(lambda t, X: lorenz_gradient(
        X, sigma, rho, beta), t0=t0, y0=x0, t_bound=tf + dt)
    t_eval_i = 0
    x_old = x0

    status = "running"
    while status == "running":
        solver.step()
        status = solver.status
        t_eval_i_new = np.searchsorted(t_eval, solver.t, side="right")
        t_eval_step = t_eval[t_eval_i:t_eval_i_new]
        if t_eval_step.size > 0:
            sol = solver.dense_output()
            ts = np.array((t_eval_step)).T
            x_next = np.array((sol(t_eval_step))).T
            x_prev = np.vstack((x_old, x_next[:-1]))
            t_eval_i = t_eval_i_new
            x_old = x_next[-1]
        for t, x, y in zip(ts, x_prev, x_next):
            # If t is initial time there is no previous time step
            # Thus x and y would be equal
            if t > t0:
                yield t, x, y


def generate_trajectory(
    sigma: float, rho: float, beta: float, x0: np.ndarray, tf: float, dt: float, t0: float = 0
) -> Tuple[np.ndarray, np.ndarray]:
    x_prev = []
    x_next = []
    for _, x, y in lorenz(sigma, rho, beta, x0, t0, tf, dt):
        x_prev.append(x)
        x_next.append(y)
    return np.array(x_prev), np.array(x_next)


def test(sigma, rho, beta, x0: np.ndarray, tf: float, dt: float, t0: float = 0, plot=False):
    ts, xs = [], []
    for t, x, y in lorenz(sigma, rho, beta, x0, t0, tf, dt):
        print(",".join([str(i) for i in np.concatenate([x, y])]) + "\n")
        ts.append(t)
        xs.append(x)
    ts, xs = np.array(ts), np.array(xs)  # type: ignore

    if plot:
        fig, ax = plt.subplots(1, 1, subplot_kw={"projection": "3d"})
        x, y, z = xs.T  # type: ignore
        ax.plot(x, y, z, linewidth=1)
        ax.scatter(x[0], y[0], z[0], color="r")
        plt.show()
        fig, (ax1, ax2, ax3) = plt.subplots(nrows=3, figsize=(27, 10), dpi=200)
        ax1.plot(
            ts,
            x,
        )
        ax2.plot(
            ts,
            y,
            lw=1,
        )
        ax3.plot(
            ts,
            z,
        )
        plt.show()


def melissa_generate_data(sigma, rho, beta, x0: np.ndarray, tf: float, dt: float, t0: float = 0):
    sys.path.append(os.environ["MELISSA_INSTALL_PREFIX"] + "/lib")

    from mpi4py import MPI
    from melissa_api import melissa_init, melissa_finalize, melissa_send

    logger.info("Start simulation.")
    comm = MPI.COMM_WORLD
    field_position = "position"
    field_previous_position = "preposition"
    vect_size = 3
    melissa_init(field_previous_position, vect_size, comm)
    melissa_init(field_position, vect_size, comm)
    timestep_ctr = 0
    for t, x, y in lorenz(sigma, rho, beta, x0, t0, tf, dt):
        if t > t0 + 1e-5 and t < tf + 1e-5:
            melissa_send(field_previous_position, x)
            melissa_send(field_position, y)
            timestep_ctr += 1
    melissa_finalize()
    logger.info("Finish simulation.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--test", action="store_true")
    parser.add_argument("sigma", type=float)
    parser.add_argument("rho", type=float)
    parser.add_argument("beta", type=float)
    parser.add_argument("x0", nargs=3, type=float, help="inital position")
    parser.add_argument("tf", type=float, help="final time")
    parser.add_argument("dt", type=float, help="time step")
    args = parser.parse_args()
    logger.debug(args)

    t0 = time.time()

    if args.test:
        test(x0=np.array(args.x0), tf=args.tf, dt=args.dt,
             sigma=args.sigma, rho=args.rho, beta=args.beta)
    else:
        melissa_generate_data(
            x0=np.array(args.x0), tf=args.tf, dt=args.dt, sigma=args.sigma,
            rho=args.rho, beta=args.beta
        )

    tf = time.time()
    print(f"Elapsed time: {tf-t0} seconds")
