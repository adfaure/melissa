import os
import subprocess
import shlex
import numpy as np
import torch
from offline_tools import (TensorboardLogger,
                           Net,
                           checkpoint,
                           draw_param_set,
                           get_file_result)
from datetime import datetime
import argparse
import time


"""
Script used for running an offline comparison of the heat-pde example.

The study should be executed by navigating to `examples/heat-pde/heat-pde-dl/offline-example`
and running:

python3 run_offline_study.py

The script creates a folder named `offline-<YEAR><MONTH><DAY><TIME>`, then
generates data for n_simulations parameter combinations of heat-pde
solutions. These solutions are stored in a folder called `Res`. Next,
a neural network is trained on these solutions using `batch_size` and `epochs`.
The final trained model is saved as `model.ckpt` and is stored in
`offline-<YEAR><MONTH><DAY><TIME>` which can be used for post processing.

The script depends on the user having already built `heat_no_melissac`, which can
be done by navigating to `examples/heat-pde/executables/build` and executing the
following:

cmake ..
make
"""

path_to_heat_no_melissac = "../../../executables/build/heat_no_melissac"

# check for cuda and use if possible
if torch.cuda.is_available() and torch.cuda.device_count() >= 1:
    world_size = torch.cuda.device_count()
    device = torch.device("cuda:0")
    print("Found cuda enabled gpu")
else:
    device = torch.device("cpu:0")
    print("Using CPU")


def generate_data(args):
    # produce offline data
    results = []
    params = []
    os.makedirs(f"{args.out_dir}/Res", exist_ok=True)
    os.chdir(f"{args.out_dir}")
    mesh_size = args.mesh_size
    for i in range(args.n_simulations):
        os.system('clear')
        print(f"Getting reference result {i+1}/{args.n_simulations}")
        parameters = draw_param_set(5)
        params.append(parameters)
        parameters = [str(p) for p in parameters]
        command = path_to_heat_no_melissac + f" {mesh_size} {mesh_size} 100 " + " ".join(parameters)
        command = shlex.split(command)  # type: ignore
        subprocess.run(command)
        result = get_file_result(mesh_size)
        results.append(result)
    full_results = np.array(results)
    full_params = np.array(params)
    print("Finished generating data.")
    full_results.shape
    np.save("heat_pde_parameters", full_params)
    np.save("heat_pde", full_results)
    os.chdir("../")


def make_train_test_data(args):
    parameters = np.load(f"{args.out_dir}/heat_pde_parameters.npy")
    temperatures = np.load(f"{args.out_dir}/heat_pde.npy")

    # times = np.arange(100)
    inputs = []
    for p in parameters:
        for t in range(100):
            x = [*p, t]
            inputs.append(x)
    full_inputs = np.array(inputs).reshape(args.n_simulations, 100, -1)

    xs = full_inputs.reshape(-1, 6).astype(np.float32)
    ys = temperatures.reshape(-1, args.mesh_size * args.mesh_size).astype(np.float32)

    xs_valid = xs[:args.n_valid_sims * 100, ...]
    ys_valid = ys[:args.n_valid_sims * 100, ...]

    xs = xs[args.n_valid_sims * 100:, ...]
    ys = ys[args.n_valid_sims * 100:, ...]

    np.save(f"{args.out_dir}/heat_pde_parameters_valid", xs_valid)
    np.save(f"{args.out_dir}/heat_pde_valid", ys_valid)
    np.save(f"{args.out_dir}/heat_pde_parameters_train", xs)
    np.save(f"{args.out_dir}/heat_pde_train", ys)


def load_data(args):

    np_xs = np.load(f"{args.out_dir}/heat_pde_parameters_train.npy")
    np_ys = np.load(f"{args.out_dir}/heat_pde_train.npy")
    np_xs_valid = np.load(f"{args.out_dir}/heat_pde_parameters_valid.npy")
    np_ys_valid = np.load(f"{args.out_dir}/heat_pde_valid.npy")

    return np_xs, np_ys, np_xs_valid, np_ys_valid


def train_model(args, logger, np_xs, np_ys, np_xs_valid, np_ys_valid):

    torch_xs = torch.from_numpy(np_xs).squeeze()
    torch_ys = torch.from_numpy(np_ys).squeeze()
    torch_xs_valid = torch.from_numpy(np_xs_valid).squeeze()
    torch_ys_valid = torch.from_numpy(np_ys_valid).squeeze()

    model = Net(6, args.mesh_size * args.mesh_size, 1).to(device)
    model.train()

    # prepare dataset
    dataset = torch.utils.data.TensorDataset(torch_xs, torch_ys)
    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=args.batch_size, drop_last=True, num_workers=0,
        shuffle=True)

    dataset_valid = torch.utils.data.TensorDataset(torch_xs_valid, torch_ys_valid)
    dataloader_valid = torch.utils.data.DataLoader(
        dataset_valid, batch_size=args.batch_size, drop_last=True, num_workers=0,
        shuffle=True)

    n_test = torch_xs_valid.shape[0]
    # Set Model exactly the same as heatpde_server.py
    criterion = torch.nn.MSELoss()
    optimizer = torch.optim.Adam(
        model.parameters(), lr=1e-3, weight_decay=1e-4)
    learning_rate_scheduler = torch.optim.lr_scheduler.ExponentialLR(
        optimizer, 0.995)

    print("Starting training loop")
    # Train loop exactly the same as heatpde_server.py
    total_batches = 0
    full_loss = 0
    for epoch in range(0, args.epochs):
        print(f"On epoch {epoch} of {args.epochs}")
        start = time.time()
        for batch, batch_data in enumerate(dataloader):
            x, y_target = batch_data
            x = x.float().to(device)  # type: ignore
            y_target = y_target.float().to(device)
            y_pred = model(x)
            loss = criterion(y_pred, y_target)
            # Backprogation
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            full_loss += loss.item()
            total_batches += 1

            # Step learning rate
            if total_batches > 0 and total_batches % 100 == 0:
                logger.log_scalar("Loss/train", full_loss / (args.batch_size * 100), total_batches)
                full_loss = 0
                # compute and log validation loss
                val_loss = 0
                for _, batch_data in enumerate(dataloader_valid):
                    x, y_target = batch_data
                    x = x.float().to(device)  # type: ignore
                    y_target = y_target.float().to(device)
                    # model evaluation
                    with torch.no_grad():
                        model.eval()
                        y_pred = model(x)
                        loss = criterion(y_pred, y_target)
                        val_loss += loss.item()

                model.train()
                logger.log_scalar("Loss/valid", val_loss / n_test, total_batches)

                if args.lr == "stepped":
                    learning_rate_scheduler.step()
                lrs = []
                for grp in optimizer.param_groups:
                    lrs.append(grp["lr"])
                logger.log_scalar("lr", lrs[0], total_batches)

        time_spent = time.time() - start
        print(f"Epoch time {time_spent:.1f}")

    checkpoint(model=model, optimizer=optimizer, batch=batch,
               loss=loss.item(), path=f"{args.out_dir}/tb_{args.id}/model.ckpt")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--batch-size", type=int, default=10, help="batch size for train loop.")
    parser.add_argument("--epochs", type=int, default=100, help="number of epochs to train on.")
    parser.add_argument("--generate-data", type=bool, default=False,
                        help="to generate data or to load existing.")
    parser.add_argument("--train", type=bool, default=False, help="to train or not")
    parser.add_argument("--n-simulations", type=int, default=120, help="number of solutions")
    parser.add_argument("--n-valid-sims", type=int, default=20,
                        help="number of validation solutions")
    parser.add_argument("--out-dir", type=str, default=datetime.now().strftime(
        'melissa-%Y%m%dT%H%M%S'), help="number of recorded steps")
    parser.add_argument("--id", type=str, default=np.random.randint(int(1e6)),
                        help="number of recorded steps")
    parser.add_argument("--lr", type=str, default="stepped", help="number of recorded steps")
    parser.add_argument("--mesh-size", type=int, default=100, help="edge mesh discretization")

    args = parser.parse_args()
    print(args)

    os.makedirs(args.out_dir, exist_ok=True)
    logger = TensorboardLogger(rank=0, logdir=f"{args.out_dir}/tb_{args.id}")

    if args.generate_data:
        generate_data(args)
        make_train_test_data(args)

    if args.train:
        train_model(args, logger, *load_data(args))
