#!/usr/bin/python3

# Copyright (c) 2021, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import argparse
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import random
import torch
import numpy as np
import os
import sys
from time import process_time
from collections import OrderedDict


class FakeModel(torch.nn.Module):
    def __init__(self, input_features, output_features, output_dim):
        super().__init__()
        self.output_dim = output_dim
        self.output_features = output_features
        self.hidden_features = 256
        self.net = torch.nn.Sequential(
            torch.nn.Linear(input_features, self.hidden_features),
            torch.nn.ReLU(),
            torch.nn.Linear(self.hidden_features, self.hidden_features),
            torch.nn.ReLU(),
            torch.nn.Linear(self.hidden_features, output_features * output_dim),
        )

    def forward(self, x):
        y = self.net(x)
        return y


def draw_param_set(nb_param: int):
    param_set = []
    for i in range(nb_param):
        param_set.append(random.uniform(100, 200))
    return param_set


def load_model(nb_parameters, nb_fields, vect_size):
    model = FakeModel(nb_parameters + 1, vect_size, nb_fields)
    checkpoint = torch.load("model.ckpt")
    new_checkpoint = OrderedDict()
    for k, v in checkpoint['model_state_dict'].items():
        name = k[7:]
        new_checkpoint[name] = v
    model.load_state_dict(new_checkpoint)
    model.eval()
    return model


def generate_data(model, parameters, timesteps, Nx, Ny):
    x = np.zeros((len(timesteps), len(parameters) + 1))
    x[:, 0:-1] = parameters
    x[:, -1] = timesteps
    y = model(torch.from_numpy(x).float())
    y = y.detach().numpy().reshape(len(timesteps), Nx, Ny)
    return y


def main():
    parser = argparse.ArgumentParser(
        description="Plot Melissa deep surrogate results as movies"
    )
    parser.add_argument("directory", help="Melissa output directory")
    parser.add_argument("executable", help="Path to the heat_no_melissac executable")
    parser.add_argument("field", help="The field or quantity to plot")
    parser.add_argument("--Nx", type=int, default=100)
    parser.add_argument("--Ny", type=int, default=100)
    parser.add_argument("--Nt", type=int, default=100)
    parser.add_argument("--command", type=str, help="Submission command", default="mpirun -np 1")
    parser.add_argument("--sample", action='store_true', help="Randomly sample the parameters")
    parser.add_argument("--fps", type=int, help="movie frames per second")

    args = parser.parse_args()

    if not os.path.isdir(args.directory):
        return "{:s} is not a directory".format(args.directory)

    os.chdir(args.directory)

    if not os.path.isdir(args.executable):
        return "{:s} is not a directory".format(args.executable)

    if args.fps is not None and args.fps <= 0:
        return "frames per second must be a positive integer, got {:d}".format(
            args.fps
        )

    t1_start = process_time()

    field = args.field
    n_1 = args.Nx
    n_2 = args.Ny
    n_t = args.Nt
    nb_param = 5

    # 1. Reference solution running (random or specific)
    if (args.sample):
        param = draw_param_set(nb_param)
        print(f"Randomly sampled parameters: {param}")
    else:
        with open('./client_scripts/client.0.sh', 'r') as f:
            for line in f:
                if ("client.sh" in line):
                    param = [float(i) for i in line.split()[-nb_param:]]

    os.makedirs("Res", exist_ok=True)
    submission_command = (
        args.command + " " + args.executable + "/heat_no_melissac" + " "
        + str(n_1) + " " + str(n_2) + " " + str(n_t)
    )
    for i in range(len(param)):
        submission_command += " " + str(param[i])
    print("Running reference simulation...")
    print(submission_command)
    os.system(submission_command)

    # 2. Neural Network solution running
    data_nn = np.full([n_t, n_1, n_2], np.nan)
    model = load_model(nb_param, 1, n_1 * n_2)
    t = [float(i) for i in range(n_t)]
    data_nn = generate_data(model, param, t, n_1, n_2)

    # 3. Reference solution loading
    data = np.full([n_t, n_1, n_2], np.nan)
    n_cpu = 0
    f_list = os.listdir("Res")
    idx_ = f_list[0].find("_")
    for files in f_list:
        n_cpu = max(
            int(f_list[0][idx_ + 1 + f_list[0][idx_ + 1:].find("_") + 1:f_list[0].find(".")]),
            n_cpu
        )
    n_cpu += 1
    print("Loading data... ")
    for i in range(n_t):
        temp_array = np.array([]).reshape(0, 2)
        for j in range(n_cpu):
            file_name = "Res/solution_" + str(i) + "_" + str(j) + ".dat"
            temp_array = np.append(temp_array, np.loadtxt(file_name, delimiter=" "), axis=0)

        temp_array = temp_array[np.argsort(temp_array, axis=0)[:, 0], :]
        data[i] = temp_array[:, 1].reshape(n_1, n_2)

    # 4. Results plotting
    fig = plt.figure(figsize=(10, 10))

    main_title = "Evaluation parameters: (" + "%.1f, " * (nb_param - 1) + "%.1f)"
    main_title = main_title % tuple(param)
    fig.suptitle(main_title, y=0.95, fontsize=16)

    ax1 = fig.add_subplot(221)
    ax2 = fig.add_subplot(222)
    ax3 = fig.add_subplot(223)
    ax4 = fig.add_subplot(224)

    print("Plotting data...")
    images = []
    for t in range(n_t):
        title_fmt1 = "Reference solution: step {:d} of {:d}"
        title1 = title_fmt1.format(t + 1, n_t)
        title_im1 = plt.text(
            0.5,
            1.0,
            title1,
            horizontalalignment='center',
            verticalalignment='bottom',
            transform=ax1.transAxes
        )
        data_im1 = ax1.imshow(data[t])

        title_fmt2 = "Neural Network solution: step {:d} of {:d}"
        title2 = title_fmt2.format(t + 1, n_t)
        title_im2 = plt.text(
            0.5,
            1.0,
            title2,
            horizontalalignment='center',
            verticalalignment='bottom',
            transform=ax2.transAxes
        )
        data_im2 = ax2.imshow(data_nn[t])
        title_fmt3 = "Absolute Error: step {:d} of {:d}"
        title3 = title_fmt3.format(t + 1, n_t)
        title_im3 = plt.text(
            0.5,
            1.0,
            title3,
            horizontalalignment='center',
            verticalalignment='bottom',
            transform=ax3.transAxes
        )
        data_im3 = ax3.imshow(abs(data[t] - data_nn[t]))
        title_fmt4 = "Relative Error (%): NN step {:d} of {:d}"
        title4 = title_fmt4.format(t + 1, n_t)
        title_im4 = plt.text(
            0.5,
            1.0,
            title4,
            horizontalalignment='center',
            verticalalignment='bottom',
            transform=ax4.transAxes
        )
        data_im4 = ax4.imshow(abs(data[t] - data_nn[t]) / data[t] * 100)
        images.append(
            [
                title_im1, data_im1, title_im2, data_im2,
                title_im3, data_im3, title_im4, data_im4
            ]
        )

    plt.colorbar(images[-1][1], ax=ax1)
    plt.colorbar(images[-1][3], ax=ax2)
    plt.colorbar(images[-1][5], ax=ax3)
    plt.colorbar(images[-1][7], ax=ax4)

    animation_duration_s = 10
    time_per_frame_ms = 1000 * animation_duration_s / n_t
    if args.fps is None:
        frames_per_second = n_t / animation_duration_s
    else:
        frames_per_second = args.fps

    anim = animation.ArtistAnimation(
        fig, images, interval=time_per_frame_ms, blit=False, repeat=True
    )
    writer = animation.FFMpegWriter(fps=frames_per_second)
    animation_path = "{:s}.mp4".format(field)

    print("Encoding data as movie in {:s}...".format(animation_path))

    try:
        anim.save(animation_path, writer=writer)
    except FileNotFoundError as e:
        if e.filename == "ffmpeg":
            print("cannot save movie plot because ffmpeg cannot be found")
            sys.exit()
        raise e

    print("Showing plot")

    plt.show()

    t1_stop = process_time()
    print('Temps CPU :', t1_stop - t1_start)


if __name__ == "__main__":
    sys.exit(main())
