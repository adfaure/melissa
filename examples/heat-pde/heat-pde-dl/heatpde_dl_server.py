import logging
import random
from typing import Dict, Any
import numpy as np
import torch
import torch.utils.data
import time

from melissa.server.deep_learning.torch_server import TorchServer, checkpoint
from melissa.server.deep_learning.dataset import MelissaIterableDataset
from melissa.server.deep_learning.buffer import ThresholdQueue
from melissa.server.simulation import SimulationData

logger = logging.getLogger("melissa")


class HeatPDEServerDL(TorchServer):
    """
    Use-case specific server
    """

    def __init__(self, config: Dict[str, Any]):
        super().__init__(config)
        self.param_list = ['ic', 'b1', 'b2', 'b3', 'b4', 't']
        self.mesh_size = self.study_options['mesh_size']

    def set_model(self):
        self.model = self.MyModel(6, self.mesh_size * self.mesh_size, 1).to(self.device)

    def configure_data_collection(self):
        """
        function designed to instantiate the data collector and
        buffer
        """

        self.buffer = ThresholdQueue(
            self.buffer_size,
            self.per_server_watermark,
            self.pseudo_epochs
        )
        self.dataset = MelissaIterableDataset(buffer=self.buffer,
                                              tb_logger=self.tb_logger,
                                              config=self.config,
                                              transform=self.process_simulation_data)

    def train(self, model: torch.nn.Module):

        dataloader = torch.utils.data.DataLoader(
            self.dataset, batch_size=self.batch_size, drop_last=True, num_workers=0)

        # Set Model
        criterion = torch.nn.MSELoss()
        optimizer = torch.optim.Adam(
            model.parameters(), lr=self.dl_config.get("lr", 1e-3), weight_decay=1e-4)

        learning_rate_scheduler = torch.optim.lr_scheduler.ExponentialLR(
            optimizer, 0.995)

        if self.dl_config.get("valid_data_path", None):
            dataloader_valid = self.load_validation_data(self.dl_config["valid_data_path"])
            logger.info("Found valid dataset path for loss validation computation")
        else:
            dataloader_valid = None
            logger.warning("Did not find valid dataset path for loss validation computation")

        logger.info("Start Training")
        full_loss = 0
        last_batch_time = time.time()
        for batch, batch_data in enumerate(dataloader):
            if self.other_processes_finished(batch):
                # user adds this to enforce all server procs
                # will stop together and avoid a deadlock
                # in the gradient averaging
                break

            self.tb_logger.log_scalar_dbg(
                "put_get_inc", self.buffer.put_get_metric.val, batch)
            if self.dl_config["get_buffer_statistics"]:
                self.get_buffer_statistics(batch)

            # Backprogation
            optimizer.zero_grad()
            x, y_target = batch_data
            x = x.to(self.device)
            y_target = y_target.to(self.device)
            y_pred = model(x)
            loss = criterion(y_pred, y_target)
            loss.backward()
            optimizer.step()
            full_loss += loss.item()

            learning_rate_scheduler.step()

            if batch > 0 and (batch + 1) % self.n_batches_update == 0:
                samples = self.batch_size * self.n_batches_update * self.num_server_proc

                self.tb_logger.log_scalar("Loss/train", full_loss / samples, batch)
                full_loss = 0

                if dataloader_valid is not None:
                    val_loss = 0
                    for _, batch_data in enumerate(dataloader_valid):
                        x, y_target = batch_data
                        x = x.float().to(self.device)  # type: ignore
                        y_target = y_target.float().to(self.device)
                        # model evaluation
                        with torch.no_grad():
                            model.eval()
                            y_pred = model(x)
                            loss = criterion(y_pred, y_target)
                            val_loss += loss.item()

                    model.train()
                    self.tb_logger.log_scalar("Loss/valid", val_loss / self.n_val, batch)

                # Learning rate
                lrs = []
                for grp in optimizer.param_groups:
                    lrs.append(grp["lr"])
                    self.tb_logger.log_scalar("lr", lrs[0], batch)
                samples_per_second = samples / (time.time() - last_batch_time)
                self.tb_logger.log_scalar_dbg(
                    "samples_per_second", samples_per_second , batch)
                last_batch_time = time.time()

        logger.info(f"{self.rank} finished training")
        checkpoint(model=model, optimizer=optimizer, batch=batch, loss=loss.item())

    def draw_parameters(self):
        time_discretization = self.study_options['time_discretization']
        Tmin, Tmax = self.study_options['parameter_range']
        param_set = [self.mesh_size, self.mesh_size, time_discretization]
        for i in range(self.study_options['nb_parameters']):
            param_set.append(random.uniform(Tmin, Tmax))
        return param_set

    def process_simulation_data(cls, msg: SimulationData, config: dict):
        study_options = config["study_options"]
        nb_params = study_options["nb_parameters"]
        # mesh_size = study_options["mesh_size"]

        x = torch.as_tensor(msg.parameters[-nb_params:] + [msg.time_step]).float()
        y = torch.from_numpy(msg.data[0])

        return x, y

    class MyModel(torch.nn.Module):
        def __init__(self, input_features, output_features, output_dim):
            super().__init__()
            self.output_dim = output_dim
            self.output_features = output_features
            self.hidden_features = 1024
            self.net = torch.nn.Sequential(
                torch.nn.Linear(input_features, self.hidden_features),
                torch.nn.ReLU(),
                torch.nn.Linear(self.hidden_features, self.hidden_features),
                torch.nn.ReLU(),
                torch.nn.Linear(self.hidden_features, output_features * output_dim),
            )

        def forward(self, x):
            y = self.net(x)
            return y

    def load_validation_data(self, path: str):
        np_xs_valid = np.load(f"{path}/heat_pde_parameters_valid.npy")
        np_ys_valid = np.load(f"{path}/heat_pde_valid.npy")
        torch_xs_valid = torch.from_numpy(np_xs_valid)
        torch_ys_valid = torch.from_numpy(np_ys_valid)
        self.n_val = torch_xs_valid.shape[0]

        dataset_valid = torch.utils.data.TensorDataset(torch_xs_valid, torch_ys_valid)
        dataloader_valid = torch.utils.data.DataLoader(
            dataset_valid, batch_size=self.batch_size, drop_last=True, num_workers=0,
            shuffle=True)

        return dataloader_valid

    def get_buffer_statistics(self, batch: int):
        rows = len(self.buffer.queue)
        columns = int(self.study_options['nb_parameters']) + 1
        stats_array = np.zeros((rows, columns))
        for i in range(rows):
            item = self.buffer.queue[i]
            x, _ = self.process_simulation_data(item.data, self.config)
            stats_array[i, :] = x

        std, mean = torch.std_mean(torch.tensor(stats_array), dim=0)
        for std_v, mean_v, param in zip(std, mean, self.param_list):
            self.tb_logger.log_scalar(f"buffer_std/{param}", std_v, batch)
            self.tb_logger.log_scalar(f"buffer_mean/{param}", mean_v, batch)
