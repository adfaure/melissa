# Online vs Offline demonstration

Melissa enables large scale deep learning which depends on a concept called "online training." In contrast to "offline" training, online neural network training begins before the full dataset is created. In other words, neural network training occurs simultaneously to `client` data generation. This online training technique relieves the memory limitations associated with collecting and sampling from the full dataset in offline training. Further, online training is well suited for supercomputers where the number of concurrent `client` data generators is not limited by a single motherboard. 

Online training depends strongly on the definition of the "`buffer`," which is a limited block of memory available for storing samples. `Client` data-generators add samples to the `buffer`, while training batches are constructed by sampling from the `buffer`. Atlhough `buffer` sampling in online-training settings is deep research topic, `Melissa` provides a solid foundation for scientists to rapidly prototype their `buffer`-related hypotheses. Further, the buffer in Melissa is distributed across multiple GPUs, enabling larger parameteric sampling explorations.

The Melissa architecture depends on the hypothesis that online training can perform equally if not better than offline training. The following demonstration supports this hypothesis by comparing online, offline, and pseudo-offline trainings of a deep surrogate on a simple Heat partial-differential equation. 

## Methodology

### Data generation
The `client` data generators are solving the heat-pde equation for a 100x100 2-dimensional grid. Each `client` starts solving the heat-pde based on 4 temperature boundary conditions as well as the initial temperature, all sampled randomly from a uniform parameter space (between 100 and 200 K). The following video shows the evolution of the heat-pde for a specific set of boundary conditions (reference and example neural net solution):

![type:video](assets/temperature.mp4)

`Clients` generate the 100x100 temperature grid for 100 time-steps. Each sample is comprised of the boundary conditions, initial temperature, and 100x100 temperature grid. For online training, these samples are added to the training `buffer` as they are generated. In offline and pseudo-offline training, all samples are generated and stored to memory before initaiting training.


### Model architecture

The model architecture is the same for online, offline, and pseudo-offline training methods: 3 layers separated by ReLU activation functions. Six parameter input and 10,000 output (100x100 temperature field).

The present study used a torch framework:

```python
model = 
torch.nn.Sequential(
    torch.nn.Linear(6, 1024),
    torch.nn.ReLU(),
    torch.nn.Linear(1024, 1024),
    torch.nn.ReLU(),
    torch.nn.Linear(1024, 100 * 100),
)
```

### Training
#### Online

A training-loop for the neural network first builds a batch of samples by randomly pulling samples from the `buffer` (assuming the `clients` have generated enough samples to reach a user-defined `watermark`), then it feeds the batch into the neural network, finally it checks the loss by comparing the neural-network output for each sample to the target fields. The loss is then back propagated across the network to update network weights. This process of batch building, training, loss backpropagation is repeated indefinitely until the `buffer` no longer has a satisfactory number of samples to build a batch. 


**pseudo-code**
```python
# data loader will pull samples from the buffer to 
# create batches of size 10. The dataloader will
# yield batches as long as there are samples 
# available in the buffer. Client data-generators will
# add samples to the buffer.

for batch, batch_data in enumerate(dataloader):
    # batch of 10 is pulled from buffer by the dataloader
    # x are the 5 boundary conditions and the timestep
    # y_target is the temperature field evolution corresponding
    # to 5 bcs + timestep
    x, y_target = batch_data

    # y_pred is the neural network prediction
    y_pred = model(x)
    
    # loss based on Mean Square Error
    loss = MSELoss(y_pred, y_target)

    # Backprogation of the loss
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
```

* Number of clients: 10,000
* watermark: 10,000 samples
* epochs: 1
* total samples seen: 1,000,000
* total unique samples seen:1,000,000


*Reproduce* this online training example can be reproduced with the following launch command and configuration file:

```bash
melissa-launcher --config_name examples/heat-pde/config_online
```

`config_online.json`:
```json
{
    "server_filename": "heatpde_server.py",
    "server_class": "HeatPDEServer",
    "output_dir": "online_watermark_500",
    "study_options": {
        "field_names": [
            "temperature"
        ],
        "num_clients": 1000,
        "num_samples": 100,
        "nb_parameters": 5,
        "simulation_timeout": 400,
        "checkpoint_interval": 300,
        "crashes_before_redraw": 1000,
        "verbosity": 2,
        "valid_data_path": "../offline-example/validation_data"
    },
    "dl_config": {
        "n_offline_simulations": 2,
        "batch_size": 10,
        "per_server_watermark": 500,
        "buffer_size": 500,
        "log_step_frequency": 1
    }
```

If users wish to track model validation loss, they will need to also generate and store a validation dataset:

```
python3 run_offline_study.py --out-dir validation_data --id offline-100sim-100epoch --epochs 100 --generate-data true --n-simulations 120 --n-valid-sims 20
```


#### Offline

Classical training: all data is first generated and stored to memory. A single static-dataset is constructed from the data and fed to the training method:

**pseudo-code**
```py
# create and store all samples in results
results = array([])
for i, param_combo in range(parameter_combinations):
    results.append(heat_pde(param_combo))
# results contains 10,000 samples (100 clients each yielding 100 timesteps)

model.fit(results, epochs=15)
```

* Number of clients: 100
* watermark: N/A
* epochs: 100
* total samples seen: 1,000,000
* total unique samples seen: 10,000

*Reproduce*:
An offline script is provided in `examples/heat-pde/offline-training` [here](https://gitlab.inria.fr/melissa/melissa/-/tree/develop/examples/heat-pde/offline-training) to help users reproduce the results presented in this document. The script can be run by navigating to `examples/heat-pde/offline-training` and running:

```
python3 run_offline_study.py --out-dir test_valid_120 --id offline-100sim-100epoch --epochs 100 --generate-data true --n-simulations 120 --n-valid-sims 20 --train true
```

This script will generate heat-pde solutions and train a model offline using the same architectures and hyperparameters as those used in the `examples/heat-pde/heatpde_server.py`. The only difference in the offline script is that all data is collected and batched into epochs in a classical training loop. In contrast, the `heatpde_server.py` uses a data streaming method which does not use epochs for training.


<!-- #### Pseudo-offline

Pseudo-offline training is a feature offered in Melissa ([details here](melissa-server.md#validation-with-a-pseudo-offline-approach)) which aims to provide users with the ability to use melissa and the basic `ThresholdQueue` to aggregate all client samples before initiating training (similar to a true offline training). Thus, the training loop matches the loop presented in [online](#online), but the batch construction does not start until all samples are generated and waiting in the buffer. In pseudo-offline, epochs are simulated by sampling until the total number of seen batches is equivalent to (`num_samples` * `num_clients` / `batch_size`) * `pseudo_epochs`. In the present study, sampling was performed with a uniform random distribution, which means not all samples are seen an equivalent number of times (in contrast to true offline training).

* Number of clients: 100
* watermark: 10,000 samples (all samples)
* epochs: 25
* total samples seen: 250,000
* total unique samples seen: ~10,000

*Reproduce* this pseudo-offline training can be reproduced with the following launch command and configuration file:

```bash
melissa-launcher --config_name examples/heat-pde/config_online
```
`config_pseudo_offline.json`:
```json
{
    "server_filename": "heatpde_server.py",
    "server_class": "HeatPDEServer",
    "output_dir": "pseudo_offline",
    "study_options": {
        "field_names": [
            "temperature"
        ],
        "num_clients": 100,
        "num_samples": 100,
        "nb_parameters": 5,
        "simulation_timeout": 400,
        "checkpoint_interval": 300,
        "crashes_before_redraw": 1000,
        "verbosity": 2
    },
    "dl_config": {
        "n_offline_simulations": 2,
        "batch_size": 10,
        "per_server_watermark": 10000,
        "buffer_size": 10000,
        "log_step_frequency": 1,
        "pseudo_epochs": 25
    }
```
 -->

## Loss 

The figure below shows the loss evolution for online (10,000 clients) and offline (100, 500 clients) methods where each configuration is run for five realizations. As shown, the online method outperforms both offline configurations by up to 68% on the validation dataset. Additionally, the offline configurations both overfit as indicated by the decreasing training loss following a plateaued.

![image](assets/heat_equation_loss.png)
*Comparison of online and offline training on the Heat equation*