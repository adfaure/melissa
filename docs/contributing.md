## Contributing

### Bugs and feature requests

The developers team welcomes any feedback from Melissa users whether their experience was successful or not. In the latter case however you are encouraged to contact us through our [Discourse Forum](https://melissa.discourse.group/invites/R58UwZ7JJh) so that an issue can be opened.

To facilitate the bug correction please tell us about your execution environment, the scheduler you used and provide a minimal working example to reproduce the problem.

### Contributing to Melissa

New contributors can contact us by [e-mail](./about-us.md#current-developers) or join the free [Discourse Forum](https://melissa.discourse.group/invites/R58UwZ7JJh) to request a free account to the open-source [Melissa gitlab](https://gitlab.inria.fr/melissa/melissa) repository.

!!! Note
    Development already in progress or under consideration are listed in Melissa [Issues](https://gitlab.inria.fr/melissa/melissa/-/issues) and [Merge requests](https://gitlab.inria.fr/melissa/melissa/-/merge_requests). New contributors are hence advised to go through these before working on a new feature.

We favour the use of Merge Requests and the following git practice:

1. Create a new branch from develop
2. Commit all your changes in this branch
3. Create a new Merge Request

!!! Notes
    - Please do not put merge commits in the branch.
    - Please create one branch per fix or feature.

### Coding style

A great deal of work was done to format Melissa Python code accordingly to [PEP8](https://peps.python.org/pep-0008/). To enforce this standard, safeguards were also added to the Continuous Integration. Hence, any implementation should pass the following tools:

- [`flake8`](https://flake8.pycqa.org/en/latest/)
- [`mypy`](https://mypy.readthedocs.io/en/stable/)

In general, we recommend developers to use [Black](https://black.readthedocs.io/en/stable/) as a code formatter.

### Additional suggestions

Because Melissa is highly modular and theoretically designed to work on any HPC platform, contributors are encouraged to keep their implementation as simple as possible. For example, a modification of the launcher would ideally take place at the level of the scheduler classes in order not to jeopardize higher parts of the code. In the same way, modifications of the server sources should be compatible with all use cases.

In any case, please do not hesitate to contact someone amongst the [developers team](./about-us.md#current-developers). We might be able to give you hints on how to implement the targeted fix or feature. 

