## TODO list

* Short-term and current developments are listed in our [Merge Requests](https://gitlab.inria.fr/melissa/melissa/-/merge_requests).

* Medium-term code related tasks are listed in this [Issue](https://gitlab.inria.fr/melissa/melissa/-/issues/1).

* Long-term Melissa development plans are listed in this [Issue](https://gitlab.inria.fr/melissa/melissa/-/issues/157).