## Continuous Integration and tests

### Continuous Integration

Melissa relies on GitLab standard continuous integration tool and the script [.gitlab-ci.yml](https://gitlab.inria.fr/melissa/melissa/-/blob/develop/.gitlab-ci.yml) located at the directory root. The CI general structure can be illustrated as follows:

![image](assets/melissa_ci.svg)

The pipeline is accelerated by caching a base docker image which holds all necessary dependencies to check, test, and install all features of Melissa. The base docker image should not be rebuilt unless developers add/update dependencies to the project. Therefore, the docker build stage of the CI can only be manually launched by clicking the play button of the `build` stage of the pipeline on the [melissa gitlab](https://gitlab.inria.fr/melissa/melissa/-/pipelines). This will begin building the base docker image, followed by pushing the container to the Melissa container registry. All subsequent pipelines will use the newly built base container.

The various stages of the CI pipeline can be run on INRIA gitlab shared runners (`ci.inria.fr` and `small`/`medium`/`large` tags) or our in-house DATAMOVE computer **maiko** (`docker` tag). It is preferable to use **maiko** because it caches the base docker image - expediting the pipeline. In comparison, the INRIA shared runners are explicitly set to not allow image caching.

### Melissa Unit Tests

The best way to test Melissa is to run Melissa. Indeed, as a distributed application, most bugs are detected in "real world" conditions (_i.e._ at scale and on a supercomputer). Nevertheless, absent-mindedness programming errors can be spotted through unit testing.

The launcher was designed as a modular piece of code *independent of the batch scheduler at hand*. As a results, the higher level structure parts (_e.g._ the I/O master and state machine) are thoroughly testable.

Similarly, the server was entirely revisited and redesigned so that the central objects (_e.g._ `BaseServer`, `Simulation`, `FaultTolerance`, etc.) could be instantiated and tested separately.

!!! Note
    Parts of the server tests are still under development and should be added progressively.

For now, the tests are contained in the [`tests`](https://gitlab.inria.fr/melissa/melissa/-/tree/develop/tests) folder which has the following structure:
```bash
tests/
├── launcher
    ├── test_io.py
    ├── test_message.py
    ├── test_state_machine.py
├── scheduler
    ├── test_dummy.py
    ├── test_openmpi.py
    ├── test_scheduler.py
    ├── test_slurm.py
├── server
    ├── simple_sa_server.py
    ├── test_sensitivity_analysis_server.py
    ├── test_server.py
├── utility
    ├── test_functools.py
    ├── test_networking.py
    ├── test_timer.py
```

As discussed in the [Contributing tab](contributing.md), any contribution to the code should be compatible with the tests in place. 

The latest interactive coverage report is available [here](coverage-report.md). If developers wish to generate their own report locally, they need to run:

```bash
coverage run --source=melissa/ -m pytest tests/
coverage html -d coverage-report
```

which will run the unit tests and then create a folder with detailed summary information about the code coverage.

### Melissa advanced Continuous Integration tools

Since the first version of Melissa, several CI strategies (_e.g._ based on various docker or lxd containers) have been explored. This work is gathered in the [melissa-ci](https://gitlab.inria.fr/melissa/melissa-ci) repository which contains the following elements:

- docker/ubuntu: the `datamove/ubuntu` docker image.
- gitlab-runner-executor-lxd: scripts to use LXD as executor of GitLab runner.
- lxd: LXD images.
- virtual-cluster: instructions and scripts to build an LXD virtual cluster (with `SLURM` or `OAR` as a scheduler).
- *.sh: scripts to build and import the LXD and docker images.

Although we can assume that users concerned with a specific scheduler usually have access to it, it can be convenient to test Melissa developments in a cluster-like environment without wasting time in the supercomputer queue. To address this issue, an LXD based virtual cluster can be deployed locally by following [these general guidelines](virtual-cluster.md#setting-up-a-virtual-slurm-cluster-for-melissa-da-ci) or this [step-by-step tutorial](virtual-cluster.md).

More recently, work performed in the frame of [`Melissa-DA`](https://gitlab.inria.fr/melissa/melissa-da) was done to use the virtual cluster as a CI testing mean. Instructions regarding this aspect are available [here](https://gitlab.inria.fr/melissa/melissa-ci/-/tree/master/virtual-cluster#setting-up-a-virtual-slurm-cluster-for-melissa-da-ci).
