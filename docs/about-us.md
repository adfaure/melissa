## About us

### History

The Melissa project stems from FlowVR, a middleware for large scale data-flow oriented parallel application which was firstly used for virtual reality, telepresence and computational steering. As Theophile Terraz and Alejandro Ribes joined Bruno Raffin, former lead developer of FlowVR, it was reoriented towards in-situ analytics to finally give birth to Melissa: a Modular External Library for In Situ Statistical Analysis.

### Current developers

The current developers are listed in the following table:

| Developers | Role | Contact info. |
|---|---|---|
| Bruno Raffin |  Research Director (INRIA DATAMOVE) | bruno.raffin@inria.fr |
| Lucas Meyer | PhD student (EDF/INRIA DATAMOVE) | lucas.meyer@inria.fr |
| Robert Caulk | Research engineer (INRIA DATAMOVE) | robert.caulk@inria.fr |
| Marc Schouler | Research engineer (UGA/INRIA DATAMOVE) | marc.schouler@inria.fr |


### Former developers

All former contributors are listed below in alphabetical order:

* Achal Agarwal
* Juan Manuel Baldonado
* Christoph Conrads
* Sebastian Friedemann
* Bartłomiej Pogodziński
* Alejandro Ribes
* Anna Sekula
* Théophile Terraz


### Citing Melissa

Melissa: Large Scale In Transit Sensitivity Analysis Avoiding Intermediate Files. Théophile Terraz, Alejandro Ribes, Yvan Fournier, Bertrand Iooss, Bruno Raffin. The International Conference for High Performance Computing, Networking, Storage and Analysis (Supercomputing), Nov 2017, Denver, United States. pp.1 - 14.


```
inproceedings{terraz:hal-01607479,
  TITLE = {{Melissa: Large Scale In Transit Sensitivity Analysis Avoiding Intermediate Files}},
  AUTHOR = {Terraz, Th{\'e}ophile and Ribes, Alejandro and Fournier, Yvan and Iooss, Bertrand and Raffin, Bruno},
  URL = {https://hal.inria.fr/hal-01607479},
  BOOKTITLE = {{The International Conference for High Performance Computing, Networking, Storage and Analysis (Supercomputing)}},
  ADDRESS = {Denver, United States},
  PAGES = {1 - 14},
  YEAR = {2017},
  MONTH = Nov,
  KEYWORDS = {Sensitivity Analysis ; Multi-run Simulations ; Ensemble Simulation ; Sobol' Index ; In Transit Processing},
  PDF = {https://hal.inria.fr/hal-01607479/file/main-Sobol-SC-2017-HALVERSION.pdf},
  HAL_ID = {hal-01607479},
  HAL_VERSION = {v1},
```

### Fundings

The recent developments of Melissa were made possible thanks to several funding sources that include:

* EOCOE II: the European Union’s Horizon 2020138 research and innovation programme under grant agreement No 824158139,
* REGALE: the European High-Performance140 Computing Joint Undertaking (JU) under grant agreement No 956560.
