## Contact us

Users can quickly contact Melissa developers for assistance by joining the free [Discourse Forum](https://melissa.discourse.group/invites/R58UwZ7JJh). Here, the developers can also give you a free account to the open-source [Melissa gitlab](https://gitlab.inria.fr/melissa/melissa) repository in case you are interested in contributing to the source. 

In other cases, users are invited to contact us directly through our [INRIA e-mail addresses](./about-us.md#current-developers).