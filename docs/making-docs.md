## Build the documentation

Users can build the documentation website by navigating to the top of the `deep-melissa` directory and running:

```bash
mkdocs serve
```

Successful execution should generate the following output:

```bash
INFO     -  Building documentation...
INFO     -  [13:12:48] Reloading browsers
INFO     -  [13:12:49] Browser connected: http://127.0.0.1:8000/
```

After which users can put `http://127.0.0.1:8000/` into their brower to access the documentation website. 