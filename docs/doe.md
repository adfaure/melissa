## Design of Experiments

"The design of experiments (DOE) also known as experiment design or experimental design, is the design of any task that aims to describe and explain the variation of information under conditions that are hypothesized to reflect the variation" (see [here](https://en.wikipedia.org/wiki/Design_of_experiments)). In other words, the DOE defines which experiments to perform in order to explore the parameter space of a given input/output problem.

In the frame of surrogate modelling for instance, the objective usually is to learn the behavior of an expensive black box function in order to cheaply predict a statistically coherent response to a given input. However, depending on the number of dimensions in the design space, exploration of the parameter space can quickly become overwhelming. Indeed, the uniform exploration of a parameter space grows exponentially with the number of dimensions which is often referred as the _curse of dimensionality_. For this reason, sophisticated sampling methods are often favoured over basic uniform sampling. 

### Design of Experiments in Melissa

In Melissa, the sampling rely on the `draw_patameters` method which needs to be defined by the user in `<use-case>_server.py`. This can either be directly done by writing the full method (see [heat-pde (SA)](https://gitlab.inria.fr/melissa/melissa/-/blob/develop/examples/heat-pde/heat-pde-sa/heatpde_sa_server.py#L14-19)), or by instantiating a [`ParameterGenerator` object](https://gitlab.inria.fr/melissa/melissa/-/blob/develop/melissa/server/deep_learning/parameters.py) and using its own `draw_parameters` method (see [lorenz (DL)](https://gitlab.inria.fr/melissa/melissa/-/blob/develop/examples/lorenz/lorenz_server.py) or [heat-pde (DL)](https://gitlab.inria.fr/melissa/melissa/-/blob/develop/examples/heat-pde/heat-pde-dl/heatpde_dl_server.py)).

### Uniform sampling

In the context of Melissa, users are mostly concerned with ensemble runs where, regardless of the number of dimensions, the number of computable solutions is often high enough for uniform sampling to stay relevant. Furthermore, When doing sensitivity analysis for instance, uniform sampling is particularly useful since it is an unbiased method that provides uncorrelated samples as required by the pick-freeze method.

### Scipy based sampling

When dealing with deep surrogates, uniform sampling may however lead to slower learning due to a less efficient coverage of the design space. In addition, an unsatisfactory training may require the study to be pushed further. In this case, incremental parameter space search can be performed efficiently thanks to sequence sampling methods provided by the [`scipy.stats.qmc` submodule](https://docs.scipy.org/doc/scipy/reference/stats.qmc.html).

#### Halton sequence

The [Halton sequence](https://en.wikipedia.org/wiki/Halton_sequence) is a deterministic sampling method. In Melissa, the `HaltonGenerator` is based on [`scipy.stats.qmc` Halton sampler](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.qmc.Halton.html#scipy.stats.qmc.Halton).

#### Latin Hypercube Sampling (LHS)

The [latin hypercube sampling](https://en.wikipedia.org/wiki/Latin_hypercube_sampling) is a non-deterministic method. In Melissa, the `LHSGenerator` is based on [`scipy.stats.qmc` Latin Hypercube sampler](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.qmc.LatinHypercube.html).

!!! Note
    Non-deterministic generators take a `seed` integer as argument in order to enforce the reproducibility of the generated inputs.

!!! Warning
    As opposed to the Halton sequence, sampling twice 10 samples from an LHS sampler won't yield the same DOE as when sampling 20 samples at once.

### DOE quality metrics

The Figure below compares the DOEs obtained with uniform, LHS and Halton sampling of 50 points across a parameter space of 2 dimensions:

![DOE comparison](assets/doe.png)

It clearly shows how uniform sampling may result in both cluttered and under-explored regions across the parameter space while Halton and LHS sampling provide a more homogeneous coverage. 

In addition, as discussed earlier, LHS and Halton sampling are sequence samplers which means that their DOE can be enhanced a posteriori by resampling from the same generator. This feature is illustrated on the figure below where 20 points are added to the previous sets of parameters.

![DOE comparison](assets/doe_add.png)

Finally, although the quality of the DOE may seem evident from the figures, intuition may be misleading. In order to evaluate the quality of a DOE, `scipy.qmc` comes with a [`discrepancy`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.qmc.discrepancy.html) method:

> The discrepancy is a uniformity criterion used to assess the space filling of a number of samples in a hypercube. A discrepancy quantifies the distance between the continuous uniform distribution on a hypercube and the discrete uniform distribution on distinct sample points.

> The lower the value is, the better the coverage of the parameter space is.

For the DOEs represented in this section, the following discrepancies were obtained:

|Sampling|Sample size|Discrepancy|
|---|---|---|
|Uniform|50|0.01167|
|Uniform|50+20|0.01045|
|LHS|50|0.00054|
|LHS|50+20|0.00041|
|Halton|50|0.00183|
|Halton|50+20|0.00097|
