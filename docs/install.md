## Installation

### Quick Install

The following guide outlines the basic installation of Melissa, which can be performed locally or on a variety of well established supercomputer platforms. However, some supercomputers/data-generators require more advanced installation methods, which can be found [below](#advanced-installation) (_e.g._ involving incompatible dependencies between the server and the clients).

The user can first create a `melissa` folder by cloning the corresponding repository from its [Inria GitLab repository](https://gitlab.inria.fr/melissa/melissa.git):

```bash
git clone https://gitlab.inria.fr/melissa/melissa.git
cd melissa
```

The following dependencies must be installed before building Melissa:

- CMake 3.7.2 or newer
- GNU Make
- A C99 compiler
- An MPI implementation
- Python 3.8 or newer
- ZeroMQ 4.1.5 or newer

On debian based systems, these dependencies can be installed via:

```bash
sudo apt-get install cmake build-essential libopenmpi-dev python3.8 libzmq3-dev
```

In addition, Melissa has three levels of Python dependencies:

1. dependencies to run the sensitivity analysis server are listed in [`requirements.txt`](https://gitlab.inria.fr/melissa/melissa/requirements.txt):
```txt
pyzmq>=22.3.0
mpi4py==3.1.3
numpy>=1.21
jsonschema>=4.5
python-rapidjson>=1.9
scipy>=1.10.0
```
By default, they will automatically be installed with `pip` at build. Otherwise their installation can be prevented by setting `-DMELISSA_REQUIREMENTS=OFF`.

2. dependencies to run a deep-surrogate training are listed in [`requirements_deep_learning.txt`](https://gitlab.inria.fr/melissa/melissa/requirements_deep_learning.txt):
```txt
tensorboard>=2.10.0
torch>=1.12.1
matplotlib
```
If missing, these will be installed with `pip` at build by setting `-DMELISSA_DEEP_LEARNING=ON`.

3. extra-dependencies for development purposes are listed in [`requirements_dev.txt`](https://gitlab.inria.fr/melissa/melissa/requirements_dev.txt):
```txt
flake8==5.0.4
mypy==0.982
pytest==7.1.3
pytest-mock
python-markdown-math
mkdocs
mkdocs-material
mkdocs-video
mkdocs-bibtex
coverage
plotext
types-requests
tensorflow
pandas
debugpy
```
If missing, these will be installed with `pip` at build by setting `-DMELISSA_EXTRAS=ON`.

!!! Notes
    - On most supercomputers these dependencies should be available as modules or could be installed with a package manager like [`spack`](https://spack.io/).
    - If the server requires a specific Python version resulting from a particular package (_e.g._ `TensorFlow` or `PyTorch`), **the user must load this module or virtual environment before building and executing Melissa**.
    - The Melissa header for Fortran90 will be installed even if no Fortran compiler is present.

Next, create build and install directories and change to build:
```sh
mkdir build install && cd build
```
Call `CMake` and customize the build by passing build options on its command line (see the table below). If you are unsure if all dependencies are installed, simply run `CMake` because it will find all required software packages automatically and check their version numbers or print error messages otherwise. The build here has compiler optimizations enabled:
```sh
cmake -DCMAKE_INSTALL_PREFIX=../install ..
make
make install
```

!!! Note
    The `install` folder will contain a copy of the `melissa` sources and the package executables (`melissa-launcher`, `melissa-server` and `melissa-monitor`) will be placed in `install/bin`. In order not to alter the original files, the user is free to work from `melissa/install/melissa/examples`.

Users installing on a local machine may benefit from including the `-DINSTALL_ZMQ=ON` flag. Meanwhile, cluster users should ensure the cluster installed ZeroMQ is linked. Additionally, users can expedite the `make` process by passing the `-j` flag appended with the number of available cores on the compilation machine (.e.g. `make -j6` for a machine with 6 cores). Other build options are listed in the table below.

### Build Options

| CMake option             | Default value | Possible values | Description |
| --                       | --            | -- | -- |
| `-DCMAKE_INSTALL_PREFIX` | `../install`  | any location on the filesystem | Melissa installation directory |
| `-DINSTALL_ZMQ`          | `OFF`         | `ON`, `OFF` | Download, build, and install ZeroMQ |
| `-DMELISSA_REQUIREMENTS`  | `ON`         | `ON`, `OFF` | Install Python minimal requirements|
| `-DMELISSA_DEEP_LEARNING` | `OFF` | `ON`, `OFF` | Install dependencies for Deep Learning server |
| `-DMELISSA_EXTRAS` | `OFF` | `ON`, `OFF` | Install extra dependencies for non-crucial features <br /> such as doc building or automatic log parsing. |
| `-DMELISSA_DEVELOP_MODE`  | `OFF`         | `ON`, `OFF` | Install the Melissa Python sources in editable mode  <br /> _i.e._ `pip` uses the `-e` option |

#### Checking the installation 

After successful installation, users can open a terminal, source the environment file `melissa/melissa_set_env.sh`, execute `melissa-launcher -h` and be prompted with the available command line arguments. A full confirmation of the installation can be performed by running one of the available example scripts:

- Sensitivity-Analysis with the Heat-PDE use-case [here](first-sa-study.md).
- Deep Surrogate training with the Heat-PDE use-case [here](first-dl-study.md#heat-pde-use-case).
- Deep Surrogate training with the Lorenz use-case [here](first-dl-study.md#lorenz-attractor-use-case).

### Advanced installation

#### Incompatible dependencies
Due to the flexible architecture of Melissa, the data transfer between the clients and the server are organized with an API developed in C and wrapped in other languages. As a consequence, it is crucial that the API compilation performed at build is made with dependencies consistent with both the server and the clients. 

When building Melissa with deep-learning dependencies on a supercomputer, the server which uses `PyTorch` or `TensorFlow` may require specific virtual environment or modules coming with their own constraints. In this case, Melissa may need to be built twice.

#### Splitting install dependencies
On [Jean-Zay](http://www.idris.fr/eng/jean-zay/index.html) for instance, the user may want to instrument a solver relying on particular compilers or `openmpi` versions. In our case, `code_saturne` was built with the following dependency:

- `openmpi/4.0.2-cuda`.

In the meantime, the server may require the `pytorch-gpu/py3/1.13.0` module which comes with the following requirement on Jean-Zay:

- `openmpi/4.1.1-cuda`.

For such a configuration, the user should build Melissa for both setups. Starting inside the `melissa` cloned repository, this would mean going through the following instructions:
```sh
# setting the client dependencies
module load gcc/8.3.1
module load zeromq/4.2.5
module load cmake/3.18.0
module load openmpi/4.0.2-cuda
mkdir build && cd build
# building the API the melissa package without additional Python dependencies
cmake -DCMAKE_INSTALL_PREFIX=../install_client -DMELISSA_REQUIREMENTS=OFF ..
make
make install
cd ..
# setting the server dependencies
module purge
module load gcc/8.3.1
module load zeromq/4.2.5
module load cmake/3.18.0
module load pytorch-gpu/py3/1.13.0
rm -r build && mkdir build && cd build
# building the API and the melissa package with its DL dependencies
cmake -DCMAKE_INSTALL_PREFIX=../install_server -DMELISSA_DEEP_LEARNING=ON ..
make 
make install
```

Since this procedure yields two distinct installation folders, the `melissa_set_env.sh` script location must then be specified explicitly for each of them. This can be done by adding the following lines to the configuration file:
```json
    "client_config": {
        "melissa_client_env": "/path/to/install_client/bin"
    },
    "server_config": {
        "melissa_server_env": "/path/to/install_server/bin"
    }
```
This way, `{melissa_set_env_file}` will be updated with different paths in `client.sh` and `server.sh`. 

!!! Note
    For the clients, only the API matters which is why the Python packages do not need to be fully installed.

#### Dependency propagation during execution
As explained in the [Building a new use-case](./new-use-case.md) tutorial, specific dependencies and environment variables must be set using the `preprocessing_commands` lists in the user configuration file. These bash commands are executed before the data generator executable, on each individual job node. Therefore, they need to prepare the node for the data generator and the `melissa-server`. For example, loading modules to prepare for a data generator would set `client/server_config` to look as follows:
```json
    "client_config": {
        "executable_command": "/path/to/melissa/examples/heat-pde/heat-pde/build/heatc 100 100 100",
        "preprocessing_commands":[
                    "module -v load zeromq/4.2.5",
                    "module -v load openmpi/4.0.2-cuda"]
    },
    "server_config": {
        "preprocessing_commands":[
                    "module -v load zeromq/4.2.5",
                    "module load -v pytorch-gpu/py3/1.10.0"]
    }
```

See [`examples/heat-pde`](https://gitlab.inria.fr/melissa/melissa/-/tree/develop/examples/heat-pde) and [`examples/lorenz`](https://gitlab.inria.fr/melissa/melissa/-/tree/develop/examples/lorenz) for more configuration examples.

!!! Note
    If the [module system](https://hpc-wiki.info/hpc/Modules) is not available or if it does not contain all the required dependencies, this procedure can easily be supplemented (or substituted) with other package managers (_e.g._ `spack` or `conda`).
