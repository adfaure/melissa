## Building a new use-case

This tutorial assumes that the user is familiar with the terminology of Melissa introduced in [Melissa overview](./melissa-overview.md).

### Instrumenting the data-generator (i.e. the `client`)

From the point of view of Melissa, a simulation (i.e. data-generator or client) manages the state of one or more fields or quantities (e.g., energy, temperature, or pressure). Each field or quantity can have its own mesh but these meshes must be **fixed**. For each kind of value to be analyzed by Melissa, the simulation must call
```c
#include <melissa_api.h>
melissa_init("value-kind", grid_size, mpi_communicator);
```
The MPI communicator must be application specific (see the `MPI_Comm_get_attr` documentation regarding the property `MPI_APPNUM`). For every time-step and for every kind of value, the simulation must call
```c
#include <melissa_api.h>
const double* values = ...;
melissa_send("value-kind", values);
```
Keep in mind that one time-step for Melissa does not have to equal one time-step in the simulation. After all data was sent, the simulation must call
```c
#include <melissa_api.h>
melissa_finalize();
```
This statement is obligatory and must take place before `MPI_Finalize()` is called.

!!! Notes
    - The `#include <melissa_api.h>` instruction is specific to C/C++ and must be adapted to the solver language. The Python and Fortran90 corresponding libraries are `melissa_api.py` and `melisa_api.f90`.
    - The list of quantities to be analyzed must be consistent with the fields given in the use-case configuration file `<config-file>.json`.

#### Hints for Fortran Users

Although good practice encourages the use of `mpi` module instead of `mpif.h` (see [here](https://www.mpi-forum.org/docs/mpi-3.1/mpi31-report/node411.htm)), melissa only supports the latter. Hence, the command:
```fortran
include "melissa_api.f90"
```
must be accompanied by:
```fortran
include "mpif.h"
```

The Melissa server being developed in C and since C and Fortran90 do not handle strings the same way, the `field_name` passing of the `melissa_init` subroutine can be responsible for the failure of Melissa. The compatibility between both languages is ensured by null terminating the Fortran `field_name`. For a field named `temperature`, this can be done through the following command:
```fortran
character(len=12) :: field_name = "temperature"//char(0)
```
where `field_name` is the first argument passed to the `melissa_init` subroutine.

### Building your server

For each use-case, a `<use-case>_server.py` file should be created. This file implements a new server class by inheriting from the `SensitivityAnalysisServer` or `TorchServer` classes (see figure below). Depending on the user's application, class methods must (if abstract) or can (if optional) be instantiated. These are summarized in the following table:

|Element|Name|Purpose|
|---|---|---|
|`BaseServer`|`draw_param_set()`|Draws a set of parameters using user <br /> defined function (abstract)|
|`TorchServer`|`server_online()`|Initiating data collection and directing <br /> the custom methods for acting on collected data <br /> (abstract).|
|`TorchServer`|`configure_data_collection()`|Instantiates the data collector and buffer <br /> (abstract).|
|`TorchServer`|`train()`|Use-case based training loop (abstract).|
|`BaseServer`|`process_simulation_data()`|Method used to custom process data <br /> (abstract).|
|`BaseServer`|`setup_environment()`|Any necessary setup methods go here. <br /> e.g. distributed data parallelism with <br /> PyTorch requires `dist.init_process_group` <br /> (abstract).|
|`TorchServer`|`server_finalize()`|All finalization methods go here <br /> (optional).|

![image](assets/server_inheritance.svg)

!!! Note
    Advanced users can inherit any of the `BaseServer` methods, but typically these should remain untouched.

!!! Note
    The `TorchServer` specializes the `DeepMelissaServer` to properly implement all the distributed initialization/trainig/synchronization/finalization methods for `PyTorch`. Therefore, it is best for users to inherit from `TorchServer` to avoid needing to re-implement the distributed wrappers. But if users wish to use a different library than `PyTorch`, they will need to copy the methodology shown in the [`TorchServer`](https://gitlab.inria.fr/melissa/melissa/-/blob/master/melissa/server/deep_learning/torch_server.py).

### Setting the Melissa environment

In simple cases, setting up the environment can simply be done with the following command:
```sh
. "/path/to/melissa/melissa_set_env.sh"
```

Considering the wide application scope of Melissa, managing dependencies can quickly become a burden in more complicated cases. Indeed, when the data-generator comes with specific dependencies different from and incompatible with those of the server, the use-case can require setting different dependencies with the `client_config` and `server_config` `preprocessing_commands` bash commands lists (see [Advanced installation](install.md#advanced-installation)).

!!! Note
    With compiled data generators, users tend to forget that the Melissa environment must first be set, otherwise the API libraries can't be found.

### Configuring the study

To finish the use-case construction, the `<config-file>.json` must finally be configured. This `JSON` file consists of a dictionary comprised of multiple sub-dictionaries. The minimal expected elements are:

* The server file, class name and output directory:
```json
{
"server_filename": "<use-case>_server.py",  # str
"server_class": "<use-case>Server",         # str
"output_dir": "<result-folder>"             # str                      
}
```
* The study general options: 
```json
"study_options": {                 
    "field_names": ["<list-of-fields>"],    # List[str]
    "num_clients": <sampling-size>,         # int
    "num_samples": <nb-time-steps>,         # int
    "group_size": <nb-clients-per-group>,   # int
    "nb_parameters": <nb-parameters>        # int
}
```
  
* The study specific options:
```json
"sa_config": {                 
    "mean": <boolean>,          # lower case boolean
    "variance": <boolean>,      # lower case boolean
    "skewness": <boolean>,      # lower case boolean
    "kurtosis": <boolean>,      # lower case boolean
    "sobol_indices": <boolean>  # lower case boolean
}
```
or
```json
"dl_config": {                 
    "batch_size": <batch-size>,                 # int
    "per_server_watermark": <water-mark>,       # int
    "buffer_size": <buffer-size>                # int
}
```

!!! Notes
    * The user can add any custom variable to any of these dictionary and access it from the server (see `parameter_range` in both `heat-pde` examples).
    * Unless the full path is specified, the output directory will by default be created in the project directory.
    * When Sobol indices are computed, the `group_size` option in the configuration file is ignored and automatically set to `group_size=nb_parameters+2` by the server.  

* The launcher options:
```json
"launcher_config": {
    "scheduler": "<scheduler>",                                 # str
    "scheduler_arg": "<scheduler-options-for-all-jobs>",        # List[str]
    "scheduler_arg_client": "<scheduler-options-for-clients>",  # List[str]
    "scheduler_arg_server": "<scheduler-options-for-server>",   # List[str]
    "fault_tolerance": <boolean>,                               # lower case boolean
    "client_executable": "<path/to/executable>"                 # str
}
```

!!! Note
    The launcher supports a wide variety of options detailed in [Melissa-Launcher](./melissa-launcher.md).


* Client configuration
```json
"client_config": {
    "preprocessing_commands": ["<bash commands>"],      # List[str]
    "executable_command": "<full execution command>"    # str
}
```


* Server configuration
```json
"server_config": {
    "preprocessing_commands": ["<bash commands>"],      # List[str]
}
```

### Launching and debugging a use-case

Once the use-case has been configured properly it can be launched with the following command:
```bash
melissa-launcher --config_name /path/to/project/dir/<config-file>
```

!!! Note
    When debugging we recommend to set the following options in the configuration file:
```json
{
    "study_options": {
        "verbosity": 3,
        ...
    },
    "launcher_config": {
        "fault_tolerance": false,
        "verbosity": 3,
        ...
    }
}
```
This will provide the maximal amount of information in the standard output/error files and the study will immediately stop in case of failure (_i.e._ no instance will be automatically restarted).

Since Melissa relies on three distinct components (launcher, server, clients), a failure can be due to errors coming from any of these elements. The first output to check is that of the launcher:

- An error occurring at the launcher level should be obvious to spot since it would directly be thrown inside the standard output (_i.e._ `melissa_launcher.log`).
- An error occurring at the server level should be noticeable in the launcher output via a reference to an unexpected server/launcher deconnection, a time-out due to the prolonged absence of life signal or the detection of a server job failure. In this case, the user should check the server output (_i.e._ `melissa_server_rank.log`)).
- An error occurring at the client level should be indicated by client job failures (generally if one client fails, all client fail). The user should check any client error output (_e.g._ `openmpi.<uid>.err`, `job.<uid>.melissa-client.err`, `oar.<uid>.err`).

!!! Note
    The `<uid>` of this file must be inferred from the job submission UID (_i.e._ unique identifier) which is attributed by the launcher at submission. For instance with the `OAR` scheduler, if the launcher output writes:
```sh
2022-10-27T16:38:40 melissa.launcher.io            DEBUG    job launched uid=1 id=15408
```
and that a failure is detected afterwards for this specific job (`id=15408`), the corresponding standard error file ot this job will be `oar.1.err`.
