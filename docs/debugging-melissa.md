## Debugging Melissa

Debugging Melissa can be performed using two methods:

* Printing logging information near error points (quick but less powerful, good for supercomputers)
* Using VSCode interactive debugging (requires setup but powerful, good for local prototyping)


### Printing logging information

If a user is encountering an error, they can usually find this information inside the output folder in the `melissa_server.log` or the `melissa_launcher.log`. Near the bottom, there should be a "traceback" which contains localizing information for where the code failed. The easiest way to get a feel for what is going on near that error point, is to add some logging information right before the failure. Typically this should be done using the existing `logger`:

```python
logger.info(f"My x variable has a value of {x}")
```

This would add this line to one of the log files (depending on which file the line was placed), replacing `{x}` with the actual value at that point.

!!! Note: 
    The logger has three levels, `.info()`, `.warning()`, `.error()`, and `.debug()` which filter based on the settings in the user configuration file for `verbosity` where 0 is errors only, 1 includes warnings, 2 includes info and 3 includes debugging messages.


### Debugging `melissa-launcher`

The logging method outlined above may not be sufficient in certain cases where a complex structure needs to be implemented. In these cases, we recommend debugging with [VSCode](https://code.visualstudio.com/docs/editor/debugging). A `launch.json` file should be created with ctrl+shift+d, then click, "Create launch.json file" [details here](https://code.visualstudio.com/docs/editor/debugging#_launch-configurations). Then edit `launch.json` to direct the debugger to call `melissa.launcher` module:

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Melissa launcher",
            "type": "python",
            "request": "launch",
            "module": "melissa.launcher",
            "justMyCode": true,
            "args": [
                "--config_name",
                "config_mpi"
            ]
        }
    ]
}
```

Place a break point anywhere inside `melissa/launcher` by clicking left of the line numbers to illuminate a red dot (this is the creation of the breakpoint). Advanced users can also add conditional logic to a break point (e.g. a variable *must* be a certain value for the breakpoint to become active).

Finally, use ctrl+shift+d to navigate to the debug pane on the left, and click the green "go" flag (or simply hit F5 on the keyboard) to initiate the debug session. Once a breakpoint is hit, the user will have access to the entire local/global scope in the `Debug Console` where they can test bug fixes, step through the code, step into and out of functions, and more.

### Debugging `melissa-server`

The melissa server requires a particular configuration for attaching a remote process, since the launcher is calling the server from the command line. 

Ensure `launch.json` file contains the following `attach` configuration:

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Remote Attach",
            "type": "python",
            "request": "attach",
            "connect": {
                "host": "localhost",
                "port": 5678
            },
            "justMyCode": false
        }
    ]
}
```

Next, ensure that the configuration file has activated `vscode_debugging` by setting it to `true`:

```json
{
    "server_filename": "heatpde_server.py",
    "server_class": "HeatPDEServer",
    "output_dir": "STUDY_OUT",
    "vscode_debugging": true,
    "study_options": {
        ...
```
After setting the config option, set your breakpoint anywhere inside `melissa/server`. Now proceed to launch `melissa-launcher` as normal from the command line. Finally, click `ctrl+shift+d` to move to the debugging pane. At the top you can select the debug config for `Python: Remote Attach`. You will see the server attach to the debugging session, eventually reaching any breakpoint that was set in the server (it may take a moment before it can attach, the server needs to initialize its conenction to the launcher before it is prepared for debug attachment).

The system will now stop at your break point where you can then use interactive commands to move around in the code, viewing the scope, testing possible bug fixes etc. 

!!! Note: the `launcher_config` should not contain any timeout arguments (or they should be sufficiently large):
    ```json
            "scheduler_arg_client": ["-n", "1"], // ,"--timeout", "300"],
            "scheduler_arg_server": ["-n", "1"], // ,"--timeout", "3600"],
    ```
